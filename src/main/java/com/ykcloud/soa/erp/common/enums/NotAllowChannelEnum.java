package com.ykcloud.soa.erp.common.enums;

/**
 * @Author stark.jiang
 * @Date 2021/10/18/15:51
 * @Description:
 * @Version 1.0
 */
public enum NotAllowChannelEnum {
    POS(1, "POS"),
    B2B(2, "B2B"),
    B2C(3, "B2C");
    private Integer typeId;
    private String typeName;
    NotAllowChannelEnum(Integer typeId, String typeName) {
        this.typeId = typeId;
        this.typeName = typeName;
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public static String getTypeNameByTypeId(Integer typeId) {
        String typeName = null;
        for (NotAllowChannelEnum typeEnum : NotAllowChannelEnum.values()) {
            if (typeEnum.typeId==typeId) {
                typeName = typeEnum.typeName;
                break;
            }
        }
        return typeName;
    }
}
