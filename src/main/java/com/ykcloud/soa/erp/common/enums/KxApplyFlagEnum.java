package com.ykcloud.soa.erp.common.enums;

public enum KxApplyFlagEnum {
    HEAD_AND_SHOP(0L,"总部+门店"),
    ONLY_HEAD(1L, "仅总部"),
    ONLY_SHOP(2L, "仅门店");

    private Long value;
    private String desc;

    KxApplyFlagEnum(Long value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public static KxApplyFlagEnum fromValue(Long value) {
        for (KxApplyFlagEnum enumItem : KxApplyFlagEnum.values()) {
            if (enumItem.getValue().equals(value)) {
                return enumItem;
            }
        }
        return null;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
