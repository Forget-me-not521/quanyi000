package com.ykcloud.soa.erp.common.enums;

/**
 * /**
 *
 * @description:
 * @author: Dan
 * @time: 2020/6/14 15:15
 */
public enum  ConfigEnum {
    SYS_PRICE_AND_TAXRATE_GET_TYPE("sys_price_and_taxrate_get_type","系统取价取税率类型");
    ;
    private String config;

    private String desc;

    ConfigEnum(String config, String desc) {
        this.config = config;
        this.desc = desc;
    }

    public String getConfig() {
        return config;
    }

    public void setConfig(String config) {
        this.config = config;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
