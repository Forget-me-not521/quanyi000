package com.ykcloud.soa.erp.common.enums;

/**
 * 库存金额调整方式
 *
 * @author Sealin
 * @date 2018-09-20
 */
public enum CostAdjustMethodEnum {
    WITH_TXC(0L, "使用TXC提交各项修改"),
    WITHOUT_TXC(1L, "不使用TXC"),
    WITH_ORDERED_MESSAGE(2L, "使用顺序消息");
    public final Long ID;
    public final String COMMENT;

    CostAdjustMethodEnum(Long id, String comment) {
        ID = id;
        COMMENT = comment;
    }
}
