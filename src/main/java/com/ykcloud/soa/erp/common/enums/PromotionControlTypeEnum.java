package com.ykcloud.soa.erp.common.enums;

/**
 * @Author stark.jiang
 * @Date 2021/10/18/15:39
 * @Description:
 * @Version 1.0
 */
public enum PromotionControlTypeEnum {
    SINGLE_PRODUCT_BARGAIN_PRICE(1, "单品特价"),
    SINGLE_PRODUCT_DISCOUNT(2, "单品打折"),
    QUOTA_PRESENT(3, "满额赠"),
    FULL_VOLUME_PRESENT(4, "满量赠"),
    FULL_VOLUME_DISCOUNT(5, "满量打折"),
    COMBINA_MARK_UP_PURCHASE(6, "组合加价购"),
    N_PIECE_PROMOTION(7, "N件促销"),
    ANY_COMBINATION(8, "任意组合"),
    QUOTA_REDUCE(9, "满额减"),
    FULL_VOLUME_REDUCE(10, "满量减"),
    QUOTA_DISCOUNT(11, "满额打折"),
    NEAR_DURATION_DISCOUNT(12, "近效期打折"),
    MEMBER_CARD_DISCOUNT(13, "会员卡打折"),
    POINTS(14, "积分");
    private Integer typeId;
    private String typeName;
    PromotionControlTypeEnum(Integer typeId, String typeName) {
        this.typeId = typeId;
        this.typeName = typeName;
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public static String getTypeNameByTypeId(Integer typeId) {
        String typeName = null;
        for (PromotionControlTypeEnum typeEnum : PromotionControlTypeEnum.values()) {
            if (typeEnum.typeId==typeId) {
                typeName = typeEnum.typeName;
                break;
            }
        }
        return typeName;
    }
}
