package com.ykcloud.soa.erp.common.enums;

public enum WhitelistStatusEnums {
    WAITING_AUDIT(1, "待审批"),
    PASS(2, "审批通过"),
    UNPASS(3, "审批未通过");

    private int id;
    private String value;

    WhitelistStatusEnums(int id, String value) {
        this.id = id;
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static String getEnums(int id) {
        for (WhitelistStatusEnums item : WhitelistStatusEnums.values()) {
            if (item.getId() == id) {
                return item.getValue();
            }
        }
        return null;
    }

}
