package com.ykcloud.soa.erp.common.enums;

public enum ReturnReasonEnum {
    COMPANY_ADJUST_INVENTORY(1L,"公司安排调库"),
    QUALITY_NOTICE_RECALL(2L, "质量通知召回"),
    COMPANY_NOTICE_RETURN(3L, "公司通知退回"),
    POS_USED_RETURN(4L, "门店领用退库"),
    POS_APPLY_ADJUST_INVENTORY(5L, "门店申请调库"),
    ITEM_QUALITY_RETURN(6L, "商品质量退库"),
    LOGISTICS_INCOMING_REASON(7L, "物流来货原因"),
    POS_APPLY_FRMLOSS(8L, "门店申请报损");

    private Long value;
    private String desc;

    ReturnReasonEnum(Long value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public static ReturnReasonEnum fromValue(Long value) {
        for (ReturnReasonEnum enumItem : ReturnReasonEnum.values()) {
            if (enumItem.getValue().equals(value)) {
                return enumItem;
            }
        }
        return null;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
