package com.ykcloud.soa.erp.common.enums;/**
 * $DESC$
 *
 * @author Sealin
 * Created on 2018-06-22
 */

/**
 * 支付状态
 *
 * @author Sealin
 * Created on 2018-06-22
 */
public enum PayStatusEnum {
    WAIT_CONFIRM(1L, "未确认"),
    CONFIRNED(2L, "已确认"),
    INVOICED(3L, "已开票");

    public final Long ID;
    public final String COMMENT;

    PayStatusEnum(Long id, String comment) {
        this.ID = id;
        this.COMMENT = comment;
    }
}
