package com.ykcloud.soa.erp.common.enums;

/**
 * 
 * @author gaoyun.shen
 *
 * @date 2018年8月27日 下午5:54:22
 * 
 * @Description BOM方案修改
 */
public enum BOMAdjust {
	BOM_RAW("1", "raw", "BOM原料"), BOM_RAW_LOSS("2", "rawloss", "BOM原料损耗"), BOM_WORK_PRO("3", "workpro", "BOM成品");

	private String typeNumId;
	private String billTypeNumId;
	private String billTypeNumName;

	BOMAdjust(String typeNumId, String billTypeNumId, String billTypeNumName) {
		this.typeNumId = typeNumId;
		this.billTypeNumId = billTypeNumId;
		this.billTypeNumName = billTypeNumName;
	}

	public String getTypeNumId() {
		return typeNumId;
	}

	public void setTypeNumId(String typeNumId) {
		this.typeNumId = typeNumId;
	}

	public String getBillTypeNumId() {
		return billTypeNumId;
	}

	public void setBillTypeNumId(String billTypeNumId) {
		this.billTypeNumId = billTypeNumId;
	}

	public String getBillTypeNumName() {
		return billTypeNumName;
	}

	public void setBillTypeNumName(String billTypeNumName) {
		this.billTypeNumName = billTypeNumName;
	}
	

}
