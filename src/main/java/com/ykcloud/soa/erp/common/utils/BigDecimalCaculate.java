package com.ykcloud.soa.erp.common.utils;

import java.math.BigDecimal;

/**
 * @Description: BigDecimal计算工具
 * @author zhaokang
 * @date 2019/6/5 21:43
 */
public class BigDecimalCaculate {

    private BigDecimal begin ;

    private static final int scale = 2;

    private static final ThreadLocal<BigDecimalCaculate> local = new ThreadLocal<>();

    private BigDecimalCaculate() {
        // 私有构造方法
    }

    public static BigDecimalCaculate ofInstance() {
		BigDecimalCaculate decimalCaculate = local.get();
		if(decimalCaculate==null){
			decimalCaculate = new BigDecimalCaculate();
			local.set(decimalCaculate);
		}
		decimalCaculate.begin = new BigDecimal(0D);//初始化清零
		return decimalCaculate;
    }

    /**
	 * 除
     * @param scale 保留位数
     */
    public BigDecimalCaculate divide(Double divide, int scale) {
        begin = begin.divide(new BigDecimal(String.valueOf(divide)), scale, BigDecimal.ROUND_HALF_UP);
        return this;
    }

	/**
	 * 加
	 */
	public BigDecimalCaculate add(Double add) {
        begin = begin.add(new BigDecimal(String.valueOf(add)));
        return this;
    }

	/**
	 * 乘
	 */
	public BigDecimalCaculate multiply(Double multiply) {
        begin = begin.multiply(new BigDecimal(String.valueOf(multiply)));
        return this;
    }

	/**
	 * 减
	 */
	public BigDecimalCaculate subtract(Double subtract) {
        begin = begin.subtract(new BigDecimal(String.valueOf(subtract)));
        return this;
    }

	/**
	 * 获取结果
	 */
	public double getResult() {
        return begin == null ? 0D : begin.setScale(this.scale, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    public void clear(){
		if (begin != null) {
			local.remove();
		}
	}


}
