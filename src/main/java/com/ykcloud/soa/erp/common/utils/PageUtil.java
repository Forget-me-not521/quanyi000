package com.ykcloud.soa.erp.common.utils;

/**
 * @ClassName: PageUtil.java
 * @Description: 分页工具类
 * @version: v1.0.0
 * @author: fred.zhao
 * @date: 2018年3月21日 下午3:36:37
 */
public class PageUtil {

	
	public static int getStartRecord(int limit, int currentPage) {
		if (currentPage <= 0) {
			return 1;
		}
		return (currentPage - 1) * limit;
	}

	public static int getTotalPage(int totalRecord, int pageSize) {
		if (totalRecord <= 0) {
			return 0;
		}
		int size = totalRecord / pageSize;// 总条数/每页显示的条数=总页数
		int mod = totalRecord % pageSize;// 最后一页的条数
		if (mod != 0) {
			size++;
		}
		return size;
	};
	
	public static int getMod(int totalRecord, int pageSize) {
		int mod = totalRecord % pageSize;// 最后一页的条数
		return mod;
	}
	
	public static void main(String[] args) {
		int totalPage = getTotalPage(1456,200);
		int start = getStartRecord(200,2);
		
		System.out.println(totalPage);
		for(int i=1;i<=8;i++) {
			System.out.println(PageUtil.getStartRecord(200, i)+"limit"+200);
		}
	}
}
