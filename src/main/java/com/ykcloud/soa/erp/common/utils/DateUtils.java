package com.ykcloud.soa.erp.common.utils;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.FastDateFormat;


/**
 * @Author Hewei
 * @Date 2018/3/22 10:31
 */
public class DateUtils {
    public static final String PATTER_TYPE_ONE = "yyyyMMdd";
    public static final String PATTER_TYPE_TWO = "yyyy-MM-dd";
    public static final String PATTER_TYPE_THREE = "yyyy年MM月dd日";
    public static final String PATTER_TYPE_FOUR = "yyyyMMdd hhmmss";
    public static final String PATTER_TYPE_FIVE = "yyyy-MM-dd hh:mm:ss";
    public static final String PATTER_TYPE_FIVE_24 = "yyyy-MM-dd HH:mm:ss";
    public static final String PATTER_TYPE_SIX = "yyyy年MM月dd日 hh:mm:ss";
    public static final String PATTER_TYPE_SEVEN = "yyyyMM";
    public static final String PATTER_TYPE_EIGHT = "yyyy-MM";

    //java.lang.NoClassDefFoundError: Could not initialize class java.time.zone.ZoneRulesProvider
    //openjdk 间歇性会出现这个问题
    public static Date getLocalDate() {
        try {
			LocalDate localDate = LocalDate.now();
			ZoneId zone = ZoneId.systemDefault();
			Instant instant = localDate.atStartOfDay().atZone(zone).toInstant();
			return Date.from(instant);
		} catch (Throwable e) {
			e.printStackTrace();
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			return cal.getTime();
		}
    }

    public static String getStringDate(String pattern) {
        if (StringUtils.isNotEmpty(pattern)) {
            return DateTimeFormatter.ofPattern(pattern).format(LocalDateTime.now());
        }
        return null;
    }

    public static String getStringDate(String pattern, Date date) {
        if (StringUtils.isNotEmpty(pattern) && date != null) {
            return DateTimeFormatter.ofPattern(pattern).format(LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault()));
        }
        return null;
    }

    public static Date convertStringToDate(String pattern, String dateStr) {
        Date date = null;
        if (StringUtils.isNotEmpty(pattern) && StringUtils.isNotEmpty(dateStr)) {
            LocalDate parseDate = LocalDate.from(DateTimeFormatter.ofPattern(pattern).parse(dateStr));
            ZoneId zoneId = ZoneId.systemDefault();
            ZonedDateTime zdt = parseDate.atStartOfDay(zoneId);
            date = Date.from(zdt.toInstant());
        }
        return date;
    }

    public static long getTimeStamp() {
        return LocalDateTime.now().toEpochSecond(ZoneOffset.of("+8"));
    }

    public static Date subStractDateByMonths(Date date,int month){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MONTH, -month);//当前时间前去一个月，即一个月前的时间
        Date subStractDate = calendar.getTime();
        return subStractDate;
    }

    public static boolean isEndMonthOfQuarter(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int month = calendar.get(Calendar.MONTH);
        Integer[] quarters = {2, 5, 8, 11};
        return Arrays.asList(quarters).contains(month);
    }

    public static boolean isEndMonthOfYear(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int month = calendar.get(Calendar.MONTH);
        return month == 11;
    }

    public static Date getEndDateTimeOfMonth(String monthly) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, Integer.valueOf(monthly.substring(0, 4)));
        calendar.set(Calendar.MONTH, Integer.valueOf(monthly.substring(4)) - 1);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        setMaxTime(calendar);
        return calendar.getTime();
    }

    public static Date getStartDateTimeOfMonth(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        setMinTime(calendar);
        return calendar.getTime();
    }

    public static Date getEndDateTimeOfMonth(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        setMaxTime(calendar);
        return calendar.getTime();
    }

    public static Date getStartDateTimeOfYear(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.MONTH, calendar.getActualMinimum(Calendar.MONTH));
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        setMinTime(calendar);
        return calendar.getTime();
    }

    public static Date getEndDateTimeOfYear(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.MONTH, calendar.getActualMaximum(Calendar.MONTH));
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        setMaxTime(calendar);
        return calendar.getTime();
    }

    public static Date getStartDateTimeOfQuarter(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.MONTH, ((int) calendar.get(Calendar.MONTH) / 3) * 3);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        setMinTime(calendar);
        return calendar.getTime();
    }

    public static Date getEndDateTimeOfQuarter(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.MONTH, ((int) calendar.get(Calendar.MONTH) / 3) * 3 + 2);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        setMaxTime(calendar);
        return calendar.getTime();
    }

    private static void setMinTime(Calendar calendar){
        calendar.set(Calendar.HOUR_OF_DAY, calendar.getActualMinimum(Calendar.HOUR_OF_DAY));
        calendar.set(Calendar.MINUTE, calendar.getActualMinimum(Calendar.MINUTE));
        calendar.set(Calendar.SECOND, calendar.getActualMinimum(Calendar.SECOND));
        calendar.set(Calendar.MILLISECOND, calendar.getActualMinimum(Calendar.MILLISECOND));
    }

    private static void setMaxTime(Calendar calendar){
        calendar.set(Calendar.HOUR_OF_DAY, calendar.getActualMaximum(Calendar.HOUR_OF_DAY));
        calendar.set(Calendar.MINUTE, calendar.getActualMaximum(Calendar.MINUTE));
        calendar.set(Calendar.SECOND, calendar.getActualMaximum(Calendar.SECOND));
        calendar.set(Calendar.MILLISECOND, calendar.getActualMinimum(Calendar.MILLISECOND));
    }

    public static Date getStartTimeOfDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        setMinTime(calendar);
        return calendar.getTime();
    }

    public static Date getEndTimeOfDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        setMaxTime(calendar);
        return calendar.getTime();
    }

    /**
     * 判断该日期是否是该月的最后一天
     *
     * @param date
     *            需要判断的日期
     * @return
     */
    public static boolean isLastDayOfMonth(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.DAY_OF_MONTH) == calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
    }
    
    /**
     * 判断某个日期是否在指定时间段内
     * @author tz.x
     * @date 2018年6月26日下午4:22:05
     */
    public static boolean isBetween2Dates(Date currentDate, Date beginDate, Date endDate) {
    	if (currentDate.before(beginDate) || currentDate.after(endDate)) {
    		return false;
    	}
    	return true;
    }
    
    /**
     * 计算两个日期之间的时间差
     * @author tz.x
     * @date 2018年7月7日上午10:05:14
     */
    public static String getDatePoor(Date endDate, Date nowDate) {
        long nd = 1000 * 24 * 60 * 60;
        long nh = 1000 * 60 * 60;
        long nm = 1000 * 60;
        // long ns = 1000;
        // 获得两个时间的毫秒时间差异
        long diff = endDate.getTime() - nowDate.getTime();
        // 计算差多少天
        long day = diff / nd;
        // 计算差多少小时
        long hour = diff % nd / nh;
        // 计算差多少分钟
        //long min = diff % nd % nh / nm;
        // 计算差多少秒//输出结果
        // long sec = diff % nd % nh % nm / ns;
        return String.format("%d天，%d小时", day, hour);
    }
    
    /**
     * 校验字符是否符合日期格式
     * @author tz.x
     * @date 2018年7月31日下午1:54:51
     */
    public static boolean validateDateStrFormat(String date) {
    	//String rexp = "^((\\d{2}(([02468][048])|([13579][26]))[\\-\\/\\s]?((((0?[13578])|(1[02]))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])))))|(\\d{2}(([02468][1235679])|([13579][01345789]))[\\-\\/\\s]?((((0?[13578])|(1[02]))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-\\/\\s]?((0?[1-9])|(1[0-9])|(2[0-8]))))))";  
    	String rexp1 = "(([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3})-(((0[13578]|1[02])-(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)-(0[1-9]|[12][0-9]|30))|(02-(0[1-9]|[1][0-9]|2[0-8]))))|((([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00))-02-29)|(([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3})(((0[13578]|1[02])(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)(0[1-9]|[12][0-9]|30))|(02(0[1-9]|[1][0-9]|2[0-8]))))|((([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00))0229)";
    	Pattern pat = Pattern.compile(rexp1);    
        Matcher mat = pat.matcher(date);    
        return mat.matches(); 
    }
    public static Date convertStrToDate(Date date,String pattern){
        if(pattern==null){
            pattern="yyyy-MM-dd";
        }
        try{
            SimpleDateFormat sdf=new SimpleDateFormat(pattern);
            String str=sdf.format(date);
            return sdf.parse(str);
        }catch (Exception e){
            return null;
        }
    }
    public static String convertDateToStr(Date date,String pattern){
        if(pattern==null){
            pattern="yyyy-MM-dd";
        }
        try{
            SimpleDateFormat sdf=new SimpleDateFormat(pattern);
            return sdf.format(date);
        }catch (Exception e){
            return null;
        }
    }
    public static Date convertStrToDate24(Date date,String pattern){
        if(pattern==null){
            pattern="yyyy-MM-dd HH:mm:ss";
        }
        try{
            SimpleDateFormat sdf=new SimpleDateFormat(pattern);
            String str=convertDateToStr(date,null)+" 23:59:59:59";
            Date newDate=sdf.parse(str);
            return newDate;
        }catch (Exception e){
            return null;
        }
    }
    
    /**
     * @author zhaokang 
     * @Description 获取当前月第一天
     * @date 2018年11月15日
     */
    public static Date getLocalDateFirstOfMonth() {
    	return getDateFirstOfMonth(new Date());
    }
    
    /**
     * @author zhaokang 
     * @Description 获取指定日期的月份的第一天
     * @date 2018年11月15日
     */
    public static Date getDateFirstOfMonth(Date date) {
    	return Date.from(convertDateToLocalDate(date).with(TemporalAdjusters.firstDayOfMonth()).atStartOfDay()
				.atZone(ZoneId.systemDefault()).toInstant());
    }
    public static Date addMinutes(Date date, int minutes) {
        return add(date, 12, minutes);
    }
    private static Date add(Date date, int field, int amount) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(field, amount);
        return cal.getTime();
    }
    /**
     * @author zhaokang 
     * @Description 获取指定日期的月份的最后一天
     * @date 2018年11月15日
     */
    public static Date getDateEndOfMonth(Date date) {
    	return Date.from(convertDateToLocalDate(date).with(TemporalAdjusters.lastDayOfMonth()).atStartOfDay()
				.atZone(ZoneId.systemDefault()).toInstant());
    }
    
    //localDate和date互相转换
    private static LocalDate convertDateToLocalDate(Date date) {
    	return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault()).toLocalDate();
    } 
    
    /**
     * @author zhaokang 
     * @Description 获取当前日的月份
     * @date 2018年11月19日
     */
    public static Month getMonthOfDate(Date date) {
    	return convertDateToLocalDate(date).getMonth();
    }


    public static List<Date> getBetweenDates(Date start, Date end) {
        List<Date> days = new ArrayList<Date>();
        Calendar tempStart = Calendar.getInstance();
        tempStart.setTime(start);

        Calendar tempEnd = Calendar.getInstance();
        tempEnd.setTime(end);
        tempEnd.add(Calendar.DATE, +1); // 日期加1(包含结束)
        while (tempStart.before(tempEnd)) {
            days.add(tempStart.getTime());
            tempStart.add(Calendar.DAY_OF_YEAR, 1);
        }
        return days;
    }

    public static Date randomDate(Date beginDate,Date endDate){
        try {
       /*     SimpleDateFormat format = new SimpleDateFormat(PATTER_TYPE_FIVE_24);
            Date start = format.parse(beginDate);
            Date end = format.parse(endDate);*/
            if(beginDate.getTime() >= endDate.getTime()){
                return null;
            }
            long date = random(beginDate.getTime(),endDate.getTime());
            return new Date(date);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    private static long random(long begin,long end){
        long rtn = begin + (long)(Math.random() * (end - begin));
        if(rtn == begin || rtn == end){
            return random(begin,end);
        }
        return rtn;
    }
    public static String format(Date date, String pattern) {
        if (date == null) {
            return null;
        } else {
            FastDateFormat df = FastDateFormat.getInstance(pattern);
            return df.format(date);
        }
    }
    public static void main(String[] args) {
        Date s=new Date();

        String s2=format(randomDate(s,addMinutes(s,30)),PATTER_TYPE_FIVE_24);
    	System.out.println(s2);

    }
	
}
