package com.ykcloud.soa.erp.common.enums;

/**
 * @Author joe.zhong
 * @Date 2021/10/14/11:18
 * @Description:
 * @Version 1.0
 */
public enum OrderStatusEnum {
    INVALID("未生成", 1L),
    CONFIRM("已确认", 2L),
    POSTED("已过帐", 3L),
    APPROVE("已审核", 4L);

    private OrderStatusEnum(String desc, Long value){
        this.desc = desc;
        this.value = value;
    }

    private String desc;
    private Long value;

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }
}
