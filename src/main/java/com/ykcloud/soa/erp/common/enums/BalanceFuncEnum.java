package com.ykcloud.soa.erp.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;


/**
 * /**
 *
 * @description:
 * @author: Dan
 * @time: 2020/9/1 16:23
 */

@AllArgsConstructor
public enum BalanceFuncEnum {

    // 1验收单
    RECEIPT_1_11001("RECEIPT",1l,11001L, "收货单收货确认"),
    RECEIPT_1_11002("RECEIPT",1l,11002L, "验收完成"),
    RECEIPT_1_11003("RECEIPT",1l,11003L, "短收(拒收)"),
    //2上架单
    SHELVES_2_21001("shelves",1l,21001L, "wms单行回传，上架完成"),
    SHELVES_2_21002("shelves",2l,21002L, "wms单行回传，上架驳回"),

    //3拣货单
    PICK_3_31001("PICK",1l,31001L, "生成拣货单库位库存锁库"),
    PICK_3_31002("PICK",1l,31002L, "生成拣货单数量小于调拨单释放仓库库存"),
    PICK_3_31003("PICK",1l,31003L, "短拣释放库存"),

    //4移库单
    MOVE_4_41001("MOVE",1l,41001L, "生成移库单锁移出库存库存"),
    MOVE_4_41002("MOVE",2l,41002L, "移出库位减实物、预出库存"),

    MOVE_4_41004("MOVE",2l,41004L, "移出库位减实物、可用库存"),


    MOVE_4_41003("MOVE",3l,41003L, "移入库位加实物、可用库存"),

    //出库单

    MOVE_5_51001("SHIP",1l,51001L, "出库单减库存"),


    DISTAPPROVAL_6_61001("DISTAPPROVAL",1l,61001L, "配送审批单锁库"),
    CO_7_71001("CO",1l,71001L, "销售订单单锁库"),

    SO_8_84001("SO",4l,71001L, "门店退连锁短发释放库存"),
    SO_8_85001("SO",4l,71001L, "门店退批发短发释放库存"),
    SO_8_86001("SO",4l,71001L, "同连锁店间调拨短发释放库存"),
    SO_8_87001("SO",4l,71001L, "跨连锁店间调拨短发释放库存"),

    ;

    @Getter
    @Setter
    private String billTypeNumId;

    @Getter
    @Setter
    private Long typeNumId;

    @Getter
    @Setter
    private Long  balanceFuncId;

    @Getter
    @Setter
    private String desc;

    public static Long getBalanceFuncBybillTypeNumIdAndTypeNumId(String billTypeNumId, Long typeNumId) {
        Integer type = Integer.parseInt(typeNumId.toString());
        Long balanceFunc = null;
        for (BalanceFuncEnum billBusinessTypeEnum : BalanceFuncEnum.values()) {
            if (billBusinessTypeEnum.billTypeNumId.equals(billTypeNumId)) {
                if (billBusinessTypeEnum.typeNumId.equals(type)) {
                    balanceFunc = billBusinessTypeEnum.getBalanceFuncId();
                    break;
                }
            }
        }
        return balanceFunc;
    }

}
