package com.ykcloud.soa.erp.common.enums;

import lombok.Getter;
import lombok.Setter;

/**
 * @author tz.x
 * @date 2020/9/24 20:54
 */
public enum BillTypeMappingEnum {

    REBATE("REBATE", 7, "返利单"),
    CREDIT("CREDIT", 1, "授信单"),
    CO("CO", 2, "销售订单"),
    INVOICE("INVOICE", 5, "发票"),
    RECEIPT("RECEIPT", 6, "收款单"),
    SHIP("SHIP", 3, "出库单");

    @Getter
    @Setter
    private String billTypeNumId;
    @Getter
    @Setter
    private Integer billType;
    @Getter
    @Setter
    private String desc;

    BillTypeMappingEnum(String billTypeNumId, Integer billType, String desc) {
        this.billTypeNumId = billTypeNumId;
        this.billType = billType;
        this.desc = desc;
    }

    public static Integer getBillType(String billTypeNumId) {
        Integer billType = null;
        for (BillTypeMappingEnum billTypeMappingEnum : BillTypeMappingEnum.values()) {
            if (billTypeMappingEnum.billTypeNumId.equals(billTypeNumId)) {
                billType = billTypeMappingEnum.billType;
                break;
            }
        }
        return billType;
    }

    public static String getBillTypeDesc(Integer billType) {
        String desc = null;
        for (BillTypeMappingEnum billTypeMappingEnum : BillTypeMappingEnum.values()) {
            if (billTypeMappingEnum.billType.equals(billType)) {
                desc = billTypeMappingEnum.desc;
                break;
            }
        }
        return desc;
    }
}
