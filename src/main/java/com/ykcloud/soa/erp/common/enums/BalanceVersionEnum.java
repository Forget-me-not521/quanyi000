package com.ykcloud.soa.erp.common.enums;

/**
 * 结算版本,0为到明细,1为到汇总
 *
 * @author Sealin
 * @date 2018-06-27
 */
public enum BalanceVersionEnum {
    DETAIL(0L, "明细"),
    SUMMARY(1L, "汇总");
    
    public final Long ID;
    public final String COMMENT;

    BalanceVersionEnum(Long id, String comment) {
        ID = id;
        COMMENT = comment;
    }
}
