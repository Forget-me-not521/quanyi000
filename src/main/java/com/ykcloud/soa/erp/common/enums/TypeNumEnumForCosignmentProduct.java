package com.ykcloud.soa.erp.common.enums;

/**
 * 代销商品款调整业务类型编号
 *
 * @author Sealin
 * @date 2018-07-13
 */
public enum TypeNumEnumForCosignmentProduct {
    DIRECT_SHOP(2L,"直通到店"),
    DISTRIB_SHOP(5L,"配送到店入库"),
    DISTRIB_TRANSFER_SHOP(7L,"配送分拨到店入库"),
    DIRECT_TRANSFER_SHOP(17L, "直通分拨到店入库"),
    TRANSFER_SHOPS(8L, "店间调拨");
    public final Long ID;
    public final String COMMENT;

    TypeNumEnumForCosignmentProduct(Long id, String comment) {
        ID = id;
        COMMENT = comment;
    }
}
