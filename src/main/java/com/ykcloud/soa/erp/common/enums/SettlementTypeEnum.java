package com.ykcloud.soa.erp.common.enums;

/**
 * @Author Hewei
 * @Date 2018/4/24 15:44
 */
public enum SettlementTypeEnum {
    PURCHASE_Sale(1l,"购销"),
    SELL_OFF(2l,"代销"),
    JOINT_SALE(3l,"联销");

    SettlementTypeEnum(Long settlementTypeId, String settlementTypeName) {
        SettlementTypeId = settlementTypeId;
        SettlementTypeName = settlementTypeName;
    }

    private Long SettlementTypeId;
    private String SettlementTypeName;

    public Long getSettlementTypeId() {
        return SettlementTypeId;
    }

    public void setSettlementTypeId(Long settlementTypeId) {
        SettlementTypeId = settlementTypeId;
    }

    public String getSettlementTypeName() {
        return SettlementTypeName;
    }

    public void setSettlementTypeName(String settlementTypeName) {
        SettlementTypeName = settlementTypeName;
    }

    public static String getSettlementTypeNameBySettlementTypeId(Long settlementTypeId) {
        String settlementTypeName = null;
        for (SettlementTypeEnum settlementTypeEnum : SettlementTypeEnum.values()) {
            if (settlementTypeEnum.SettlementTypeId.equals(settlementTypeId)) {
                settlementTypeName = settlementTypeEnum.SettlementTypeName;
                break;
            }
        }
        return settlementTypeName;
    }
    
    public static boolean isPurchase(Long settlementId) {
    	return PURCHASE_Sale.getSettlementTypeId().equals(settlementId);
    }
    
    public static boolean isSellOff(Long settlementId) {
    	return SELL_OFF.getSettlementTypeId().equals(settlementId);
    }
    
    public static boolean isJoinSale(Long settlementId) {
    	return JOINT_SALE.getSettlementTypeId().equals(settlementId);
    }
    
}
