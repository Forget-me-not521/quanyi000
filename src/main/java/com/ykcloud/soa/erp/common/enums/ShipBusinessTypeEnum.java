
package com.ykcloud.soa.erp.common.enums;

import java.util.ArrayList;
import java.util.List;

/**
 *@Author: Dan
 *@Description:
 *@Date 2020/6/5 13:49
 *
 */
public enum ShipBusinessTypeEnum {

	DIRECT_TO_STORE(1L,"直通到店出库"),
	WAREHOUSE_DIRECT_TO_WAREHOUSE(2L,"直通分拨到仓出库"),
	WAREHOUSE_DIRECT_TO_STORE(3L,"直通分拨到店出库"),
	DISPATCHING_TO_STORE(4L,"配送到店出库"),
	WAREHOUSE_DISPATCHING_TO_WAREHOUSE(5L,"配送分拨到仓出库"),
	WAREHOUSE_DISPATCHING_TO_STORE(6L,"配送分拨到店出库"),
	SHOP_TRANSFER(7L, "店间调拨出库"),
	WAREHOUSE_TRANSFER(8L, "仓间调拨出库"),
	O2O_SEND(10L,"O2O代发出库"),
	ADMINISTRATION_SEND(13L,"行政领料出库"),
	DISPATCHING_RETURN_TO_WAREHOUSE(11L,"反配退仓入库(负数)"),
	DIRECT_RETURN_TO_WAREHOUSE(15L,"直通退仓入库(负数)");

	private Long typeNumId;

	private String typeDesc;


	ShipBusinessTypeEnum(Long typeNumId, String typeDesc) {
		this.typeNumId = typeNumId;
		this.typeDesc = typeDesc;
	}
	
	public Long getTypeNumId() {
		return typeNumId;
	}



	public void setTypeNumId(Long typeNumId) {
		this.typeNumId = typeNumId;
	}



	public String getTypeDesc() {
		return typeDesc;
	}



	public void setTypeDesc(String typeDesc) {
		this.typeDesc = typeDesc;
	}

}
