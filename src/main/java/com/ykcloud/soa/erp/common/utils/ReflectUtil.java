
package com.ykcloud.soa.erp.common.utils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import com.gb.soa.omp.ccommon.util.Assert;

/**
 * @author zk 
 * 反射工具类
 */
public class ReflectUtil {
	
	private static final String excludeFiled = "serialVersionUID";
	
	//private Map<Class<?>,Field[]> fieldCacheMap = new HashMap<>();
	
	/*
	 * 根据属性名获取属性值
	 */
	public static Object getFieldValueByName(String fieldName, Object o) {
		try {
			String firstLetter = fieldName.substring(0, 1).toUpperCase();
			String getter = "get" + firstLetter + fieldName.substring(1);
			Method method = o.getClass().getMethod(getter, new Class[] {});
			Object value = method.invoke(o, new Object[] {});
			return value;
		} catch (Exception e) {
			return null;
		}
	}
	public static void makeAccessible(Field field) {
		if (!Modifier.isPublic(field.getModifiers())) {
			field.setAccessible(true);
		}

	}
	public static Field getDeclaredField(Object object, String filedName) {
		Class superClass = object.getClass();

		while(superClass != Object.class) {
			try {
				return superClass.getDeclaredField(filedName);
			} catch (NoSuchFieldException var4) {
				superClass = superClass.getSuperclass();
			}
		}

		return null;
	}
	public static void setFieldValue(Object object, String fieldName, Object value) {
		Field field = getDeclaredField(object, fieldName);
		if (field == null) {
			throw new IllegalArgumentException("Could not find field [" + fieldName + "] on target [" + object + "]");
		} else {
			makeAccessible(field);

			try {
				field.set(object, value);
			} catch (IllegalAccessException var5) {
				System.out.println("不可能抛出的异常");
			}

		}
	}

	/**
	 * 获取属性名数组
	 */
	private static String[] getFiledName(Object o) {
		Class<?> parent = o.getClass();
		List<String> names = new ArrayList<>();
		while (Objects.nonNull(parent)) {
			final Field[] fields = parent.getDeclaredFields();
			for (int i = 0; i < fields.length; i++) {
				if(fields[i].getName().equals(excludeFiled)){
					continue;
				}
				else{
					names.add(fields[i].getName()); 
				}
				
			}
			parent = parent.getSuperclass();
		}
		//重复属性去重
		Set<String> set = new HashSet<>(names);
		return set.toArray(new String[names.size()]);
	}

	/**
	 *  获取属性值结合
	 */
	public static List<Object> getFieldValues(Object o) {
		String[] filedNames = getFiledName(o);
		List<Object> values = new ArrayList<>();
		for(String name:filedNames){
			values.add(getFieldValueByName(name,o));
		}
		return values;
	}

	public static List<Field> getFieldList(Class<?> clazz){
		List<Field> fieldList = new ArrayList<>();
		
		Class<?> supClazz = clazz;
		while(supClazz!=null){
			fieldList.addAll(Arrays.asList(supClazz.getDeclaredFields()));
			supClazz = supClazz.getSuperclass();
		}
		return fieldList;
		
	}


}
