package com.ykcloud.soa.erp.common.enums;

/**
 * @Author stark.jiang
 * @Date 2021/10/28/21:17
 * @Description:
 * @Version 1.0
 */
public enum ProcessStatusEnum {
    PENDING_APPROVAL(1, "待审批"),
    UNDER_APPROVAL(2, "审批中"),
    APPROVED(3, "审批通过"),
    VETO_APPROVAL (9, "审批否决");
    private Integer statusId;
    private String statusName;

    ProcessStatusEnum(Integer statusId, String statusName) {
        this.statusId = statusId;
        this.statusName = statusName;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public static String getStatusNameByStatudId(Integer statusId) {
        String statusName = null;
        for (ProcessStatusEnum statusEnum : ProcessStatusEnum.values()) {
            if (statusEnum.statusId==statusId) {
                statusName = statusEnum.statusName;
                break;
            }
        }
        return statusName;
    }
}
