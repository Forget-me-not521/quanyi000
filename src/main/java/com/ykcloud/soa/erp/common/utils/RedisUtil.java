package com.ykcloud.soa.erp.common.utils;

import com.google.common.base.Joiner;

/**
 * @author tz.x
 * @date 2021/9/1 9:59
 */
public class RedisUtil {

    public final static Long EXPIRE_LONG_TIME = 100000000L;

    public final static Long EXPIRE_ONE_WEEK = 604800L;

    public final static String STO_ITEM_POND_PREFIX = "stoItemPond";

    public final static String TRANSIT_PREFIX_STO = "transitSto";

    public final static String TRANSIT_PREFIX_SUB = "transitSub";

    public final static String REP_SUB_MQ_PREFIX = "repSubMq";

    public final static String REP_STO_MQ_PREFIX = "repStoMq";

    public final static String PHYSICAL_CONFIG_SWITCH_PREFIX = "PHYSICAL_CONFIG_SWITCH";

    public static String getPhysicalConfigSwitchPrefix(Long tenantNumId, Long dataSign,
                                                       String cortNumId, String physiNumId){
        return Joiner.on("_").join(tenantNumId, dataSign, cortNumId, physiNumId, PHYSICAL_CONFIG_SWITCH_PREFIX);
    }
}
