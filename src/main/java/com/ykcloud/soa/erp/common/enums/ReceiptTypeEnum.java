
package com.ykcloud.soa.erp.common.enums;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author gaoyun.shen
 *
 * @date 2018年8月23日 下午9:07:07
 * 
 * @Description 验收单类型,需要更新最新供应商
 */
public enum ReceiptTypeEnum {
	
    DIRECT_SENDING(1l,"直送验收入库"),
    DIRECT_TO_STORE(2l,"直通到店验收入库"),
    DIRECT_TO_WAREHOUSE(4l,"直通到总仓入库"),
    DISPATCHING_TO_STORE(5l,"配送到店入库"),
    WAREHOUSE_DISPATCHING_TO_STORE(7l,"配送分拨到店入库"),
    WAREHOUSE_DIRECT_TO_STORE(17l,"配送分拨到店入库");
	
	private Long typeNumId;
	
	private String typeDesc;


	ReceiptTypeEnum(Long typeNumId,String typeDesc) {
		this.typeNumId = typeNumId;
		this.typeDesc = typeDesc;
	}
	
	public Long getTypeNumId() {
		return typeNumId;
	}



	public void setTypeNumId(Long typeNumId) {
		this.typeNumId = typeNumId;
	}



	public String getTypeDesc() {
		return typeDesc;
	}



	public void setTypeDesc(String typeDesc) {
		this.typeDesc = typeDesc;
	}
	
	public static List<Long> getCodeList() {
		List<Long> typeNumIdList = new ArrayList<>();
		for(ReceiptTypeEnum itemTypeEnum:ReceiptTypeEnum.values()) {
			typeNumIdList.add(itemTypeEnum.getTypeNumId());
		}
		return typeNumIdList;
	}

}
