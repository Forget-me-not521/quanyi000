package com.ykcloud.soa.erp.common.enums;

import java.util.Objects;

/**
 * 订单业务类型
 * @author tz.x
 * @date 2018年5月29日下午4:58:36
 */
public enum SoBusinessTypeEnum {
	
	DIRECT_WAY_TO_SHOP(1L, "直通到店"),
	
	DIRECT_WAY_DISTRIBUTION_TO_SHOP(2L, "直通分拨到店"),
	
	DISTRIBUTION_TO_SHOP(3L, "配送到店"),
	
	DISTRIBUTION_ALLOCATION_TO_STORE(4L, "配送分拨到仓"),
	
	DISTRIBUTION_ALLOCATION_TO_SHOP(5L, "配送分拨到店"),
	
	SHOP_TRANSFER(6L, "店间调拨"),
	
	STORE_TRANSFER(7L, "仓间调拨"),
	
	SELL_WHOLESALE(8L, "批销"),
	
	RETURN_TO_SUPPLYER(9L, "退供应商"),
	
	DIRECT_WAY_RETURN_TO_STORE(10L, "直通退仓"),
	
	DISTRIBUTION_RETURN_TO_STORE(11L, "配送退仓"),
	
	O2O_SEND_GOODS(12L, "o2o发货"),
	
	ENTRUST_DECOMPOSE_COLLAR(13L, "委外分解领用"),
	
	ADMINISTRATION_PICKING(14L, "行政领料"),
	
	REVERSE_MATCHING_ALLOCATION(15L, "反配调拨"),
	
	DIRECT_WAY_DISTRIBUTION_TO_STORE(16L, "直通分拨到仓"),
	
	O2O_SUBSTITUTING_SEND(20L, "o2o代发");
	
	/**
	 * 根据typeNumId获取typeName
	 * @author tz.x
	 * @date 2018年5月30日上午10:58:49
	 */
	public static String getBusinessTypeNameByTypeNumId(Long typeNumId) {
		String businessTypeName = null;
        for (SoBusinessTypeEnum soBusinessTypeEnum : SoBusinessTypeEnum.values()) {
            if (Objects.equals(typeNumId, soBusinessTypeEnum.typeNumName)) {
            	businessTypeName = soBusinessTypeEnum.typeNumName;
                break;
            }
        }
        return businessTypeName;
	}
	
	
	/**
	 * 业务类型
	 */
	private Long typeNumId;
	
	/**
	 * 业务类型名称
	 */
	private String typeNumName;
	
	SoBusinessTypeEnum(Long typeNumId, String typeNumName) {
		this.typeNumId = typeNumId;
		this.typeNumName = typeNumName;
	}

	public Long getTypeNumId() {
		return typeNumId;
	}

	public void setTypeNumId(Long typeNumId) {
		this.typeNumId = typeNumId;
	}

	public String getTypeNumName() {
		return typeNumName;
	}

	public void setTypeNumName(String typeNumName) {
		this.typeNumName = typeNumName;
	}

}
