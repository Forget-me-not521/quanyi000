package com.ykcloud.soa.erp.common.enums;

/**
 * @author Sealin
 * 供应商结算状态
 */
public enum SupBalanceStatusEnum {
    WAIT_CONFIRM_BILL(0L, "待供应商对账"),
    WITHOUT_CONFIRM(1L, "未确认"),
    FIN_AUDITED(2L, "财务审核"),
    BILL_AUDITED(3L, "制单审核"),
    CURRENT_PERIOD_NOT_CLEAR(4L, "本期不结算"),
    SCROLL_NEXT_PERIOD(5L, "已滚动到下期");

    public final Long ID;
    public final String COMMENT;

    SupBalanceStatusEnum(Long id, String comment) {
        this.ID = id;
        this.COMMENT = comment;
    }
}