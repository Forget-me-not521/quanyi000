
package com.ykcloud.soa.erp.common.enums;

/**
 * @author zk
 */
public enum DayEndMethodEnum {
	
	CHECKBILL(1L, "checkBillsForDailyCarry", "wmDailyCarryService", 1L, 1L, "单据检查"),

	UNCONFIRMSTOCKADJUST(2L, "getUnConfirmStockAdjust", "WmStockCheckService", 0L, 1L, "前日盈亏单记账检查"),
	
	ADJUSTWMINVENTORYCOST(3L,"adjustWmInventoryCostForDailyCarry","ScmBlChangeCostService",1L,1L,"批次进价调整"),
	
	CHECKWMADJUSTCOMPLETE(4L,"checkWmAdjustComplete","WmCostAdjustServiceImpl",1L,1L,"批次进价调整完成检查"),
	
	ADJUSTMENTNEGATIVESTOCK(5L,"adjustmentNegativeStockBatch","WmSellDailyServiceImpl",1L,1L,"负库存批次调整"),
	
	ADJUSTMENTNEGATIVESTOCKFORFINANCE(6L,"adjustNegativeBatchForFinance","FiCostDailyAccountGaService",1L,1L,"负库存批次调整入财务账"),
	
	COMPARETMLPRODUCTAMOUNTANDPAYAMOUNT(7L,"compareTmlProductAmountAndPayAmount","SoTmlService",1L,1L,"小票与实收金额校验"),
	
	GENERATEDATAFROMTML(8L,"generateDataFromTml","WmSellDailyServiceImpl",1L,1L,"生成出库日报"),
	
    HANDLESELLDETERMININVENTORY(9L,"handleSellDeterminInventory","WmSellDailyServiceImpl",1L,1L,"日结处理已销定入商品"),
	
	GENERATEDATAFROMTMLFORWMBATCH(10L,"adjustWmBatchForDailyAccount","WmSellDailyServiceImpl",1L,1L,"生成出库日报分配批次"),
	
	GENERATEDATAFROMTMLFORFINANCE(11L,"adjustFinanceBatchForDailyAccount","FiHandleAccountsService",1L,1L,"生成出库日报入财务账"),
	
	GENERATESALESDAILY(12L,"generateSalesDaily","SoTmlService",1L,1L,"销售日报生成"),

    CALCSALEGROSSPROFIT(13L,"calcSaleGrossProfit","FiCostDailyAccountGaService",1L,1L,"毛利倒算"),
   
//    HANDLEPOORINVERTEDEXTRUSION(14L,"handlePoorInvertedExtrusion","FiHandleAccountsService",1L,1L,"处理倒挤差成本"),
    
    GENERATESALESOFSUPPLEMENTARY(14L,"generateSalesOfSupplementary","FiHandleAccountsService",1L,1L,"销售补差,生成供应商往来"),

	SENDDAILYMESSAGE(15L,"sendDailyMessage","FiHandleAccountsService",1L,1L,"发送消息生成扣项和报损单"),

	CHECKSHIPLOSS(16L,"checkShipLoss","wmShipService",1L,1L,"报损单入账检查"),

	CARRYPHYSICALDAILYACCOUNT(17L,"carryPhysicalDailyAccount","WmDailyCarryServiceImpl",1L,1L,"物理仓结转"),
    
    CARRYCOSTDAILYACCOUNT(18L,"carryCostDailyAccount","FiCostDailyAccountGaService",1L,1L,"商品成本结转"),
    
    CARRYOVERGOODSMOVEWEIGHTING(19L,"carryOverGoodsMoveWeighting","FiCostDailyAccountGaService",1L,1L,"移动加权结转"),
    
    CLEARDAILYCOSTAVAILABLENUM(20L,"clearDailyCostAvailableNum","InventorySupplyChainService",1L,1L,"日结的时候可用中的预入数清零"),

    BATCHCANCELEXPIREPO(21L,"batchCancelExpirePo","ScmPoService",0L,0L,"超期采购单关闭"),
    
    SENDMESSAGEFORREFRESHPRICE(22L,"sendMessageForDailyCarryRefreshPriceAndCost","prdSaleService",1L,1L,"刷新进售价,以及7天售价"),
    
    DELETEUNADUITAPPLY(23L,"deleteUnaduitApply","ScmReplenishService",1L,1L,"删除未审核单据"),
    
//    BUILDMONTHLYSETTLEMENTRECORD(25L,"buildMonthlySettlementRecord","WmReceiptInAccountTSCService",1L,1L,"物理仓月进销月结"),
    
//    BUILBATCHDMONTHLYSETTLEMENTRECORD(26L,"buildBatchMonthSalesMonthlySettlementRecord","FiBillInAccountService",1L,1L,"批次月进销，月结"),
    
//    BUILDITEMMONTHSALESMONTHLYSETTLEMENTRECORD(27L,"buildItemMonthSalesMonthlySettlementRecord","FiBillInAccountService",1L,1L,"商品成本月进销存结转"),
    

    // BUILDSENDPRODUCTSALESMONTHLYSETTLEMENTRECORD(28L,"buildSendProductSalesMonthlySettlementRecord","FiBillInAccountService",1L,1L,"发出商品，月结"),
    
    // BUILDSENDPRODUCTCONSIGNMENTSALESMONTHLYSETTLEMENTRECORD(29L,"buildSendProductConsignmentSalesMonthlySettlementRecord","FiBillInAccountService",1L,1L,"发出代销商品款"),

//    BUILDCONSIGNMENTSALESMONTHLYSETTLEMENTRECORD(30L,"buildConsignmentSalesMonthlySettlementRecord","FiBillInAccountService",1L,1L,"代销商品款月结"),
	
//    SOLESALEGROSSPROFIT(31L,"soleSaleGrossProfit","FiCostDailyAccountGaService",1L,1L,"代销商品款日更新"),
    
//	ADJUSTNEGATIVEINVENTORY(32L,"adjustNegativeInventory","FiCostDailyAccountGaService",1L,1L,"代销商品款日负库存调整"),
	
//	CARRYOVERSALEGOODS(33L,"carryOverSaleGoods","FiConsignmentInAccountService",1L,1L,"代销商品款日结转"),

	CARRYBATCHDAILY(24L,"carryFiBatchDailyAccountGa","FiCostDailyAccountGaService",1L,1L,"批次日结转"),

	RATEADJUST(25L,"enterIntoAccountRateAdjust","WmRateAdjustService",1L,1L,"税率调整"),

	GENERATETMLDAILY(26L,"getTmlDailyGenerateForMessage","SoTmlService",1L,1L,"销售日汇总");





	private Long stageNumId;

	private String methodName;

	private String serviceName;

	private Long isMust;

	private Long cancelSign;

	private String remark;

	DayEndMethodEnum(Long stageNumId, String methodName, String serviceName, Long isMust, Long cancelsign,
			String remark) {
		this.stageNumId = stageNumId;
		this.methodName = methodName;
		this.serviceName = serviceName;
		this.isMust = isMust;
		this.cancelSign = cancelsign;
		this.remark = remark;
	}

	public Long getStageNumId() {
		return stageNumId;
	}

	public String getMethodName() {
		return methodName;
	}

	public String getServiceName() {
		return serviceName;
	}

	public Long getIsMust() {
		return isMust;
	}

	public Long getCancelSign() {
		return cancelSign;
	}
	
	public String getRemark() {
		return remark;
	}

}
