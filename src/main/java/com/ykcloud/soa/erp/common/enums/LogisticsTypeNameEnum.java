package com.ykcloud.soa.erp.common.enums;

public enum LogisticsTypeNameEnum {
	
	LOGISTICS_TYPE_DIRECT_SEND(1L, "直送"),
	LOGISTICS_TYPE_DIRECT_WAY(2L, "直通"),
	LOGISTICS_TYPE_DISTRIBUTION(3L, "配送");

	LogisticsTypeNameEnum(Long logisticsTypeId, String logisticsTypeName) {
		LogisticsTypeId = logisticsTypeId;
		LogisticsTypeName = logisticsTypeName;
    }

    private Long LogisticsTypeId;
    private String LogisticsTypeName;

    public Long getLogisticsTypeId() {
		return LogisticsTypeId;
	}

	public void setLogisticsTypeId(Long logisticsTypeId) {
		LogisticsTypeId = logisticsTypeId;
	}


	public String getLogisticsTypeName() {
		return LogisticsTypeName;
	}


	public void setLogisticsTypeName(String logisticsTypeName) {
		LogisticsTypeName = logisticsTypeName;
	}


	public static String getLogisticsTypeNameByLogisticsTypeId(Long logisticsTypeId) {
        String logisticsTypeName = null;
        for (LogisticsTypeNameEnum logisticsTypeNameEnum : LogisticsTypeNameEnum.values()) {
            if (logisticsTypeNameEnum.LogisticsTypeId.equals(logisticsTypeId)) {
            	logisticsTypeName = logisticsTypeNameEnum.LogisticsTypeName;
                break;
            }
        }
        return logisticsTypeName;
    }

}
