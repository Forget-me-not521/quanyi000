package com.ykcloud.soa.erp.common.enums;

/**
 * @Author Hewei
 * @Date 2018/3/31 17:20
 */
public enum BillBusinessTypeEnum {
    RECEIPT("1", "RECEIPT", "WM_BL_RECEIPT_BUD_HDR", "验收入库单"),
    SO("2", "SO", "SD_BL_SO_HDR", "销售订单"),
    CONTAINER("3", "CONTAINER", "WM_BL_CONTAINER_DTL", "出库装箱单"),
    SHIP("4", "SHIP", "WM_BL_SHIP_HDR", "出库单"),
    STOCKADJUST("5", "STOCKADJUST", "WM_BL_STOCKADJUST_HDR", "盘盈盘亏单"),
    WORK("6", "WORK", "WM_BL_WORK_HDR", "加工单"),
    MOVE("7", "MOVE", "WM_BL_MOVE_HDR", "移库单"),
    LOSS("8", "LOSS", "WM_BL_LOSS_HDR", "损溢单"),
    CHANGECOST("9", "CHANGECOST", "SCM_BL_CHANGE_COST", "进价调整"),
    CHANGESTOCKCOST("10", "CHANGESTOCKCOST", "SCM_BL_CHANGE_STOCK_COST", "库存调整"),
    DISTAPPROVAL("11", "DISTAPPROVAL", "SCM_BL_DIST_APPROVAL_HDR", "配送审批单"),
    SOTML("12", "SOTML", "SD_BL_SO_TML_HDR", "销售小票"),
    RETURNAPPROVAL("13", "RETURNAPPROVAL", "SCM_BL_RETURN_APPROVAL_HDR", "退货审批单"),
    SOAPPLY("14", "SOAPPLY", "SD_BL_SO_APPLY_HDR", "SO申请单"),
    SELLDAILY("15", "SELLDAILY", "WM_BL_SHIP_SELLDAILY_HDR", "出库日报"),
    BALANCE("16", "BALANCE", "FI_BL_SUP_BALANCE_HDR", "供应商结算单"),
    PAY("17", "PAY", "FI_BL_SUP_PAY_HDR", "付款单"),
    PREPAY("18", "PREPAY", "FI_BL_SUP_ADV_INF", "预付款"),
    ITEMSELLDAILY("19", "ITEMSELLDAILY", "SD_BL_SO_TML_ITEM_SELLDAILY_HDR", "销售日报"),
    COSTADJUST("20","COSTADJUST","WM_BL_COSTADJUST_HDR ","库存金额调整"),
    RECEIPTMONEY("21","RECEIPTMONEY","FI_BL_SUP_RECEIPT_MONEY_HDR ","现金收据"),
    DEALING("22","DEALING","FI_BL_DEALING_HDR ","供应商往来"),
    SUPPLIERBOND("23","SUPPLIERBOND","FI_SUPPLIER_BOND ","供应商保证金"),
    WORKAMOUNT("24","WORKAMOUNT","WM_BL_WORK_HDR ","委外加工费"),
    BILLINFO("25", "ITEMSELLDAILY", " FI_BL_SUP_BILL_INFO", "发票接收单"),
    CUSTBALANCE("26","CUSTBALANCE","FI_BL_CUST_BALANCE_HDR","客户结算单"),
    CUSTPAY("28","CUSTPAY","FL_BL_CUST_PAY_HDR","收款单"),
    STOCKTAKING("29","STOCKTAKING","WM_BL_STOCKTAKING_HDR","盘点单"),
    STOCKTAKINGTASK("30","STOCKTAKINGTASK","WM_BL_STOCKTAKING_TASK_HDR","盘点单任务单"),
	TMLDAILYHDR("31","TMLDAILYHDR","SD_BL_SO_TML_DAILY_HDR","销售小票日汇总"),
	RATEADJUST("32","RATEADJUST","WM_BL_RATEADJUST_HDR","税率调整单"),
    TYPEADJUST("33","TYPEADJUST","WM_BL_TYPEADJUST_HDR","品类调整单"),
    CO("34","CO","sd_bl_co_hdr","B2B销售订单"),
    CUS_BALANCE("35","CUS_BALANCE","fi_bl_cus_balance_hdr","B2B销售结算单"),
    REBATE("36","REBATE","fi_bl_rebate_hdr","B2B返利单"),
    INVOICE("37","INVOICE","fi_bl_sales_invoice_hdr","B2B销售发票"),
    SHELVES("38","SHELVES","wm_bl_shelves_hdr","上架单"),
    PICK("39","PICK","wm_bl_pick_hdr","拣货单");
    private String typeNumId;
    private String billTypeNumId;
    private String accountName;
    private String billTypeNumName;

    BillBusinessTypeEnum(String typeNumId, String billTypeNumId, String accountName, String billTypeNumName) {
        this.typeNumId = typeNumId;
        this.billTypeNumId = billTypeNumId;
        this.accountName = accountName;
        this.billTypeNumName = billTypeNumName;
    }

    public static String getBillTypeNumIdByTypeNumId(String typeNumId) {
        String billTypeNumId = null;
        for (BillBusinessTypeEnum billBusinessTypeEnum : BillBusinessTypeEnum.values()) {
            if (billBusinessTypeEnum.typeNumId.equals(typeNumId)) {
                billTypeNumId = billBusinessTypeEnum.billTypeNumId;
                break;
            }
        }
        return billTypeNumId;
    }

    public static String getAccountNameByTypeNumId(String typeNumId) {
        String accountName = null;
        for (BillBusinessTypeEnum billBusinessTypeEnum : BillBusinessTypeEnum.values()) {
            if (billBusinessTypeEnum.typeNumId.equals(typeNumId)) {
                accountName = billBusinessTypeEnum.accountName;
                break;
            }
        }
        return accountName;
    }

    public static String getTypeNumIdByBillTypeNumId(String billTypeNumId) {
        String typeNumId = null;
        for (BillBusinessTypeEnum billBusinessTypeEnum : BillBusinessTypeEnum.values()) {
            if (billBusinessTypeEnum.billTypeNumId.equals(billTypeNumId)) {
                typeNumId = billBusinessTypeEnum.typeNumId;
                break;
            }
        }
        return typeNumId;
    }
    public static String getBillTypeNumNameByTypeNumId(String typeNumId) {
        String billTypeNumName = null;
        for (BillBusinessTypeEnum billBusinessTypeEnum : BillBusinessTypeEnum.values()) {
            if (billBusinessTypeEnum.typeNumId.equals(typeNumId)) {
                billTypeNumName = billBusinessTypeEnum.billTypeNumName;
                break;
            }
        }
        return billTypeNumName;
    }

    public String getTypeNumId() {
        return typeNumId;
    }

    public void setTypeNumId(String typeNumId) {
        this.typeNumId = typeNumId;
    }

    public String getBillTypeNumId() {
        return billTypeNumId;
    }

    public void setBillTypeNumId(String billTypeNumId) {
        this.billTypeNumId = billTypeNumId;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getBillTypeNumName() {
        return billTypeNumName;
    }

    public void setBillTypeNumName(String billTypeNumName) {
        this.billTypeNumName = billTypeNumName;
    }

    public static void main(String[] args) {
        System.out.println(getBillTypeNumNameByTypeNumId("1"));
    }
}
