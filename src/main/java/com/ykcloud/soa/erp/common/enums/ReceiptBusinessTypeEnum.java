
package com.ykcloud.soa.erp.common.enums;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author gaoyun.shen
 *
 * @date 2018年8月23日 下午9:07:07
 * 
 * @Description 验收单类型,需要更新最新供应商
 */
public enum ReceiptBusinessTypeEnum {

    DIRECT_SENDING(1l,"直送验收入库"),
    DIRECT_TO_STORE(2l,"直通到店验收入库"),
	WAREHOUSE_DIRECT_TO_WAREHOUSE(3l,"直通分拨到仓"),
    DIRECT_TO_WAREHOUSE(4l,"直通到总仓入库"),
    DISPATCHING_TO_STORE(5l,"配送到店入库"),
	SHOP_TRANSFER(8l,"店间调拨"),
	WAREHOUSE_TRANSFER(9L, "仓间调拨入库"),
	DISPATCHING_RETURN_TO_WAREHOUSE(11l,"反配退仓出库(负数)"),
	DIRECT_RETURN_TO_WAREHOUSE(10l,"直通退仓出库(负数)"),
	RETURN_TO_SUPPLY(15l,"退供应商出库(负数)");

	private Long typeNumId;

	private String typeDesc;


	ReceiptBusinessTypeEnum(Long typeNumId, String typeDesc) {
		this.typeNumId = typeNumId;
		this.typeDesc = typeDesc;
	}
	
	public Long getTypeNumId() {
		return typeNumId;
	}



	public void setTypeNumId(Long typeNumId) {
		this.typeNumId = typeNumId;
	}



	public String getTypeDesc() {
		return typeDesc;
	}



	public void setTypeDesc(String typeDesc) {
		this.typeDesc = typeDesc;
	}
	

}
