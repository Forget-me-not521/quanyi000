package com.ykcloud.soa.erp.common.enums;

/**
 * 调整结果表业务类型
 *
 * @author Sealin
 * @date 2018-07-02
 */
public enum TypeNumEnumForAdjustResult {
    /**
     * 代销商品款调整
     */
    CONSIGNMENT_GOODS_COST("代销商品款调整", 1L),
    /**
     * 商品成本日月进销存调整
     */
    GOODS_COST("商品成本调整", 2L),
    /**
     * 发出代销商品调整
     */
    CONSIGNMENT_EMIT_GOODS("发出代销商品调整", 3L),
    /**
     * 发出商品调整(购销)
     */
    EMIT_GOODS("发出商品调整", 4L),
    /**
     * 批次日月进销存调整
     */
    BATCH("批次进销存调整", 5L);

    public final String COMMENT;
    public final Long ID;
    TypeNumEnumForAdjustResult(String comment, Long id) {
        COMMENT = comment;
        ID = id;
    }
}
