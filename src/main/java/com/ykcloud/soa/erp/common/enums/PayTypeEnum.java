package com.ykcloud.soa.erp.common.enums;

/**
 * @Author:ALi
 * @Description: 结算方式
 * @Date:
 */
public enum PayTypeEnum {
    BALANCE_DAY(1L, "日"),
    BALANCE_WEEK(2L, "周"),
    BALANCE_HALF_MONTH(3L, "半月"),
    BALANCE_MONTH(4L, "月"),
    BALANCE_TWO_MONTH(5L, "双月");

    public final Long payTypeId;
    public final String payTypeName;

    PayTypeEnum(Long payTypeId, String payTypeName) {
        this.payTypeId = payTypeId;
        this.payTypeName = payTypeName;
    }

}
