package com.ykcloud.soa.erp.common.enums;

/**
 * @Author stark.jiang
 * @Date 2021/11/03/14:32
 * @Description:
 * @Version 1.0
 */
public enum ShopLogFromTypeEnum {

    GROUP(1, "集团级价格组"),
    CHAIN(2, "连锁级价格组"),
    SHOP_CHANGE_PRICE(3, "门店调价单"),
    ISSUED_PRODUCT(4, "商品下发门店");
    private Integer typeId;
    private String typeName;
    ShopLogFromTypeEnum(Integer typeId, String typeName) {
        this.typeId = typeId;
        this.typeName = typeName;
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public static String getTypeNameByTypeId(Integer typeId) {
        String typeName = null;
        for (ShopLogFromTypeEnum typeEnum : ShopLogFromTypeEnum.values()) {
            if (typeEnum.typeId==typeId) {
                typeName = typeEnum.typeName;
                break;
            }
        }
        return typeName;
    }
}
