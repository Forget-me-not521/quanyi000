package com.ykcloud.soa.erp.common.enums;

/**
 * @Description: 异步处理状态
 * @author: henry.wang
 * @date: 2018/12/11 14:23
 **/
public enum AsyncProcessStatusEnum {
    STATUS_LOCKING(0L,"锁库中"),
    STATUS_PART_ERROR(-99L,"部分错误"),
    STATUS_ALL_ERROR(-1L, "全部错误"),
    STATUS_PROCESSING(-2L, "正在处理"),
    STATUS_SUCCESS(2L,"处理成功");

    private Long value;
    private String desc;

    AsyncProcessStatusEnum(Long value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public static AsyncProcessStatusEnum fromValue(Long value) {
        for (AsyncProcessStatusEnum enumItem : AsyncProcessStatusEnum.values()) {
            if (enumItem.getValue().equals(value)) {
                return enumItem;
            }
        }
        return null;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
