package com.ykcloud.soa.erp.common.enums;

/**
 * @Author stark.jiang
 * @Date 2021/10/14/11:18
 * @Description:
 * @Version 1.0
 */
public enum ProtocolStatusEnum {
    INVALID("未生效", 1),
    ACTIVE("生效中", 2),
    DEACTIVATED("已失效", 3),
    CANCAL("已作废", 4);

    private ProtocolStatusEnum(String desc, Integer typeNumId){
        this.desc = desc;
        this.typeNumId = typeNumId;
    }

    private String desc;
    private Integer typeNumId;

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Integer getTypeNumId() {
        return typeNumId;
    }

    public void setTypeNumId(Integer typeNumId) {
        this.typeNumId = typeNumId;
    }
}
