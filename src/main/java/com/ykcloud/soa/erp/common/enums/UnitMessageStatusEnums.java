package com.ykcloud.soa.erp.common.enums;

public enum UnitMessageStatusEnums {
    UNRELEASE(0, "未发布"),
    RELEASE(1, "已发布");

    private int id;
    private String value;

    UnitMessageStatusEnums(int id, String value) {
        this.id = id;
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static String getEnums(int id) {
        for (UnitMessageStatusEnums item : UnitMessageStatusEnums.values()) {
            if (item.getId() == id) {
                return item.getValue();
            }
        }
        return null;
    }

}
