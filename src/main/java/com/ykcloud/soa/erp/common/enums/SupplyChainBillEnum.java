package com.ykcloud.soa.erp.common.enums;

import java.util.Objects;

/**
　* @description: 供应链业务单据枚举
　* @author zhangzonglu
　* @date 2019-08-23 11:01
　*/
public enum SupplyChainBillEnum {

    BILL_RETURN_APPROVAL(1L, "退货审批单"),
    BILL_APPLY(2L, "门店团购单"),
    PRE_PO(3L, "预采单"),
    PO(4L, "采购单"),
    DIST_APPROVAL(5L, "强配单"),
    DISTRIBUTION_PLAN(6L, "采购配货单"),
    BL_APPLY(7L, "补货单"),
    PO_ADD_RULE(8L, "采购协议加价设置"),
    ONE_APPROVAL(9L, "直送审批单"),
    TWO_APPROVAL(10L, "直通审批单");

    private Long billType;

    private String billTypeName;

    SupplyChainBillEnum(Long billType, String billTypeName) {
        this.billType = billType;
        this.billTypeName = billTypeName;
    }

    public String getBillTypeName(Long billType) {
        String billTypeName = null;
        for (SupplyChainBillEnum supplyChainBillEnum : SupplyChainBillEnum.values()) {
            if (Objects.equals(billType, supplyChainBillEnum.billType)) {
                billTypeName = supplyChainBillEnum.billTypeName;
                break;
            }
        }
        return billTypeName;
    }

    public Long getBillType() {
        return billType;
    }

    public void setBillType(Long billType) {
        this.billType = billType;
    }

    public String getBillTypeName() {
        return billTypeName;
    }

    public void setBillTypeName(String billTypeName) {
        this.billTypeName = billTypeName;
    }
}
