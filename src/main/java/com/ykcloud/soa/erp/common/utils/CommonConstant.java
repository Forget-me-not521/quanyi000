package com.ykcloud.soa.erp.common.utils;

/**
 * @author tz.x
 * @date 2021/9/3 18:44
 */
public class CommonConstant {

    public final static Integer TRANSIT_ADD = 1;

    public final static Integer TRANSIT_REDUCE = 2;

}
