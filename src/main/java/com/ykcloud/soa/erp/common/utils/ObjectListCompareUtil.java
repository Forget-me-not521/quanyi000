package com.ykcloud.soa.erp.common.utils;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 对象队列对比工具
 * @param <T>
 * @author xxw
 */
public class ObjectListCompareUtil<T> {

    private ObjectAssignmentUtil<T> assignmentUtil;

    public ObjectListCompareUtil() {
        assignmentUtil = new ObjectAssignmentUtil<>();
    }


    /**
     * 获取对比队列的描述
     * @param oldList
     * @param newList
     * @param keyFunction
     * @param describeFunction
     * @return
     */
    public List<String> getCompareList(List<T> oldList, List<T> newList, Function<T,String> keyFunction,final Function<T,String> describeFunction){
        final List<String> res = new ArrayList<>();
        final Map<String,T> oldMap = oldList.stream().collect(Collectors.toMap(keyFunction,Function.identity()));
        final Map<String,T> newMap = newList.stream().collect(Collectors.toMap(keyFunction,Function.identity()));
        List<String> codeString = Lists.newArrayList();
        codeString.addAll(oldMap.keySet());
        codeString.addAll(newMap.keySet());
        Map<String,Long> counts = codeString.stream().collect(Collectors.groupingBy(e -> e, Collectors.counting()));
        counts.forEach((code,count)->{
            T oldItem = oldMap.get(code);
            T newItem = newMap.get(code);
            String logInfo = null;
            try {
                logInfo = assignmentUtil.assignment(oldItem,newItem,describeFunction);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if(StringUtils.isNotBlank(logInfo)){
                res.add(logInfo);
            }
        });
        return res;
    }

}
