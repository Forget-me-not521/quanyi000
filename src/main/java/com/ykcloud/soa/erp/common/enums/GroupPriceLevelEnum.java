package com.ykcloud.soa.erp.common.enums;

/**
 * @Author stark.jiang
 * @Date 2021/11/03/9:49
 * @Description:
 * @Version 1.0
 */
public enum GroupPriceLevelEnum {
    GROUP(1, "集团"),
    CHAIN(2, "连锁");

    private Integer levelTypeId;
    private String levelTypeName;

    GroupPriceLevelEnum(Integer levelTypeId, String levelTypeName) {
        this.levelTypeId = levelTypeId;
        this.levelTypeName = levelTypeName;
    }

    public Integer getLevelTypeId() {
        return levelTypeId;
    }

    public void setLevelTypeId(Integer levelTypeId) {
        this.levelTypeId = levelTypeId;
    }

    public String getLevelTypeName() {
        return levelTypeName;
    }

    public void setLevelTypeName(String levelTypeName) {
        this.levelTypeName = levelTypeName;
    }

    public static String getLevelTypeNameByLevelTypeId(Integer levelTypeId) {
        String levelTypeName = null;
        for (GroupPriceLevelEnum statusEnum : GroupPriceLevelEnum.values()) {
            if (statusEnum.levelTypeId==levelTypeId) {
                levelTypeName = statusEnum.levelTypeName;
                break;
            }
        }
        return levelTypeName;
    }
}
