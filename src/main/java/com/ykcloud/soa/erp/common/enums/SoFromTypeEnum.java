package com.ykcloud.soa.erp.common.enums;/**
 * $DESC$
 *
 * @author Sealin
 * Created on 2018-06-22
 */

/**
 * 数据来源
 *
 * @author Sealin
 * Created on 2018-06-22
 */
public enum SoFromTypeEnum {
    MANUAL(1L, "手动录入"),
    AUTO(2L, "服务自动生成");

    public final Long ID;
    public final String COMMENT;

    SoFromTypeEnum(Long id, String comment) {
        this.ID = id;
        this.COMMENT = comment;
    }
}
