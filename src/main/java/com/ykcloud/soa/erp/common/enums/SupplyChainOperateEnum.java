package com.ykcloud.soa.erp.common.enums;

import java.util.Objects;

/**
　* @description: 供应链单据操作类型枚举
　* @author zhangzonglu
　* @date 2019-08-23 11:00
　*/
public enum SupplyChainOperateEnum {

    OPERATE_INSERT(1L, "新增"),
    OPERATE_UPDATE(2L, "修改"),
    OPERATE_DELETE(3L, "删除"),
    OPERATE_CONFIRM(4L, "确认"),
    OPERATE_AUDIT(5L, "审核"),
    OPERATE_REJECT(6L, "驳回"),
    OPERATE_CANCEL(7L, "作废"),
    TIMEOUT_OPERATE_CANCEL(8L, "超时作废"),
    OPERATE_LOCK_INVENTORY(9L, "锁库");

    private Long operateType;

    private String operateTypeName;

    SupplyChainOperateEnum(Long operateType, String operateTypeName) {
        this.operateType = operateType;
        this.operateTypeName = operateTypeName;
    }

    public static String getOperateTypeName(Long operateType) {
        String operateTypeName = null;
        for (SupplyChainOperateEnum supplyChainOperateEnum : SupplyChainOperateEnum.values()) {
            if (Objects.equals(operateType, supplyChainOperateEnum.operateType)) {
                operateTypeName = supplyChainOperateEnum.operateTypeName;
                break;
            }
        }
        return operateTypeName;
    }

    public Long getOperateType() {
        return operateType;
    }

    public void setOperateType(Long operateType) {
        this.operateType = operateType;
    }

    public String getOperateTypeName() {
        return operateTypeName;
    }

    public void setOperateTypeName(String operateTypeName) {
        this.operateTypeName = operateTypeName;
    }
}
