package com.ykcloud.soa.erp.common.enums;

/**
 * @Author Hewei
 * @Date 2018/4/13 16:16
 */
public enum AccountNoEnum {
    ONE("1", "SCM_ACC_INV_OWN_STK", "可用库存"),

    TWO("2", "WM_PHYSICAL_DAILY_ACCOUNT_GA", "物理仓日进销存"),
    THREE("3", "WM_PHYSICAL_MONTH_ACCOUNT_GA", "物理仓月进销存"),

    FOUR("4", "FI_BATCH_DAILY_ACCOUNT_GA", "批次日进销存"),
    FIVE("5", "FI_BATCH_MONTH_ACCOUNT_GA", "批次月进销存"),
    SIX("6", "FI_COST_DAILY_ACCOUNT_GA", "商品成本日进销存"),
    SEVEN("7", "FI_COST_MONTH_ACCOUNT_GA", "商品成本月进销存"),
    EIGHT("8", "FI_CONSIGNMENT_PRODUCT_ACCOUNT_GA", "代销商品款"),

    NINE("9", "FI_ACC_EMIT_GOODS_GA", "发出商品"),
    TEN("10", "FI_ACC_EMIT_CONSIGNMENT_GOODS_GA", "发出代销商品款"),
    ELEVEN("11", "FI_CONSIGNMENT_PRODUCT_ACCOUNT_DAILY_GA", "代销商品款日进销存"),
    TWELVE("12", "SCM_ACC_INV_OWN_STK_BATCH_LOC", "批号库位库存"),;

    private String accountNo;
    private String accountName;
    private String accountNumName;

    AccountNoEnum(String accountNo, String accountName, String accountNumName) {
        this.accountNo = accountNo;
        this.accountName = accountName;
        this.accountNumName = accountNumName;
    }

    public static String getAccountNameByAccountNo(String accountNo) {
        String accountName = null;
        for (AccountNoEnum accountNoEnum : AccountNoEnum.values()) {
            if (accountNoEnum.accountNo.equals(accountNo)) {
                accountName = accountNoEnum.accountName;
                break;
            }
        }
        return accountName;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getAccountNumName() {
        return accountNumName;
    }

    public void setAccountNumName(String accountNumName) {
        this.accountNumName = accountNumName;
    }

    public static void main(String[] args) {
        System.out.println(getAccountNameByAccountNo("1"));
    }
}
