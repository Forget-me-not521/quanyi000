package com.ykcloud.soa.erp.common.utils;



import com.ykcloud.soa.erp.common.annotation.FeildLogTag;
import lombok.extern.slf4j.Slf4j;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;
import org.apache.commons.lang.StringUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.function.Function;

/**
 * 动态代理产生日志
 *
 * @param <T>
 * @author xxw
 */
@Slf4j
public class FieldLogCglib<T> implements MethodInterceptor {

    /**
     * 被代理的对象
     */
    private T target;

    private static final String SET_PREFIX = "set";

    private static final String GET_PREFIX = "get";

    private StringBuilder sb;

    private Function<T,String> function;

    public FieldLogCglib() {
    }

    public FieldLogCglib(Function<T, String> function) {
        this.function = function;
    }

    /**
     * 设置代理对象
     * @param target
     */
    public void setTarget(T target) {
        this.target = target;
        sb = new StringBuilder();
    }

    public void setFunction(Function<T, String> function) {
        this.function = function;
    }

    /**
     * 获取代理对象
     *
     * @return
     */
    public T getProxy() {
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(target.getClass());
        enhancer.setCallback(this);
        return (T) enhancer.create();
    }

    /**
     * 实现回调方法
     *
     * @param obj    代理的对象
     * @param method 被代理对象的方法
     * @param args   参数集合
     * @param proxy  生成的代理类的方法
     * @return
     * @throws Throwable
     */
    @Override
    public Object intercept(Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable {
        //写入日志
        appendLog(method, args);
        Object result = proxy.invokeSuper(obj, args);
        return result;
    }

    /**
     * 写入日志信息
     *
     * @param method
     * @param args
     */
    private void appendLog(Method method, Object[] args) throws Exception {
        String methodName = method.getName();
        Class clazz = target.getClass();
        String className = clazz.getName();
        if (methodName.startsWith(SET_PREFIX)) {
            Object arg = args[0];
            /*String prefix = getPrefix(methodName.charAt(3));*/
            String fieldName =  methodName.substring(3);
            String getMethodName = GET_PREFIX+ methodName.substring(3);
            try {
                Field field = clazz.getDeclaredField(fieldName);
                FeildLogTag tag = field.getAnnotation(FeildLogTag.class);
                if (tag != null) {
                    Method getMethod = clazz.getMethod(getMethodName);
                    Object val = getMethod.invoke(target);
                    if (!checkArgs(arg, val)) {
                        if(tag.isDict()){

                        }else{
                            sb.append("将").append(tag.value()).append("字段由").append(getVal(val)).append("改为")
                            .append(getVal(arg)).append(";");
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                log.error("类:{}获取属性:{}失败,异常{}",className, fieldName,e);
                throw new Exception(String.format("类:%s获取属性:%s失败", className, fieldName));
            }
        }
    }

    /**
     * 获取值的string
     * @param obj
     * @return
     */
    private String getVal(Object obj){
        if(obj == null || (obj instanceof String && StringUtils.isBlank(String.valueOf(obj)))){
            return "空值";
        }
        if(obj instanceof Date){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            try{
                return sdf.format((Date)obj);
            }catch (Exception e){

            }
        }
        return String.valueOf(obj);
    }

    /**
     * 获取首字母
     * @param prefix
     * @return
     */
    private String getPrefix(char prefix){
        if(Character.isLowerCase(prefix)){
            return String.valueOf(prefix).toUpperCase();
        }else{
            return String.valueOf(prefix).toLowerCase();
        }
    }


    private boolean checkArgs(Object o1, Object o2) {
        if (o1 == null && o2 == null) {
            return true;
        } else if (o1 == null || o2 == null) {
            return false;
        }
        return o1.equals(o2);
    }

    /**
     * 获取日志信息
     * @return
     */
    public String getLogInfo() {
        if(sb.length() > 0 && function != null){
            sb.insert(0,function.apply(target));
        }
        return sb.toString();
    }
}
