package com.ykcloud.soa.erp.common.dao;

import lombok.Data;

import java.io.File;
import java.lang.reflect.Field;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class SqlUtil {

    private static final ConcurrentMap<Class<?>, Field[]> CLASS_CONCURRENT_MAP = new ConcurrentHashMap<>();

    public static String generateInsertSql(Class clazz) {
        List<Field> fieldList = new LinkedList<>();
        StringBuilder fieldName = new StringBuilder();
        StringBuilder index = new StringBuilder();
        Class classTemp = clazz;
        do {
            Field[] declaredFields = classTemp.getDeclaredFields();
            for (Field declaredField : declaredFields) {
                declaredField.setAccessible(true);
                if (!declaredField.getName().equalsIgnoreCase("serialVersionUID")) {
                    fieldName.append(declaredField.getName()).append(",");
                    index.append("?,");
                    fieldList.add(declaredField);
                }
            }
            classTemp = classTemp.getSuperclass();
        } while (Objects.nonNull(classTemp) && classTemp != Object.class);
        String insertSql = String.format("insert into %s (%s) values (%s) ",
                clazz.getSimpleName(),
                fieldName.substring(0, fieldName.length() - 1),
                index.substring(0, index.length() - 1));
        CLASS_CONCURRENT_MAP.putIfAbsent(clazz, fieldList.toArray(new Field[0]));
        return insertSql;
    }

    public static <T> Object[] convertEntityInsert(T entity) {
        Field[] fields = CLASS_CONCURRENT_MAP.get(entity.getClass());
        List<Object> args = new LinkedList<>();
        for (Field field : fields) {
            field.setAccessible(true);
            try {
                if (!field.getName().equalsIgnoreCase("serialVersionUID")) {
                    Object o = field.get(entity);
                    args.add(o);
                }
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e.getMessage());
            }
        }
        return args.toArray();
    }

    public static <T> Object[] convertEntityInsertNotNull(T entity) {
        Field[] fields = CLASS_CONCURRENT_MAP.get(entity.getClass());
        List<Object> args = new LinkedList<>();
        for (Field field : fields) {
            field.setAccessible(true);
            try {
                if (!field.getName().equalsIgnoreCase("serialVersionUID")) {
                    Object o = field.get(entity);
                    args.add(Objects.isNull(o) ? 0 : o);
                }
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e.getMessage());
            }
        }
        return args.toArray();
    }


    public static <T> String convertEntityUpdate(T entity, List<Object> args) {
        Field[] fields = CLASS_CONCURRENT_MAP.get(entity.getClass());
        StringBuffer sql = new StringBuffer();
        for (Field field : fields) {
            field.setAccessible(true);
            try {
                Object value = field.get(entity);
                if (Objects.nonNull(value) && !field.getName().equalsIgnoreCase("tenant_num_id")
                        && !field.getName().equalsIgnoreCase("data_sign")
                        && !field.getName().equalsIgnoreCase("cort_num_id")
                        && !field.getName().equalsIgnoreCase("sub_unit_num_id")) {
                    sql.append(field.getName()).append(" = ").append(" ? ").append(" ,");
                    args.add(value);
                }
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e.getMessage());
            }
        }
        return sql.substring(0, sql.length() - 1);
    }


}
