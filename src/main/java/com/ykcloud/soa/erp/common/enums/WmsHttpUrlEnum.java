package com.ykcloud.soa.erp.common.enums;

/**
 * @Auther: Dan
 * @Date: 2019/5/23 14:58
 * @Description:
 */
public enum WmsHttpUrlEnum {
    RECEIPTSEND("RECEIPTSEND","http://47.92.255.0:8400/po/pobillc" , "供应商确认产生验收单"),
    RECEIPTGET("RECEIPTGET","http://47.92.255.0:8400/po/pobillc" , "WMS回写收货数量");

    private String accountTypeName;
    private String accountHttpUrl;
    private String desc;

    WmsHttpUrlEnum(String accountTypeName, String accountHttpUrl, String desc) {
        this.accountTypeName = accountTypeName;
        this.accountHttpUrl = accountHttpUrl;
        this.desc = desc;
    }

    public String getAccountTypeName() {
        return accountTypeName;
    }

    public void setAccountTypeName(String accountTypeName) {
        this.accountTypeName = accountTypeName;
    }

    public String getAccountHttpUrl() {
        return accountHttpUrl;
    }

    public void setAccountHttpUrl(String accountHttpUrl) {
        this.accountHttpUrl = accountHttpUrl;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
