package com.ykcloud.soa.erp.common.enums;

/**
 * @Author stark.jiang
 * @Date 2021/10/18/15:55
 * @Description:
 * @Version 1.0
 */
public enum PriceTypeEnum {
    RETAIL_PRICE(501, "零售价"),
    MEMBER_PRICE(502, "会员价"),
    MEMBER_DAY_PRICE(503, "会员日价"),
    TINY_PRICE(504, "拆零价"),
    HEALTH_CARE_PRICE(505, "医保价"),
    FLOOR_PRICE(506, "最低价");
    private Integer typeId;
    private String typeName;
    PriceTypeEnum(Integer typeId, String typeName) {
        this.typeId = typeId;
        this.typeName = typeName;
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public static String getTypeNameByTypeId(Integer typeId) {
        String typeName = null;
        for (PriceTypeEnum typeEnum : PriceTypeEnum.values()) {
            if (typeEnum.typeId==typeId) {
                typeName = typeEnum.typeName;
                break;
            }
        }
        return typeName;
    }
}
