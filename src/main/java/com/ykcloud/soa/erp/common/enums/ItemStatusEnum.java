package com.ykcloud.soa.erp.common.enums;

public enum ItemStatusEnum {
	
	TO_BE_ACTIVATED(0L, "待启用", "statusZeroSign"),
	NEW(1L, "新品", "statusOneSign"),
	NORMAL(2L, "正常", "statusTwoSign"),
	SUSPENSION_OF_ORDERS(3L, "暂停订货", "statusThreeSign"),
	SUSPENSION_OF_SALES(4L, "暂停销售", "statusFourSign"),
	SUSPENSION_OF_OPERATION(5L, "暂停经营", "statusFiveSign"),
	TO_BE_CLEARED(6L, "待清退", "statusSixSign"),
	CLEANED_UP(7L, "已清退", "statusSevenSign"),
	TERMINAL_SEASONAL(51L, "暂停订货-季节关闭", "statusFiftyOneSign"),
	TERMINAL_SUPPLIER(52L, "暂停订货-供应商缺货", "statusFiftyTwoSign"),
	TERMINAL_ABNORMAL(53L, "暂停订货-合作异常", "statusFiftyThreeSign");

	private Long itemStatus;
	
	private String itemStatusName;
	
	private String itemStatusConfigSign;
	
	ItemStatusEnum(Long itemStatus, String itemStatusName, String itemStatusConfigSign) {
		this.itemStatus = itemStatus;
		this.itemStatusName = itemStatusName;
		this.itemStatusConfigSign = itemStatusConfigSign;
	}
	
	public String getItemStatusConfigSign() {
		return itemStatusConfigSign;
	}

	public void setItemStatusConfigSign(String itemStatusConfigSign) {
		this.itemStatusConfigSign = itemStatusConfigSign;
	}

	public Long getItemStatus() {
		return itemStatus;
	}

	public void setItemStatus(Long itemStatus) {
		this.itemStatus = itemStatus;
	}

	public String getItemStatusName() {
		return itemStatusName;
	}

	public void setItemStatusName(String itemStatusName) {
		this.itemStatusName = itemStatusName;
	}
	
	public static Long getItemStatusByItemStatusConfigSign(String itemStatusConfigSign) {
        Long itemStatus = null;
        for (ItemStatusEnum itemStatusEnum : ItemStatusEnum.values()) {
            if (itemStatusEnum.itemStatusConfigSign.equals(itemStatusConfigSign)) {
            	itemStatus = itemStatusEnum.itemStatus;
                break;
            }
        }
        return itemStatus;
    }
	
	public static String getItemStatusNameByItemStatusConfigSign(String itemStatusConfigSign) {
        String itemStatusName = null;
        for (ItemStatusEnum itemStatusEnum : ItemStatusEnum.values()) {
            if (itemStatusEnum.itemStatusConfigSign.equals(itemStatusConfigSign)) {
            	itemStatusName = itemStatusEnum.itemStatusName;
                break;
            }
        }
        return itemStatusName;
    }
	public static String getItemStatusNameByItemStatus(Long itemStatus) {
        String itemStatusName = null;
        for (ItemStatusEnum itemStatusEnum : ItemStatusEnum.values()) {
            if (itemStatusEnum.itemStatus.equals(itemStatus)) {
            	itemStatusName = itemStatusEnum.itemStatusName;
                break;
            }
        }
        return itemStatusName;
    }
	
}
