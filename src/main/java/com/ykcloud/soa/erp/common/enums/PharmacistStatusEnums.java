package com.ykcloud.soa.erp.common.enums;

public enum PharmacistStatusEnums {
    UNALLOCATION(0, "已未分配"),
    ALLOCATION(1, "已分配");

    private int id;
    private String value;

    PharmacistStatusEnums(int id, String value) {
        this.id = id;
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static String getEnums(int id) {
        for (PharmacistStatusEnums item : PharmacistStatusEnums.values()) {
            if (item.getId() == id) {
                return item.getValue();
            }
        }
        return null;
    }

}
