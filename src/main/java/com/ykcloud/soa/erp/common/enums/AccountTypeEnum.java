package com.ykcloud.soa.erp.common.enums;

/**
 * @Author Hewei
 * @Date 2018/4/13 12:00
 */
public enum AccountTypeEnum {
    STKACCOUNT("1", "STKACCOUNT","可用帐"),
    PHYSICALACCOUNT("2", "WMACCOUNT","物理仓"),
    FINANCEACCOUNT("3", "FINANCEACCOUNT","财务批次账"),
    CONSIGNMENTACCOUNT("4", "CONSIGNMENTACCOUNT","代销商品款");

    private String accountType;
    private String accountTypeName;
    private String desc;

    AccountTypeEnum(String accountType, String accountTypeName, String desc) {
        this.accountType = accountType;
        this.accountTypeName = accountTypeName;
        this.desc = desc;
    }

    public static String getAccountTypeNameByAccountType(String accountType) {
        String accountTypeName = null;
        for (AccountTypeEnum accountTypeEnum : AccountTypeEnum.values()) {
            if (accountTypeEnum.accountType.equals(accountType)) {
                accountTypeName = accountTypeEnum.accountTypeName;
                break;
            }
        }
        return accountTypeName;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getAccountTypeName() {
        return accountTypeName;
    }

    public void setAccountTypeName(String accountTypeName) {
        this.accountTypeName = accountTypeName;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
