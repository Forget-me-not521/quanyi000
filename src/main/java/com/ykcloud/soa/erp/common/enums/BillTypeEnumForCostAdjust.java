package com.ykcloud.soa.erp.common.enums;

import java.util.Arrays;

/**
 * 库存金额调整的单据类型
 *
 * @author Sealin
 * @date 2018-09-04
 */
public enum BillTypeEnumForCostAdjust {
    UNKNOW(0L, "COSTADJUST", 0L),
    BATCH(1L, "COSTADJUST", 8201L),
    FULL(2L, "COSTADJUST", 8202L),
    LOWER(3L, "COSTADJUST", 8203L),
    ASSIGN(4L, "COSTADJUST", 8204L);
    public final Long ADJUST_TYPE;
    public final String BILL_TYPE_NUM_ID;
    public final Long BALANCE_FUNCTION_ID;

    BillTypeEnumForCostAdjust(Long adjustType, String billTypeNumId, Long balanceFunctionId) {
        ADJUST_TYPE = adjustType;
        BILL_TYPE_NUM_ID = billTypeNumId;
        BALANCE_FUNCTION_ID = balanceFunctionId;
    }

    public static BillTypeEnumForCostAdjust valueOf(Long typeNumId) {
        return Arrays.stream(values())
                     .filter(billTypeEnumForCostAdjust -> billTypeEnumForCostAdjust.ADJUST_TYPE.equals(typeNumId))
                     .findFirst()
                     .orElse(BillTypeEnumForCostAdjust.UNKNOW);
    }
}
