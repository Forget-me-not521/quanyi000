package com.ykcloud.soa.erp.common.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description: 扣项计算方式
 * @author: henry.wang
 * @date: 2018/5/14 14:27
 **/
public enum KxStyleEnum {
    MONTHLY(1L, "按月"),
    PER_YEAR(2L,"按年"),
    QUARTERLY(3L, "按季度");

    public static Map<Long,Long> kxStyle=new HashMap<>();
    static{
//        kxStyle.put(0L,?);
          kxStyle.put(1L,1L);
          kxStyle.put(2L,2L);
    }

    private Long value;
    private String desc;

    KxStyleEnum(Long value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public static KxStyleEnum fromValue(Long value) {
        for (KxStyleEnum enumItem : KxStyleEnum.values()) {
            if (enumItem.getValue().equals(value)) {
                return enumItem;
            }
        }
        return null;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
