package com.ykcloud.soa.erp.common.enums;

/**
 * @Author stark.jiang
 * @Date 2021/09/13/14:25
 * @Description:
 * @Version 1.0
 */
public enum BlProcessTypeEnum {
    HEAD_PRODUCT_INTRODUCT(1,"商品管理",101, "新品引进(总部)"),
    SUB_PRODUCT_INTRODUCT(1,"商品管理",102, "新品引进(分部)"),
    PRODUCT_BUILD_CODE(1,"商品管理",103, "商品建码"),
    PRODUCT_FIRST_SALSE(1,"商品管理",104, "商品首营"),
    PRODUCT_PUSH(1,"商品管理",105, "商品推送"),
    HEAD_PRODUCT_UPDATE(1,"商品管理",106, "商品修改(总部)"),
    SUB_PRODUCT_UPDATE(1,"商品管理",107, "商品修改(分部)"),

    SUPPLY_CREATE(2,"供应商",108, "供应商首营-工作流"),
    SUPPLY_UPDATE(2,"供应商",109, "供应商资料修改-工作流"),
    SUPPLY_HEAD_UPDATE(2,"供应商",2101, "供应商(总部)资料修改-主数据"),
    SUPPLY_SUB_UPDATE(2,"供应商",2102, "供应商(分部)资料修改-主数据"),
    CUSTOMER_CREATE(3,"客户",110, "客户首营-工作流"),
    CUSTOMER_UPDATE(3,"客户",111, "客户资料修改-工作流"),

    CUSTOMER_HEAD_UPDATE(3,"客户",3101, "客户资料(总部)修改-主数据"),
    CUSTOMER_SUB_UPDATE(3,"客户",3102, "客户资料(分部)修改-主数据"),


    PARTNER_CREATE(4,"合作商",112, "合作商创建-工作流"),
    PARTNER_UPDATE(4,"合作商",113, "合作商修改-工作流"),

    PARTNER_HEAD_UPDATE(4,"合作商",4101, "合作商资料(总部)修改-主数据"),
    PARTNER_SUB_UPDATE(4,"合作商",4102, "合作商资料(分部)修改-主数据"),


    SHOP_CREATE(5,"内部门店",114,"门店建档"),
    SHOP_UPDATE(5,"内部门店",115,"门店资料修改"),
    CLOSE_SHOP(5,"内部门店",116,"门店闭店"),
    MOVE_SHOP(5,"内部门店",117,"门店迁店"),
    CORT_CREATE(6,"公司",118,"公司创建"),
    CORT_UPDATE(6,"公司",119,"公司修改"),
    CORT_ASSIST(6,"公司",122,"辅助首营开关"),
    SHOP_OUTSIDE_CREATE(7,"外部门店",120,"外部门店建档"),
    SHOP_OUTSIDE_UPDATE(7,"外部门店",121,"外部门店修改"),

    PROTOCOL_CREATE(8,"协议",601,"协议创建"),
    PROTOCOL_UPDATE(8,"协议",602,"协议更正"),
    PROTOCOL_CANCAL(8,"协议",603,"协议废止"),
    PROTOCOL_DELETE(8,"协议",617,"协议删除"),
    CONTRACT_CREATE(9,"合同",604,"新增合同"),
    CONTRACT_UPDATE(9,"合同",605,"修改合同"),
    CONTRACT_CANCAL(9,"合同",606,"合同终止"),
    CONTRACT_RENEWAL(9,"合同",607,"合同续签"),
    CONTRACT_DELAY(9,"合同",608,"合同延期"),
    CONTRACT_ADD_PRODUCT(9,"合同",609,"合同追加新品"),
    CONTRACT_FILE_NO(9,"合同",610,"合同保存存档号"),
    CONTRACT_AUDITED(9,"合同",611,"合同审批通过"),
    TEMPLATE_CREATE(10,"合同模板",612,"合同模板创建"),
    TEMPLATE_UPDATE(10,"合同模板",613,"合同模板修改"),
    TEMPLATE_AUDITED(10,"合同模板",614,"合同模板审批通过"),
    TEMPLATE_GROUP_CREATE(10,"合同模板",615,"合同模板分组创建"),
    TEMPLATE_GROUP_UPDATE(10,"合同模板",616,"合同模板分组修改"),
    ITEM_POND_CREATE(11,"货源清单调价单",617,"新增货源清单调价单"),
    ITEM_POND_UPDATE(11,"货源清单调价单",618,"修改货源清单调价单"),
    ITEM_POND_DELETE(11,"货源清单调价单",619,"删除货源清单调价单"),
    ITEM_POND_AUDITED(11,"货源清单调价单",620,"货源清单调价单审批通过"),
    ITEM_POND_INVALID(11,"货源清单调价单",621,"货源清单调价单作废"),
    ITEM_POND_SUBMIT(11,"货源清单调价单",622,"货源清单调价单提交审核");
    private Integer businessType;
    private String businessTypeName;
    private Integer billType;
    private String billTypeName;

    BlProcessTypeEnum(Integer businessType,String businessTypeName,Integer billType, String billTypeName) {
        this.businessType = businessType;
        this.businessTypeName = businessTypeName;
        this.billType = billType;
        this.billTypeName = billTypeName;
    }

    public Integer getBusinessType() {
        return businessType;
    }

    public void setBusinessType(Integer businessType) {
        this.businessType = businessType;
    }

    public String getBusinessTypeName() {
        return businessTypeName;
    }

    public void setBusinessTypeName(String businessTypeName) {
        this.businessTypeName = businessTypeName;
    }

    public Integer getBillType() {
        return billType;
    }

    public void setBillType(Integer billType) {
        this.billType = billType;
    }

    public String getBillTypeName() {
        return billTypeName;
    }

    public void setBillTypeName(String billTypeName) {
        this.billTypeName = billTypeName;
    }

    public static String getBillTypeNameByBillType(Integer billType) {
        String billTypeName = null;
        for (BlProcessTypeEnum businessTypeEnum : BlProcessTypeEnum.values()) {
            if (businessTypeEnum.billType==billType) {
                billTypeName = businessTypeEnum.billTypeName;
                break;
            }
        }
        return billTypeName;
    }
    public static Integer getBusinessTypeByBillType(Integer billType) {
        int businessType = 0;
        for (BlProcessTypeEnum businessTypeEnum : BlProcessTypeEnum.values()) {
            if (businessTypeEnum.billType==billType) {
                businessType = businessTypeEnum.businessType;
                break;
            }
        }
        return businessType;
    }
}
