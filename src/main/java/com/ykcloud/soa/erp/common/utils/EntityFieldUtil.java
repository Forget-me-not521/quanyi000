package com.ykcloud.soa.erp.common.utils;

import com.ykcloud.soa.erp.common.entity.BaseEntity;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @Author Hewei
 * @Date 2018/3/21 20:30
 */
public class EntityFieldUtil {
    private static final String filterStr = "serialVersionUID";
    public static final String record_suffix = "_var";
    private static final List<String> filterList = Arrays.asList("CREATE_DTME", "LAST_UPDTME", "create_dtme", "last_updtme", "lastUpdtme", "createDtme");
    private static final List<String> filterList_update = Arrays.asList("LAST_UPDTME", "last_updtme", "lastUpdtme");
    private static final Map<String, String> defaultMap = new HashMap<>();

    static {
        defaultMap.put("cancelsign", "'N'");
        defaultMap.put("CANCELSIGN", "'N'");
    }

    /**
     * 将对象的属性转化为sql的字段
     *
     * @param klass
     * @type upperCase
     * @retur "col0,col1,col2....."
     */
    public static String fieldSplit(Class klass, String regx) {
        StringBuilder stringBuilder = new StringBuilder();
        Field[] fields = klass.getDeclaredFields();
        Field[] superFields = klass.getSuperclass().getDeclaredFields();
        int len = fields.length, superLen = superFields.length;
        if (len > 0 || superLen > 0) {
            stringBuilder.setLength(0);
            for (int i = 0; i < len; i++) {
                if (filterStr.equalsIgnoreCase(fields[i].getName())) {
                    continue;
                }
                stringBuilder.append(fields[i].getName().toUpperCase());
                if (i < len - 1) {
                    stringBuilder.append(regx);
                }
            }
            if (superLen > 0) {
                stringBuilder.append(regx);
                for (int i = 0; i < superLen; i++) {
                    if (filterStr.equalsIgnoreCase(superFields[i].getName())) {
                        continue;
                    }
                    stringBuilder.append(superFields[i].getName().toUpperCase());
                    if (i < superLen - 1) {
                        stringBuilder.append(regx);
                    }
                }
            }

        }
        return stringBuilder.toString();
    }

    /**
     * 生成通配符号字符串
     *
     * @param klass
     * @param regx
     * @retur "?,?,?,?"
     */
    public static String wildcardSplit(Class klass, String regx) {
        StringBuilder stringBuilder = new StringBuilder();
        Field[] fields = ArrayUtils.addAll(klass.getDeclaredFields(), klass.getSuperclass().getDeclaredFields());
        int len = fields.length;
        if (len > 0) {
            stringBuilder.setLength(0);
            for (int i = 0; i < len; i++) {
                if (filterStr.equalsIgnoreCase(fields[i].getName())) {
                    continue;
                } else if (filterList.contains(fields[i].getName())) {
                    stringBuilder.append("now()");
                } else if (defaultMap.containsKey(fields[i].getName())) {
                    stringBuilder.append(defaultMap.get(fields[i].getName()));
                } else {
                    stringBuilder.append("?");
                }
                if (i < len - 1) {
                    stringBuilder.append(regx);
                }
            }
        }
        return stringBuilder.toString();
    }

    public static String wildCardSplitUpdate(Object dataObject, String regx) {
        StringBuilder stringBuilder = new StringBuilder();
        Field[] fields = ArrayUtils.addAll(dataObject.getClass().getDeclaredFields(), dataObject.getClass().getSuperclass().getDeclaredFields());
        int len = fields.length;
        if (len > 0) {
            Method method;
            for (int i = 0; i < len; i++) {
                if (filterList_update.contains(fields[i].getName())) {
                    if (stringBuilder.length() > 0) {
                        stringBuilder.append(regx);
                    }
                    stringBuilder.append(fields[i].getName().toUpperCase());
                    stringBuilder.append("=now()");
                    continue;
                }
                try {
                    method = dataObject.getClass().getDeclaredMethod("get" + fields[i].getName().toUpperCase());
                } catch (NoSuchMethodException e) {
                    try {
                        method = dataObject.getClass().getSuperclass().getDeclaredMethod("get" + fields[i].getName().toUpperCase());
                    } catch (NoSuchMethodException e1) {
                        continue;
                    }
                }
                try {
                    Object object = method.invoke(dataObject);
                    if (object != null) {
                        if (stringBuilder.length() > 0) {
                            stringBuilder.append(regx);
                        }
                        stringBuilder.append(fields[i].getName().toUpperCase());
                        stringBuilder.append("=?");
                    }
                } catch (IllegalAccessException | InvocationTargetException e) {
                    continue;
                }

            }
        }
        return stringBuilder.toString();
    }

    /**
     * 生成通配符号字符串
     *
     * @param klass
     * @param dataObject
     * @retur Object[] 通配符对应的数值
     */
    public static Object[] fieldSplitValue(Class klass, Object dataObject) {
        List<Object> objectList = null;

        try {
            Field[] fields = klass.getDeclaredFields();
            Field[] superFields = klass.getSuperclass().getDeclaredFields();
            int len = fields.length, superLen = superFields.length;
            if (len > 0 || superLen > 0) {
                objectList = new ArrayList<>();
                Method method;
                for (int i = 0; i < len; i++) {
                    if (filterStr.equalsIgnoreCase(fields[i].getName()) || filterList.contains(fields[i].getName()) || defaultMap.containsKey(fields[i].getName())) {
                        continue;
                    }
                    method = dataObject.getClass().getDeclaredMethod("get" + fields[i].getName().toUpperCase());
                    Object object = method.invoke(dataObject);
                    if (object == null && (fields[i].getType() == Double.class || fields[i].getType() == Long.class || fields[i].getType() == Integer.class)) {
                        objectList.add(0);
                    } else {
                        objectList.add(object);
                    }
                }
                for (int i = 0; i < superLen; i++) {
                    if (filterStr.equalsIgnoreCase(superFields[i].getName()) || filterList.contains(superFields[i].getName()) || defaultMap.containsKey(superFields[i].getName())) {
                        continue;
                    }
                    method = dataObject.getClass().getSuperclass().getDeclaredMethod("get" + superFields[i].getName().toUpperCase());
                    objectList.add(method.invoke(dataObject));
                }
            }
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }
        return objectList == null ? null : objectList.toArray();
    }

    public static Object[] fieldSplitValueUpdate(Class<?> klass, Object dataObject) {
        List<Object> objectList = null;
        try {
            Field[] fields = klass.getDeclaredFields();
            Field[] superFields = klass.getSuperclass().getDeclaredFields();
            int len = fields.length, superLen = superFields.length;
            if (len > 0 || superLen > 0) {
                objectList = new ArrayList<>();
                Method method;
                for (int i = 0; i < len; i++) {
                    if (filterStr.equalsIgnoreCase(fields[i].getName()) || filterList.contains(fields[i].getName()) || defaultMap.containsKey(fields[i].getName())) {
                        continue;
                    }
                    method = dataObject.getClass().getDeclaredMethod("get" + fields[i].getName().toUpperCase());
                    Object object = method.invoke(dataObject);
                    if (object != null) {
                        objectList.add(object);
                    }
                }
                for (int i = 0; i < superLen; i++) {
                    if (filterStr.equalsIgnoreCase(superFields[i].getName()) || filterList.contains(superFields[i].getName()) || defaultMap.containsKey(superFields[i].getName())) {
                        continue;
                    }
                    method = dataObject.getClass().getSuperclass().getDeclaredMethod("get" + superFields[i].getName().toUpperCase());
                    Object object = method.invoke(dataObject);
                    if (object != null) {
                        objectList.add(object);
                    }
                }
            }
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }
        return objectList == null ? null : objectList.toArray();
    }

    public static Object[] fieldSplitValue(Class klass, Object dataObject, Map<String, Object> varMap) {
        List<Object> objectList = null;
        try {
            Field[] fields = klass.getDeclaredFields();
            Field[] superFields = klass.getSuperclass().getDeclaredFields();
            int len = fields.length, superLen = superFields.length;
            if (len > 0 || superLen > 0) {
                objectList = new ArrayList<>();
                Method method;
                for (int i = 0; i < len; i++) {
                    String name = fields[i].getName();
                    if (filterStr.equalsIgnoreCase(name) || filterList.contains(name) || defaultMap.containsKey(fields[i].getName())) {
                        continue;
                    }
                    if (varMap != null && varMap.containsKey(name)) {
                        objectList.add(varMap.get(name));
                    } else {
                        method = dataObject.getClass().getDeclaredMethod("get" + name.toUpperCase());
                        Object object = method.invoke(dataObject);
                        if (object == null && (fields[i].getType() == Double.class || fields[i].getType() == Long.class || fields[i].getType() == Integer.class)) {
                            objectList.add(0);
                        } else {
                            objectList.add(object);
                        }
                    }
                }
                for (int i = 0; i < superLen; i++) {
                    String name = superFields[i].getName();
                    if (filterStr.equalsIgnoreCase(name) || filterList.contains(name) || defaultMap.containsKey(superFields[i].getName())) {
                        continue;
                    }
                    if (varMap != null && varMap.containsKey(name)) {
                        objectList.add(varMap.get(name));
                    } else {
                        method = dataObject.getClass().getSuperclass().getDeclaredMethod("get" + name.toUpperCase());
                        objectList.add(method.invoke(dataObject));
                    }
                }
            }
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }
        return objectList == null ? null : objectList.toArray();
    }

    public static String buildInsertField(Class klass, String regx) {
        Field[] fields = klass.getDeclaredFields();
        Field[] superFields = klass.getSuperclass().getDeclaredFields();
        StringBuilder stringBuilder = new StringBuilder();
        int len = fields.length, superLen = superFields.length;
        if (len > 0) {
            stringBuilder.setLength(0);
            for (int i = 0; i < len; i++) {
                if (filterStr.equalsIgnoreCase(fields[i].getName())) {
                    continue;
                }
                stringBuilder.append(fields[i].getName().toUpperCase());
                stringBuilder.append("=");
                stringBuilder.append(regx);
                if (i < len - 1) {
                    stringBuilder.append(",");
                }
            }
        }
        if (superLen > 0) {
            stringBuilder.append(",");
            for (int i = 0; i < superLen; i++) {
                if (filterStr.equalsIgnoreCase(superFields[i].getName())) {
                    continue;
                }
                stringBuilder.append(superFields[i].getName().toUpperCase());
                stringBuilder.append("=");
                stringBuilder.append(regx);
                if (i < superLen - 1) {
                    stringBuilder.append(",");
                }
            }
        }
        return stringBuilder.toString();
    }

    public static Object covertObject(Object source, Object target, boolean camelToUnderLine) {
        Field[] sourceDeclaredFields = source.getClass().getDeclaredFields();
        Field[] sourceSuperFields = source.getClass().getSuperclass().getDeclaredFields();
        Field[] targetDeclaredFields = target.getClass().getDeclaredFields();
        if (sourceDeclaredFields.length > 0 && targetDeclaredFields.length > 0) {
            try {
                Method method;
                for (Field sourceField : sourceDeclaredFields) {
                    String sourceName = sourceField.getName();
                    String targetName = camelToUnderLine ? camel2Underline(sourceField.getName()) : underline2Camel(sourceField.getName(), true);
                    for (Field targetField : targetDeclaredFields) {
                        if (StringUtils.isNotEmpty(targetName) && targetName.equalsIgnoreCase(targetField.getName())) {
                            if (camelToUnderLine) {
                                method = source.getClass().getDeclaredMethod("get" + Character.toUpperCase(sourceName.charAt(0)) + sourceName.substring(1));
                                Object object = method.invoke(source);
                                method = target.getClass().getDeclaredMethod("set" + targetName.toUpperCase(), sourceField.getType());
                                method.invoke(target, object);
                                break;
                            } else {
                                method = source.getClass().getDeclaredMethod("get" + sourceName.toUpperCase());
                                Object object = method.invoke(source);
                                method = target.getClass().getDeclaredMethod("set" + Character.toUpperCase(targetName.charAt(0)) + targetName.substring(1), sourceField.getType());
                                method.invoke(target, object);
                                break;
                            }
                        }
                    }
                }
                for (Field sourceField : sourceSuperFields) {
                    String sourceName = sourceField.getName();
                    String targetName = camelToUnderLine ? camel2Underline(sourceField.getName()) : underline2Camel(sourceField.getName(), true);
                    for (Field targetField : targetDeclaredFields) {
                        if (StringUtils.isNotEmpty(targetName) && targetName.equalsIgnoreCase(targetField.getName())) {
                            if (camelToUnderLine) {
                                method = source.getClass().getSuperclass().getDeclaredMethod("get" + Character.toUpperCase(sourceName.charAt(0)) + sourceName.substring(1));
                                Object object = method.invoke(source);
                                method = target.getClass().getDeclaredMethod("set" + targetName.toUpperCase(), sourceField.getType());
                                method.invoke(target, object);
                                break;
                            } else {
                                method = source.getClass().getSuperclass().getDeclaredMethod("get" + sourceName.toUpperCase());
                                Object object = method.invoke(source);
                                method = target.getClass().getDeclaredMethod("set" + Character.toUpperCase(targetName.charAt(0)) + targetName.substring(1), sourceField.getType());
                                method.invoke(target, object);
                                break;
                            }
                        }
                    }
                }
            } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }
        }
        return target;
    }

    public static String camel2Underline(String line) {
        StringBuilder stringBuilder = new StringBuilder();
        if (StringUtils.isNotEmpty(line)) {
            line = String.valueOf(line.charAt(0)).toUpperCase().concat(line.substring(1));
            Pattern pattern = Pattern.compile("[A-Z]([a-z\\d]+)?");
            Matcher matcher = pattern.matcher(line);
            while (matcher.find()) {
                String word = matcher.group();
                stringBuilder.append(word.toUpperCase());
                stringBuilder.append(matcher.end() == line.length() ? "" : "_");
            }
        }
        return stringBuilder.toString();

    }

    public static String underline2Camel(String line, boolean smallCamel) {
        StringBuilder stringBuilder = new StringBuilder();
        if (StringUtils.isNotEmpty(line)) {
            stringBuilder.setLength(0);
            Pattern pattern = Pattern.compile("([A-Za-z\\d]+)(_)?");
            Matcher matcher = pattern.matcher(line);
            while (matcher.find()) {
                String word = matcher.group();
                stringBuilder.append(smallCamel && matcher.start() == 0 ? Character.toLowerCase(word.charAt(0)) : Character.toUpperCase(word.charAt(0)));
                int index = word.lastIndexOf('_');
                if (index > 0) {
                    stringBuilder.append(word.substring(1, index).toLowerCase());
                } else {
                    stringBuilder.append(word.substring(1).toLowerCase());
                }
            }
        }
        return stringBuilder.toString();
    }

    public static void main(String[] args) {
        BaseEntity baseEntity = new BaseEntity();
        baseEntity.setSERIES("1111");
        baseEntity.setTENANT_NUM_ID(1231l);
        baseEntity.setCANCELSIGN("n");
        System.out.println(wildCardSplitUpdate(baseEntity, ","));
    }

    public static String getInsertSql(Class clazz, String tableName) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("insert into ");
        stringBuffer.append(tableName);
        stringBuffer.append(" (");
        stringBuffer.append(EntityFieldUtil.fieldSplit(clazz, ","));
        stringBuffer.append(") values (");
        stringBuffer.append(EntityFieldUtil.wildcardSplit(clazz, ","));
        stringBuffer.append(")");
        return stringBuffer.toString();
    }
}
