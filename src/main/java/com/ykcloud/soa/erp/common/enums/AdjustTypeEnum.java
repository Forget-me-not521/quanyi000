package com.ykcloud.soa.erp.common.enums;

import java.util.Arrays;

/**
 * 调整类型
 *
 * @author Sealin
 * @date 2018-08-07
 */
public enum AdjustTypeEnum {
    /**
     * 批次日月进销存
     */
    BATCH_DAILY_MONTH(1L, "批次日月进销存"),
    /**
     * 成本日月进销存
     */
    COST_DAILY_MONTH(2L, "成本日月进销存"),
    /**
     * 加权平均
     */
    MOVING_WEIGHT(3L, "加权平均"),
    /**
     * 供应商往来表头
     */
    DEALINGS_HDR(4L, "供应商往来表头"),
    /**
     * 供应商往来表体
     */
    DEALINGS_DTL(5L, "供应商往来表体"),
    /**
     * 供应商往来商品明细
     */
    DEALINGS_ITEM_DTL(6L, "供应商往来商品明细"),
    /**
     * 代销商品款
     */
//    CONSIGNMENT_PRODUCT(8L, "代销商品款"),
    /**
     * 供应商发出代销商品
     */
//    EMIT_CONSIGNMENT_PRD(7L, "供应商发出代销商品"),
    /**
     * 调整结果
     */
    ADJUST_RESULT(9L, "调整结果"),
    /**
     * 批次日月进销存流水
     */
    BATCH_DAILY_MONTH_WBA(10L, "批次日月进销存流水"),
    /**
     * 发出代销商品流水
     */
//    EMIT_CONSIGNMENT_PRD_WBA(11L, "发出代销商品流水"),
    /**
     * 代销商品款流水
     */
//    CONSIGNMENT_PRODUCT_WBA(12L, "代销商品款流水"),
    WM_BATCH(13L, "批次价格调整"),
    WM_RECEIPT(14L, "验收单-不到批次"),
    WM_RECEIPT_BATCH(15L, "验收单-到批次");

    public final Long typeID;
    public final String comment;

    AdjustTypeEnum(Long typeID, String comment) {
        this.typeID = typeID;
        this.comment = comment;
    }

    /**
     * 根据ID获得类型
     *
     * @throws IllegalArgumentException 找不到对应ID时, 抛出非法参数异常
     * @author Sealin
     */
    public static AdjustTypeEnum valueOf(Long typeID) {
        return Arrays.stream(AdjustTypeEnum.values())
                     .filter(type -> type.typeID.equals(typeID))
                     .findFirst()
                     .orElseThrow(IllegalArgumentException::new);
    }
}
