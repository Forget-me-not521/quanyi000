package com.ykcloud.soa.erp.common.enums;

/**
 * @Author stark.jiang
 * @Date 2021/10/18/15:53
 * @Description:
 * @Version 1.0
 */
public enum NotAllowPriceTypeEnum {
    MEMBER_PRICE(1, "会员价"),
    BELOW_LOWEST_PRICE(2, "低于最低价");
    private Integer typeId;
    private String typeName;
    NotAllowPriceTypeEnum(Integer typeId, String typeName) {
        this.typeId = typeId;
        this.typeName = typeName;
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public static String getTypeNameByTypeId(Integer typeId) {
        String typeName = null;
        for (NotAllowPriceTypeEnum typeEnum : NotAllowPriceTypeEnum.values()) {
            if (typeEnum.typeId==typeId) {
                typeName = typeEnum.typeName;
                break;
            }
        }
        return typeName;
    }
}
