package com.ykcloud.soa.erp.common.enums;

public enum RedisStkEnum {

    //RECEIPT_1_11001("RECEIPT",1l,11001L, "0"),


    ;
    private String billTypeNumId;
    private Long typeNumId;
    private Long balanceFunctionId;
    private String flag;

    RedisStkEnum(String billTypeNumId, Long typeNumId, Long balanceFunctionId, String flag) {
        this.billTypeNumId = billTypeNumId;
        this.typeNumId = typeNumId;
        this.balanceFunctionId = balanceFunctionId;
        this.flag = flag;
    }

    public static String getFlag(String billTypeNumId, Long typeNumId, Long balanceFunctionId) {
        String flag = null;
        for (RedisStkEnum redisStkEnum : RedisStkEnum.values()) {
            if (redisStkEnum.billTypeNumId.equals(billTypeNumId)) {
                if (redisStkEnum.typeNumId.equals(typeNumId)) {
                    if (redisStkEnum.balanceFunctionId.equals(balanceFunctionId)) {
                        flag = redisStkEnum.flag;
                        break;
                    }
                }
            }
        }
        return flag;
    }

    public String getBillTypeNumId() {
        return billTypeNumId;
    }

    public void setBillTypeNumId(String billTypeNumId) {
        this.billTypeNumId = billTypeNumId;
    }

    public Long getTypeNumId() {
        return typeNumId;
    }

    public void setTypeNumId(Long typeNumId) {
        this.typeNumId = typeNumId;
    }

    public Long getBalanceFunctionId() {
        return balanceFunctionId;
    }

    public void setBalanceFunctionId(Long balanceFunctionId) {
        this.balanceFunctionId = balanceFunctionId;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }
}
