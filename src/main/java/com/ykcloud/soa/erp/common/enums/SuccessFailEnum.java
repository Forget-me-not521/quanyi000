package com.ykcloud.soa.erp.common.enums;

/**
 * @Auther: Dan
 * @Date: 2019/5/23 19:40
 * @Description:
 */
public enum SuccessFailEnum {
    SUCCESS("SUCCESS","成功"),
    FAIL("FALI","失败");

    private String typeNameEn;
    private String typeNameZh;

    SuccessFailEnum(String typeNameEn, String typeNameZh) {
        this.typeNameEn = typeNameEn;
        this.typeNameZh = typeNameZh;
    }

    public String getTypeNameEn() {
        return typeNameEn;
    }

    public void setTypeNameEn(String typeNameEn) {
        this.typeNameEn = typeNameEn;
    }

    public String getTypeNameZh() {
        return typeNameZh;
    }

    public void setTypeNameZh(String typeNameZh) {
        this.typeNameZh = typeNameZh;
    }
}
