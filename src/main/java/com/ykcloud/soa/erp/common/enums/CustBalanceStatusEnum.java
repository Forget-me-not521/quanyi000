package com.ykcloud.soa.erp.common.enums;

/**
 * 客户结算状态
 *
 * @author Sealin
 */
public enum CustBalanceStatusEnum {
    WITHOUT_CONFIRM(1L, "未确认"),
    CONFIRMED(2L, "已确认"),
    SENT_TICKETS(3L, "已开票");

    public final Long ID;
    public final String COMMENT;

    CustBalanceStatusEnum(Long id, String comment) {
        this.ID = id;
        this.COMMENT = comment;
    }
}
