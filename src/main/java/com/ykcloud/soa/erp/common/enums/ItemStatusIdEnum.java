package com.ykcloud.soa.erp.common.enums;

import java.util.Objects;

/**
 * 商品状态枚举
 * @author tz.x
 * @date 2018年8月22日下午1:42:55
 */
public enum ItemStatusIdEnum {
	
	//YK: 0待启用 1新品 2正常 3暂停订货售完即止 4暂停销售 5暂停经营 6待清退 7已清理
	
	TO_BE_ENABLED(0L, "待启用 "),
	NEW_ITEM(1L, "新品"),
	NORMAL(2L, "正常"),
	STOP_ORDER(3L, "暂停订货售完即止"),
	STOP_ORDER_SEASONAL(51L, "暂停订货季节关闭"),
	STOP_ORDER_SUPPLIER(52L, "暂停订货供应商缺货"),
	STOP_ORDER_ABNORMAL(53L, "暂停订货合作异常"),
	STOP_SELL(4L, "暂停销售"),
	STOP_BUSINESS(5L, "暂停经营"),
	TO_BE_RETREATED(6L, "待清退"),

	CLEANED(7L, "已清理");
	
	private Long itemStatusId;
	
	private String itemStatusName;
	
	ItemStatusIdEnum(Long itemStatusId, String itemStatusName) {
		this.itemStatusId = itemStatusId;
		this.itemStatusName = itemStatusName;
	}
	
	public static String getItemStatusNameByItemStautusId(Long itemStautusId) {
        String itemStatusName = null;
        for (ItemStatusIdEnum itemStatusIdEnum : ItemStatusIdEnum.values()) {
            if (Objects.equals(itemStatusIdEnum.itemStatusId, itemStautusId)) {
            	itemStatusName = itemStatusIdEnum.itemStatusName;
                break;
            }
        }
        return itemStatusName;
    }

	public Long getItemStatusId() {
		return itemStatusId;
	}

	public void setItemStatusId(Long itemStatusId) {
		this.itemStatusId = itemStatusId;
	}

	public String getItemStatusName() {
		return itemStatusName;
	}

	public void setItemStatusName(String itemStatusName) {
		this.itemStatusName = itemStatusName;
	}
}
