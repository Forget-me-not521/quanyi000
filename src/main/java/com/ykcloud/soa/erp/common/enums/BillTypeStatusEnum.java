package com.ykcloud.soa.erp.common.enums;

/**
 * /**
 *
 * @description:
 * @author: Dan
 * @time: 2020/9/1 16:23
 */
public enum BillTypeStatusEnum {

    //receipt
    RECEIPT_TYPE_NUM_ID_1(1L, "RECEIPT", "TYPE_NUM_ID","采购入库"),
    RECEIPT_TYPE_NUM_ID_2(2L, "RECEIPT", "TYPE_NUM_ID","退仓入库"),
    RECEIPT_TYPE_NUM_ID_3(3L, "RECEIPT", "TYPE_NUM_ID","直通入库"),
    RECEIPT_TYPE_NUM_ID_4(4L, "RECEIPT", "TYPE_NUM_ID","b2b销售退货入库"),
    RECEIPT_TYPE_NUM_ID_5(5L, "RECEIPT", "TYPE_NUM_ID","委托配送，连锁空转入库"),
    RECEIPT_TYPE_NUM_ID_6(6L, "RECEIPT", "TYPE_NUM_ID","配送到店，门店入库"),

    RECEIPT_STATUS_NUM_ID_1(1L, "RECEIPT", "STATUS_NUM_ID","待收货"),
    RECEIPT_STATUS_NUM_ID_2(2L, "RECEIPT", "STATUS_NUM_ID","收货审批中"),
    _RECEIPT_STATUS_NUM_ID_3(-3L, "RECEIPT", "STATUS_NUM_ID","收货中(库存入虚拟库位)"),
    RECEIPT_STATUS_NUM_ID_3(3L, "RECEIPT", "STATUS_NUM_ID","验收中"),
    RECEIPT_STATUS_NUM_ID_5(5L, "RECEIPT", "STATUS_NUM_ID","入账完成"),
    _RECEIPT_STATUS_NUM_ID_5(-5L, "RECEIPT", "STATUS_NUM_ID","入账完成中"),

    RECEIPT_SO_FROM_TYPE_1(1L, "RECEIPT", "SO_FROM_TYPE","预约产生"),
    RECEIPT_SO_FROM_TYPE_2(2L, "RECEIPT", "SO_FROM_TYPE","手动创建"),


    SHELVES_STATUS_NUM_ID_1(1L, "SHELVES", "STATUS_NUM_ID","创建(待上架)"),
    //_SHELVES_STATUS_NUM_ID_2(-2L, "SHELVES", "STATUS_NUM_ID","上架中"),
    SHELVES_STATUS_NUM_ID_2(2L, "SHELVES", "STATUS_NUM_ID","已上架"),
    SHELVES_TYPE_NUM_ID_1(1L, "SHELVES", "TYPE_NUM_ID","上架"),
    SHELVES_TYPE_NUM_ID_2(2L, "SHELVES", "TYPE_NUM_ID","下架"),
    SHELVES_SO_FROM_TYPE_1(1L, "SHELVES", "SO_FROM_TYPE","采购入库"),
    SHELVES_SO_FROM_TYPE_2(2L, "SHELVES", "SO_FROM_TYPE","其他"),

    SHELVES_LOC_DTL_STATUS_NUM_ID_0(0L, "SHELVES_DTL", "STATUS_NUM_ID","wms未上架"),
    SHELVES_LOC_DTL_STATUS_NUM_ID_1(1L, "SHELVES_DTL", "STATUS_NUM_ID","wms已上架"),

    MOVE_TYPE_NUM_ID_1(1L, "MOVE", "TYPE_NUM_ID","补货单"),
    MOVE_TYPE_NUM_ID_2(2L, "MOVE", "TYPE_NUM_ID","移位单"),
    MOVE_TYPE_NUM_ID_3(3L, "MOVE", "TYPE_NUM_ID","上仓单"),
    MOVE_TYPE_NUM_ID_4(4L, "MOVE", "TYPE_NUM_ID","下仓单"),
    MOVE_STATUS_NUM_ID_1(1L, "MOVE", "STATUS_NUM_ID","工作流审批中"),
    _MOVE_STATUS_NUM_ID_2(2L, "MOVE", "STATUS_NUM_ID","待下架前锁库中"),
    MOVE_STATUS_NUM_ID_2(2L, "MOVE", "STATUS_NUM_ID","待下架"),
    _MOVE_STATUS_NUM_ID_3(-3L, "MOVE", "STATUS_NUM_ID","下架中"),
    MOVE_STATUS_NUM_ID_3(3L, "MOVE", "STATUS_NUM_ID","下架完成，待上架"),
    _MOVE_STATUS_NUM_ID_4(-4L, "MOVE", "STATUS_NUM_ID","上架中"),
    MOVE_STATUS_NUM_ID_4(4L, "MOVE", "STATUS_NUM_ID","上架完成上架"),


    SO_STATUS_NUM_ID_2(2L, "SO", "STATUS_NUM_ID","创建"),
    SO_STATUS_NUM_ID_3(3L, "SO", "STATUS_NUM_ID","拣货中"),
    SO_STATUS_NUM_ID_4(4L, "SO", "STATUS_NUM_ID","0短发作废"),
    _SO_STATUS_NUM_ID_8(-8L, "SO", "STATUS_NUM_ID","门店发货中"),
    SO_STATUS_NUM_ID_8(8L, "SO", "STATUS_NUM_ID","门店已发货未过账"),
    SO_STATUS_NUM_ID_9(9L, "SO", "STATUS_NUM_ID","过账完成"),

    SO_TYPE_NUM_ID_1(1L, "SO", "TYPE_NUM_ID","批发委托配送"),
    SO_TYPE_NUM_ID_2(2L, "SO", "TYPE_NUM_ID","连锁配送"),
    SO_TYPE_NUM_ID_3(3L, "SO", "TYPE_NUM_ID","批发对外销售"),
    SO_TYPE_NUM_ID_4(4L, "SO", "TYPE_NUM_ID","门店退连锁"),
    SO_TYPE_NUM_ID_5(5L, "SO", "TYPE_NUM_ID","门店退批发"),
    SO_TYPE_NUM_ID_6(6L, "SO", "TYPE_NUM_ID","同连锁店间调拨"),
    SO_TYPE_NUM_ID_7(7L, "SO", "TYPE_NUM_ID","跨连锁店间调拨"),

    PICK_TYPE_NUM_ID_1(1L, "PICK", "TYPE_NUM_ID",""),

    PICK_STATUS_NUM_ID_1(1L, "PICK", "STATUS_NUM_ID","创建"),
    PICK_STATUS_NUM_ID_2(2L, "PICK", "STATUS_NUM_ID","待拣货"),
    PICK_STATUS_NUM_ID_3(3L, "PICK", "STATUS_NUM_ID","拣货中"),
    PICK_STATUS_NUM_ID_4(4L, "PICK", "STATUS_NUM_ID","拣货完成"),
    PICK_STATUS_NUM_ID_5(5L, "PICK", "STATUS_NUM_ID","复核中"),
    _PICK_STATUS_NUM_ID_6(-6L, "PICK", "STATUS_NUM_ID","复核装箱完成中(过账中)"),
    PICK_STATUS_NUM_ID_6(6L, "PICK", "STATUS_NUM_ID","复核装箱完成"),
    PICK_STATUS_NUM_ID_7(7L, "PICK", "STATUS_NUM_ID","已发货"),


    CONTAINER_STATUS_NUM_ID_1(1L, "CONTAINER", "STATUS_NUM_ID","创建"),
    CONTAINER_TYPE_NUM_ID_1(1L, "CONTAINER", "TYPE_NUM_ID","配送"),

    SHIP_TYPE_NUM_ID_1(1L, " SHIP", "TYPE_NUM_ID","配送"),
    SHIP_STATUS_NUM_ID_1(1L, " SHIP", "STATUS_NUM_ID","创建"),
    _SHIP_STATUS_NUM_ID_2(-2L, " SHIP", "STATUS_NUM_ID","过账完成中"),
    SHIP_STATUS_NUM_ID_2(2L, " SHIP", "STATUS_NUM_ID","过账完成"),





    RECEIPT_TRAY_STATUS_NUM_ID_1(1L, "RECEIPT_TRAY", "STATUS_NUM_ID","创建"),
    RECEIPT_TRAY_STATUS_NUM_ID_2(2L, "RECEIPT_TRAY", "STATUS_NUM_ID","封箱"),
    RECEIPT_TRAY_STATUS_NUM_ID_3(3L, "RECEIPT_TRAY", "STATUS_NUM_ID","上架完成"),
    RECEIPT_TRAY_STATUS_NUM_ID_4(4L, "RECEIPT_TRAY", "STATUS_NUM_ID","过账完成"),

    RECEIPT_TRAY_TYPE_NUM_ID_1(1L, "RECEIPT_TRAY", "TYPE_NUM_ID","整托"),
    RECEIPT_TRAY_TYPE_NUM_ID_2(2L, "RECEIPT_TRAY", "TYPE_NUM_ID","整箱"),
    RECEIPT_TRAY_TYPE_NUM_ID_3(3L, "RECEIPT_TRAY", "TYPE_NUM_ID","散件"),
    RECEIPT_TRAY_TYPE_NUM_ID_4(4L, "RECEIPT_TRAY", "TYPE_NUM_ID","pc收货"),

    RECEIPT_TRAY_SO_FROM_TYPE_1(1L, "RECEIPT_TRAY", "SO_FROM_TYPE","采购来源"),
    RECEIPT_TRAY_SO_FROM_TYPE_2(2L, "RECEIPT_TRAY", "SO_FROM_TYPE","其他来源"),

    REP_PO_STATUS_NUM_ID_6(6L, "REP_PO", "STATUS_NUM_ID","已生效"),
    REP_PO_REC_STATUS_1(1L, "REP_PO", "REC_STATUS","待收货"),
    _REP_PO_REC_STATUS_2(-2L, "REP_PO", "REC_STATUS","收货单生成中"),
    REP_PO_REC_STATUS_2(2L, "REP_PO", "REC_STATUS","已收货"),


    PO_RECORD_STATUS_NUM_ID_3(3L, "PO_RECORD", "STATUS_NUM_ID","已确认"),
    PO_RECORD_STATUS_NUM_ID_6(6L, "PO_RECORD", "STATUS_NUM_ID","已收货"),
    PO_RECORD_STATUS_NUM_ID_9(9L, "PO_RECORD", "STATUS_NUM_ID","作废"),


    BATCH_VIRTUAL_BATCH_ID_1(1L, "BATCH", "VIRTUAL_BATCH_ID","虚拟批次"),
    BATCH_VIRTUAL_BATCH_ID_0(0L, "BATCH", "VIRTUAL_BATCH_ID","非虚拟批次"),

    BATCH_ZT_SIGN_0(0L, "BATCH", "ZT_SIGN","非直通批次库存"),
    BATCH_ZT_SIGN_1(1L, "BATCH", "ZT_SIGN","直通批次库存"),




    ZONE_VIRTUAL_SIGN_0(0L, "ZONE", "VIRTUAL_SIGN","非虚拟库区"),
    ZONE_VIRTUAL_SIGN_1(1L, "ZONE", "VIRTUAL_SIGN","虚拟库区"),

    ZONE_SPECIFICATIONS_1(1L, "ZONE", "ZONE_SPECIFICATIONS","高架整托区"),
    ZONE_SPECIFICATIONS_2(2L, "ZONE", "ZONE_SPECIFICATIONS","整件(箱)区"),
    ZONE_SPECIFICATIONS_3(3L, "ZONE", "ZONE_SPECIFICATIONS","散件区"),
    ZONE_SPECIFICATIONS_4(4L, "ZONE", "ZONE_SPECIFICATIONS","直通区"),
    ZONE_SPECIFICATIONS_5(5L, "ZONE", "ZONE_SPECIFICATIONS","收货区"),
    ZONE_SPECIFICATIONS_6(6L, "ZONE", "ZONE_SPECIFICATIONS","发货区"),
    ZONE_SPECIFICATIONS_7(7L, "ZONE", "ZONE_SPECIFICATIONS","存货区"),


    WAREHOUSE_REPLENISH_TYPE_ONE(1L, "WAREHOUSE_REPLENISH", "TYPE_NUM_ID","一级补货，整件->零散"),
    WAREHOUSE_REPLENISH_TYPE_TWE(2L, "WAREHOUSE_REPLENISH", "TYPE_NUM_ID","二级补货，整托->整件"),

    WAREHOUSE_REPLENISH_AUTOMATIC_1(1L, "WAREHOUSE_REPLENISH", "TYPE_NUM_ID","自动"),
    WAREHOUSE_REPLENISH_AUTOMATIC_0(0L, "WAREHOUSE_REPLENISH", "TYPE_NUM_ID","非自动"),

    STK_LOCK_TYPE_1(1L, "STK", "LOCK_TYPE","有多少锁多少"),
    STK_LOCK_TYPE_2(2L, "STK", "LOCK_TYPE","锁不到指定数量报错"),
    STK_LOCK_TYPE_3(3L, "STK", "LOCK_TYPE","可以负库存锁库"),



    ALL_SETTLEMENT_TYPE_1(1L, "ALL", "SETTLEMENT_TYPE","购销"),
    ALL_SETTLEMENT_TYPE_2(2L, "ALL", "SETTLEMENT_TYPE","代销"),
    ALL_SETTLEMENT_TYPE_3(3L, "ALL", "SETTLEMENT_TYPE","联营"),

    ALL_LOGISTICS_TYPE_1(1L, "ALL", "LOGISTICS_TYPE","直送"),
    ALL_LOGISTICS_TYPE_2(2L, "ALL", "LOGISTICS_TYPE","直通"),
    ALL_LOGISTICS_TYPE_3(3L, "ALL", "LOGISTICS_TYPE","配送"),

    ALL_PMT_SIGN_0(0L, "ALL", "PMT_SIGN","正常品"),
    ALL_PMT_SIGN_1(1L, "ALL", "PMT_SIGN","赠品"),


    ALL_CANCELSIGN_NO(0L, "CANCELSIGN", "N","取消标识"),

    PHYSICAL_VIRTUAL_SIGN_0(0L, "PHYSICAL", "VIRTUAL_SIGN","大仓非虚拟"),
    PHYSICAL_VIRTUAL_SIGN_1(1L, "PHYSICAL", "VIRTUAL_SIGN","大仓虚拟"),
    PHYSICAL_ENABLE_SIGN_0(0L, "PHYSICAL", "ENABLE_SIGN","大仓不可用"),
    PHYSICAL_ENABLE_SIGN_1(1L, "PHYSICAL", "ENABLE_SIGN","大仓可用"),

    ZONE_ENABLE_SIGN_0(0L, "ZONE", "ENABLE_SIGN","库区不可用"),
    ZONE_ENABLE_SIGN_1(1L, "ZONE", "ENABLE_SIGN","库区可用"),

    LOC_ENABLE_SIGN_0(0L, "LOC", "ENABLE_SIGN","库位不可用"),
    LOC_ENABLE_SIGN_1(1L, "LOC", "ENABLE_SIGN","库位可用"),

    LOC_ITEM_MIX_SIGN_0(0L, "LOC", "ITEM_MIX_SIGN","库位不能混品"),
    LOC_ITEM_MIX_SIGN_1(1L, "LOC", "ITEM_MIX_SIGN","库位能混品"),

    LOC_BATCH_MIX_SIGN_0(0L, "LOC", "BATCH_MIX_SIGN","库位不能混批"),
    LOC_BATCH_MIX_SIGN_1(1L, "LOC", "BATCH_MIX_SIGN","库位能混批"),


    ALL_IS_DOUBLE_RECEIVE_0(0L, "ALL", "IS_DOUBLE_RECEIVE","不需要双人验收"),
    ALL_IS_DOUBLE_RECEIVE_1(1L, "ALL", "IS_DOUBLE_RECEIVE","需要双人验收"),

    ALL_ESUPERVISE_FOLLOW_0(0L, "ALL", "ESUPERVISE_FOLLOW","药监码管控-否"),
    ALL_ESUPERVISE_FOLLOW_1(1L, "ALL", "ESUPERVISE_FOLLOW","药监码管控-是"),


    ;

    private Long numId;
    private String billTypeNumId;
    private String accountName;
    private String desc;

    public static String getBillTypeStatusEnumDesc(String billTypeNumId, String accountName ,Long numId) {
        String desc = null;
        for (BillTypeStatusEnum billTypeStatusEnum : BillTypeStatusEnum.values()) {
            if (billTypeStatusEnum.billTypeNumId.equals(billTypeNumId)) {
                if (billTypeStatusEnum.accountName.equals(accountName)) {
                    if (billTypeStatusEnum.numId.equals(numId)) {
                        desc = billTypeStatusEnum.desc;
                        break;
                    }
                }
            }
        }
        return desc;
    }

    BillTypeStatusEnum(Long numId, String billTypeNumId, String accountName, String desc) {
        this.numId = numId;
        this.billTypeNumId = billTypeNumId;
        this.accountName = accountName;
        this.desc = desc;
    }

    public Long getNumId() {
        return numId;
    }

    public void setNumId(Long numId) {
        this.numId = numId;
    }

    public String getBillTypeNumId() {
        return billTypeNumId;
    }

    public void setBillTypeNumId(String billTypeNumId) {
        this.billTypeNumId = billTypeNumId;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
