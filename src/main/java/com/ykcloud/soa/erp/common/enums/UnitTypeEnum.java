
package com.ykcloud.soa.erp.common.enums;

import java.util.ArrayList;
import java.util.List;

public enum UnitTypeEnum {
	UNIT_ORG(1,"业务单元"),
	PUR_ORG(2,"采购组织"),
	SAL_ORG(3,"销售组织"),
	INV_ORG(4,"库存组织"),
	FIN_ORG(5,"财务组织");
	
	private int typeNumId;
	
	private String typeDesc;


	UnitTypeEnum(int typeNumId, String typeDesc) {
		this.typeNumId = typeNumId;
		this.typeDesc = typeDesc;
	}
	
	public int getTypeNumId() {
		return typeNumId;
	}



	public void setTypeNumId(int typeNumId) {
		this.typeNumId = typeNumId;
	}



	public String getTypeDesc() {
		return typeDesc;
	}



	public void setTypeDesc(String typeDesc) {
		this.typeDesc = typeDesc;
	}
	
	public static List<Integer> getCodeList() {
		List<Integer> typeNumIdList = new ArrayList<>();
		for(UnitTypeEnum itemTypeEnum: UnitTypeEnum.values()) {
			typeNumIdList.add(itemTypeEnum.getTypeNumId());
		}
		return typeNumIdList;
	}

}
