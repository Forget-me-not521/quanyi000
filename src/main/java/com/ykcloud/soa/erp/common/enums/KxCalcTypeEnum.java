package com.ykcloud.soa.erp.common.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description: 扣项计算类型
 * @author: henry.wang
 * @date: 2018/5/14 14:27
 **/
public enum KxCalcTypeEnum {

    SETTLEMENT_SHEET(0L,"按结算单收取"),
    MONTHLY(1L, "按月收取"),
    YEAR(1L, "按年收取");

    public static Map<Long,Long> kxCalcType=new HashMap<>();
    static{
//        kxCalcType.put(0L,?);
        kxCalcType.put(1L,1L);
        kxCalcType.put(2L,2L);
    }

    private Long value;
    private String desc;

    KxCalcTypeEnum(Long value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public static KxStyleEnum fromValue(Long value) {
        for (KxStyleEnum enumItem : KxStyleEnum.values()) {
            if (enumItem.getValue().equals(value)) {
                return enumItem;
            }
        }
        return null;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }


}
