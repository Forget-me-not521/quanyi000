package com.ykcloud.soa.erp.common.enums;

import java.util.Arrays;

public enum RateAdjustStatusEnum {

	APPROVING(1L, "待审核"),APPROVED(2l,"制单审核"),FINANCE_APPROVED(3L,"财务审核")
	, APPROVING_NOT(5L, "审核不通过");

	public final Long ID;
	public final String COMMENT;

	RateAdjustStatusEnum(Long id, String comment) {
		this.ID = id;
		this.COMMENT = comment;
	}

	public static String getComment(Long id) {
		 RateAdjustStatusEnum statusEnum = Arrays.stream(RateAdjustStatusEnum.values())
				.filter(status -> status.ID.equals(id)).findFirst().
				orElseThrow(IllegalArgumentException::new);
		 return statusEnum.COMMENT;
	}

}
