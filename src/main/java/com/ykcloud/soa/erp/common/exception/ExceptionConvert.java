package com.ykcloud.soa.erp.common.exception;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * @Author Hewei
 * @Date 2018/4/20 19:19
 */
public class ExceptionConvert {
    public static String toString(Exception e) {
        String exceptionStr = null;
        try {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            exceptionStr = sw.toString();
            sw.close();
            pw.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return exceptionStr;
    }
}
