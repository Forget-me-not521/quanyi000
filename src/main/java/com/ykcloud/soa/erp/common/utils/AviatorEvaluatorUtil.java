package com.ykcloud.soa.erp.common.utils;

import com.googlecode.aviator.AviatorEvaluator;

import java.math.BigDecimal;
import java.util.Map;

/**
 * @author tz.x
 * @date 2021/9/27 9:39
 * 适合复杂运算
 */
public class AviatorEvaluatorUtil {

    public static double executeToDouble(String expression, Map<String, Object> env) {
        BigDecimal par1 = new BigDecimal(String.valueOf(AviatorEvaluator.execute(expression, env, false)));
        return par1.setScale(2, 4).doubleValue();
    }

    public static Object executeToObject(String expression, Map<String, Object> env) {
        Object par1 = AviatorEvaluator.execute(expression, env, false);
        return par1;
    }
}
