package com.ykcloud.soa.erp.common.annotation;

import java.lang.annotation.*;

/**
 * 对象日志注释
 * @author xxw
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface FeildLogTag {

    String value() default "";

    boolean isDict() default false;

}
