package com.ykcloud.soa.erp.common.enums;

import java.util.Arrays;

public enum RateAdjustTypeEnum {

	IN_RATE(1L, "进项税率调整"),

	OUT_RATE(2L, "销项税"),

	BACKOFF_RATE(3L, "倒扣率");

	public final Long id;

	public final String comment;

	RateAdjustTypeEnum(Long id, String comment) {
		this.id = id;
		this.comment = comment;
	}
	
	public static RateAdjustTypeEnum getRateAdjustType(Long id) {
		
		return Arrays.stream(RateAdjustTypeEnum.values()).filter(type->type.id.equals(id)).findFirst().orElse(null);
		
	}

}
