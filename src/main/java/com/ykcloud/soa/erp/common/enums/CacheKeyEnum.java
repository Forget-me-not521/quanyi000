package com.ykcloud.soa.erp.common.enums;

import com.ykcloud.cache.spring.boot.starter.CacheKey;
import lombok.Getter;

/**
 * @author tz.x
 * @date 2021/9/19 16:45
 */
public enum CacheKeyEnum {

    STO_ITEM_POND_PREFIX("stoItemPond", "仓库货源清单"),
    TRANSIT_PREFIX_STO("transitSto", "仓库在途"),
    REP_SUB_MQ_PREFIX("repSubMq", "门店移动月销"),
    REP_STO_MQ_PREFIX("repStoMq", "仓库移动月销");

    @Getter
    private String prefix;
    @Getter
    private String desc;

    CacheKeyEnum(String prefix, String desc) {
        this.prefix = prefix;
        this.desc = desc;
    }

    public final static CacheKey getCacheKey(String prefix, String... args) {
        StringBuffer buffer = new StringBuffer();
        for (String arg : args) {
            buffer.append(":").append(arg);
        }
        CacheKey cacheKey = CacheKey.builder().prefix(prefix)
                .suffix(buffer.toString()).build();
        return cacheKey;
    }


}
