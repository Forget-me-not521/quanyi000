package com.ykcloud.soa.erp.common.utils;


import com.ykcloud.soa.erp.common.annotation.FeildLogTag;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.function.Function;

/**
 * 对象赋值工具
 *
 * @param <T>
 * @author xxw
 */
@Slf4j
public class ObjectAssignmentUtil<T> {

    private FieldLogCglib<T> cglibUtil;

    private static final String SET_PREFIX = "set";

    private static final String GET_PREFIX = "get";

    public ObjectAssignmentUtil() {
        this.cglibUtil = new FieldLogCglib<T>();
    }

    /**
     * 赋值操作
     *
     * @param t1
     * @param t2
     * @return
     */
    public String assignment(T t1, T t2) throws Exception {
        return assignment(t1, t2, null);
    }

    /**
     * 赋值操作
     *
     * @param t1
     * @param t2
     * @param function
     * @return
     */
    public String assignment(T t1, T t2, Function<T, String> function) throws Exception {
        if (t1 == null && t2 == null) {
            throw new Exception("对比的参数不能为空");
        }
        if (function != null) {
            if (t1 == null) {
                return new StringBuilder().append("新增").append(function.apply(t2)).toString();
            }
            if (t2 == null) {
                return new StringBuilder().append("删除").append(function.apply(t1)).toString();
            }
        }else{
            if (t1 == null || t2 == null) {
                throw new Exception("对比的参数不能为空");
            }
        }
        Class clazz = t1.getClass();
        this.cglibUtil.setTarget(t1);
        this.cglibUtil.setFunction(function);
        t1 = this.cglibUtil.getProxy();
        Field[] fields = clazz.getDeclaredFields();
        FeildLogTag tag;
        String fieldName;
        Method getMethod;
        Method setMethod;
        for (Field f : fields) {
            tag = f.getAnnotation(FeildLogTag.class);
            if (tag != null) {
                fieldName = f.getName();
                try {
                    getMethod = clazz.getMethod(GET_PREFIX + fieldName);
                    setMethod = clazz.getMethod(SET_PREFIX + fieldName, f.getType());
                    setMethod.invoke(t1, getMethod.invoke(t2));
                } catch (Exception e) {
                    e.printStackTrace();
                    log.error("类:{}赋值操作失败，异常{}", clazz.getName(), e);
                    throw new Exception(String.format("类:%s赋值操作失败", clazz.getName()));
                }
            }
        }

        return cglibUtil.getLogInfo();
    }

    /**
     * 获取首字母
     *
     * @param prefix
     * @return
     */
    private String getPrefix(char prefix) {
        if (Character.isLowerCase(prefix)) {
            return String.valueOf(prefix).toUpperCase();
        } else {
            return String.valueOf(prefix).toLowerCase();
        }
    }

}
