package com.ykcloud.soa.erp.common.utils;


import com.ykcloud.soa.erp.common.entity.BaseEntity;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

/**
 * @Author Hewei
 * @Date 2018/6/4 14:06
 */
public class FinanceConfigUtil {
    private final static Logger log = LoggerFactory.getLogger(FinanceConfigUtil.class);
    private final static List<String> filterStr = Arrays.asList("SUB_UNIT_NUM_ID", "UNIT_NUM_ID", "CUST_SUB_UNIT_NUM_ID", "CUST_UNIT_NUM_ID", "ACCOUNT_MONTH", "CURRENCY", "ITEM_NUM_ID", "DIV_NUM_ID", "PTY_NUM_1", "PTY_NUM_2", "PTY_NUM_3", "DEPART_NUM_ID", "CARRY_SIGN", "SUPPLY_UNIT_NUM_ID", "TENANT_NUM_ID", "DATA_SIGN", "CREATE_USER_ID", "LAST_UPDATE_USER_ID");
    public static final String suffix = "_VAR";

    /**
     * @param sqlConditions            组装sql的条件集合
     * @param exArcAccountFieldconfigs 账务配置表
     * @param fieldList                单据配置表字段集合
     * @param phwbaObject              单据配置对象
     * @param gaObject                 总账对象
     * @param statementObject          流水对象
     * @param excludeFields            排除字段集合
     * @param excludeFieldsMap         key：排除字段-流水字段，value：排除字段对应的sign
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     */
    public static void dealConfig(List<String[]> sqlConditions, List<? extends Object> exArcAccountFieldconfigs, Field[] fieldList, Object phwbaObject, Object gaObject, Object statementObject, List<String> excludeFields, Map<String, Double> excludeFieldsMap) throws InvocationTargetException, IllegalAccessException {
        Method method;
        for (Object tmpConfig : exArcAccountFieldconfigs) {
            String actFieldName, actFieldName_hump;
            try {
                method = tmpConfig.getClass().getDeclaredMethod("getACTFIELDNAME");
                actFieldName = String.valueOf(method.invoke(tmpConfig));
            } catch (NoSuchMethodException e) {
                continue;
            }
            if (StringUtils.isEmpty(actFieldName)) {
                continue;
            }
            actFieldName = actFieldName.trim().replaceAll("\r", "").replaceAll("\n", "");
            actFieldName_hump = EntityFieldUtil.underline2Camel(actFieldName, false);
            for (Field field : fieldList) {
                if (field.getName().equalsIgnoreCase(actFieldName.concat("_sign"))) {
                    try {
                        method = phwbaObject.getClass().getDeclaredMethod("get" + actFieldName.concat("_sign").toUpperCase());
                    } catch (NoSuchMethodException e) {
                        log.debug("-------->" + phwbaObject.getClass().getName() + "-" + actFieldName.concat("_sign"));
                        continue;
                    }
                    long sign = method.invoke(phwbaObject) == null ? 0 : Long.parseLong(method.invoke(phwbaObject).toString());

                    try {
                        method = tmpConfig.getClass().getDeclaredMethod("getQTYORAMOUNT");
                    } catch (NoSuchMethodException e) {
                        log.debug("-------->" + tmpConfig.getClass().getName() + "-getQTYORAMOUNT");
                        continue;
                    }
                    Double qtyOrAmount = Double.parseDouble(method.invoke(tmpConfig).toString());

                    //判断是否需要处理
                    if (sign == 0 || qtyOrAmount == null) {
                        break;
                    }

                    try {
                        method = gaObject.getClass().getDeclaredMethod("get" + actFieldName.toUpperCase());
                    } catch (NoSuchMethodException e) {
                        log.debug("-------->" + gaObject.getClass().getName() + "-" + actFieldName);
                        try {
                            method = gaObject.getClass().getDeclaredMethod("get" + actFieldName_hump);
                        } catch (NoSuchMethodException ee) {
                            log.debug("-------->" + gaObject.getClass().getName() + "-" + actFieldName_hump);
                            continue;
                        }
                    }

                    //获取已有总账数据
                    double sourceData = method.invoke(gaObject) == null ? 0 : Double.parseDouble(method.invoke(gaObject).toString());

                    double newData;
                    String phwbaFieldName = null, phwbaFieldName_hump = null;
                    try {
                        if (qtyOrAmount == 0) {
                            method = phwbaObject.getClass().getDeclaredMethod("getWBAQTYFIELDNAME");
                        } else if (qtyOrAmount == 1) {
                            method = phwbaObject.getClass().getDeclaredMethod("getWBAAMOUNTFIELDNAME");
                        } else if (qtyOrAmount == 2) {
                            method = phwbaObject.getClass().getDeclaredMethod("getWBATAXAMOUNTFIELDNAME");
                        } else if (qtyOrAmount == 3) {
                            method = phwbaObject.getClass().getDeclaredMethod("getWBACOSTAMOUNTFIELDNAME");
                        }
                        phwbaFieldName = method.invoke(phwbaObject).toString();
                        if (StringUtils.isNotEmpty(phwbaFieldName)) {
                            try {
                                method = statementObject.getClass().getDeclaredMethod("get" + phwbaFieldName.toUpperCase());
                            } catch (NoSuchMethodException e) {
                                phwbaFieldName_hump = EntityFieldUtil.underline2Camel(phwbaFieldName, false);
                                method = statementObject.getClass().getDeclaredMethod("get" + phwbaFieldName_hump);
                            }
                        }
                        newData = method.invoke(statementObject) == null ? 0 : Double.parseDouble(method.invoke(statementObject).toString());
                    } catch (NoSuchMethodException e) {
                        log.debug("-------->" + statementObject.getClass().getName() + "-" + phwbaFieldName);
                        continue;
                    }

                    if (CollectionUtils.isNotEmpty(excludeFields) && excludeFieldsMap != null) {
                        //过滤期末不处理字段
                        boolean exclude = false;
                        for (String excludeField : excludeFields) {
                            if (StringUtils.isEmpty(excludeField)) {
                                continue;
                            }
                            if (actFieldName.equalsIgnoreCase(excludeField)) {
                                excludeFieldsMap.put(excludeField, sign * Double.valueOf(newData));
                                exclude = true;
                                break;
                            }
                        }
                        if (exclude) {
                            break;
                        }
                    }

                    try {
                        if (sqlConditions != null) {
                            String[] tmpCondition = new String[5];//index:0-字段，1-运算类型，2-数值，3-是否允许负数(1为减法有效)
                            tmpCondition[0] = actFieldName;
                            tmpCondition[1] = sign == 1 ? "+" : sign == -1 ? "-" : "";
                            tmpCondition[2] = String.valueOf(newData);

                            phwbaFieldName = "CANBENEGATIVE";
                            method = phwbaObject.getClass().getDeclaredMethod("get" + phwbaFieldName);
                            tmpCondition[3] = method.invoke(phwbaObject).toString();

                            phwbaFieldName = "GACOMPAREFIELDNAME";
                            method = phwbaObject.getClass().getDeclaredMethod("get" + phwbaFieldName);
                            tmpCondition[4] = method.invoke(phwbaObject).toString();

                            sqlConditions.add(tmpCondition);
                        }
                    } catch (NoSuchMethodException e) {
                        log.debug("-------->" + phwbaObject.getClass().getName() + "-" + phwbaFieldName);
                        continue;
                    }

                    try {
                        method = gaObject.getClass().getDeclaredMethod("set" + actFieldName.toUpperCase(), Double.class);
                    } catch (NoSuchMethodException e) {
                        log.debug("-------->" + gaObject.getClass().getName() + "-" + actFieldName);
                        try {
                            method = gaObject.getClass().getDeclaredMethod("set" + actFieldName_hump, Double.class);
                        } catch (NoSuchMethodException ee) {
                            log.debug("-------->" + gaObject.getClass().getName() + "-" + actFieldName_hump);
                            continue;
                        }
                    }
                    //变更数据
                    method.invoke(gaObject, sign == 1 ? sourceData + newData : sign == -1 ? sourceData - newData : 0);
                    break;
                }
            }
        }
    }

    public static void differenceObject(Object logObject, Object newObject) {
        if (logObject != null && newObject != null) {
            Field[] declaredFields = ArrayUtils.addAll(newObject.getClass().getDeclaredFields(), newObject.getClass().getSuperclass().getDeclaredFields());
            Method method;
            for (Field field : declaredFields) {
                try {
                    try {
                        method = newObject.getClass().getDeclaredMethod("get" + field.getName().toUpperCase());
                    } catch (NoSuchMethodException e) {
                        try {
                            method = newObject.getClass().getSuperclass().getDeclaredMethod("get" + field.getName().toUpperCase());
                        } catch (NoSuchMethodException ex) {
                            continue;
                        }
                    }
                    Object newValue = method.invoke(newObject);
                    if (newValue == null) {
                        continue;
                    }

                    try {
                        method = logObject.getClass().getDeclaredMethod("get" + field.getName().toUpperCase());
                    } catch (NoSuchMethodException e) {
                        try {
                            method = logObject.getClass().getSuperclass().getDeclaredMethod("get" + field.getName().toUpperCase());
                        } catch (NoSuchMethodException ex) {
                            continue;
                        }
                    }
                    Object logValue = method.invoke(logObject);

                    boolean matchFilter = filterStr.contains(field.getName().toUpperCase());
                    if (field.getType() == Double.class) {
                        try {
                            method = logObject.getClass().getMethod("set" + field.getName().toUpperCase(), Double.class);
                        } catch (NoSuchMethodException e) {
                            try {
                                method = logObject.getClass().getSuperclass().getMethod("set" + field.getName().toUpperCase(), Double.class);
                            } catch (NoSuchMethodException ex) {
                                continue;
                            }
                        }
                        method.invoke(logObject, Double.parseDouble(newValue.toString()));
                        if (!matchFilter) {
                            try {
                                method = logObject.getClass().getMethod("set" + field.getName().toUpperCase() + suffix, Double.class);
                            } catch (NoSuchMethodException e) {
                                try {
                                    method = logObject.getClass().getSuperclass().getMethod("set" + field.getName().toUpperCase() + suffix, Double.class);
                                } catch (NoSuchMethodException ex) {
                                    continue;
                                }
                            }
                            method.invoke(logObject, Double.parseDouble(newValue.toString()) - (logValue == null ? 0d : Double.parseDouble(logValue.toString())));
                        }
                    } else if (field.getType() == Long.class) {
                        try {
                            method = logObject.getClass().getMethod("set" + field.getName().toUpperCase(), Long.class);
                        } catch (NoSuchMethodException e) {
                            try {
                                method = logObject.getClass().getSuperclass().getMethod("set" + field.getName().toUpperCase(), Long.class);
                            } catch (NoSuchMethodException ex) {
                                continue;
                            }
                        }
                        method.invoke(logObject, Long.parseLong(newValue.toString()));
                        if (!matchFilter) {
                            try {
                                method = logObject.getClass().getMethod("set" + field.getName().toUpperCase() + suffix, Long.class);
                            } catch (NoSuchMethodException e) {
                                try {
                                    method = logObject.getClass().getSuperclass().getMethod("set" + field.getName().toUpperCase() + suffix, Long.class);
                                } catch (NoSuchMethodException ex) {
                                    continue;
                                }
                            }
                            method.invoke(logObject, Long.parseLong(newValue.toString()) - (logValue == null ? 0d : Long.parseLong(logValue.toString())));
                        }
                    } else if (field.getType() == Integer.class) {
                        try {
                            method = logObject.getClass().getMethod("set" + field.getName().toUpperCase(), Integer.class);
                        } catch (NoSuchMethodException e) {
                            try {
                                method = logObject.getClass().getSuperclass().getMethod("set" + field.getName().toUpperCase(), Integer.class);
                            } catch (NoSuchMethodException ex) {
                                continue;
                            }
                        }
                        method.invoke(logObject, Integer.parseInt(newValue.toString()));
                        if (!matchFilter) {
                            try {
                                method = logObject.getClass().getMethod("set" + field.getName().toUpperCase() + suffix, Integer.class);
                            } catch (NoSuchMethodException e) {
                                try {
                                    method = logObject.getClass().getSuperclass().getMethod("set" + field.getName().toUpperCase() + suffix, Integer.class);
                                } catch (NoSuchMethodException ex) {
                                    continue;
                                }
                            }
                            method.invoke(logObject, Integer.parseInt(newValue.toString()) - (logValue == null ? 0d : Integer.parseInt(logValue.toString())));
                        }
                    } else if (field.getType() == Date.class && logValue == null) {
                        try {
                            method = logObject.getClass().getMethod("set" + field.getName().toUpperCase(), Date.class);
                        } catch (NoSuchMethodException e) {
                            try {
                                method = logObject.getClass().getSuperclass().getMethod("set" + field.getName().toUpperCase(), Date.class);
                            } catch (NoSuchMethodException e1) {
                                continue;
                            }
                        }
                        method.invoke(logObject, newValue);
                    } else if (field.getType() == String.class && logValue == null) {
                        try {
                            method = logObject.getClass().getMethod("set" + field.getName().toUpperCase(), String.class);
                        } catch (NoSuchMethodException e) {
                            try {
                                method = logObject.getClass().getSuperclass().getMethod("set" + field.getName().toUpperCase(), String.class);
                            } catch (NoSuchMethodException e1) {
                                continue;
                            }
                        }
                        method.invoke(logObject, newValue);
                    } else if (logValue == null || matchFilter) {
                        try {
                            method = logObject.getClass().getMethod("set" + field.getName().toUpperCase(), Object.class);
                        } catch (NoSuchMethodException e) {
                            try {
                                method = logObject.getClass().getSuperclass().getMethod("set" + field.getName().toUpperCase(), Object.class);
                            } catch (NoSuchMethodException e1) {
                                continue;
                            }
                        }
                        method.invoke(logObject, newValue);
                    }
                } catch (IllegalAccessException | InvocationTargetException e) {
                    continue;
                }
            }
        }
    }

    public static void buildDynamicUpdateSQL(String tableName, List<String[]> sqlConditions, Long lastUpdateUserId, Long accountType, Object updateObject, StringBuffer stringBuffer, List<Object> objects, String... whereCondition) {
        if (CollectionUtils.isNotEmpty(sqlConditions) && whereCondition.length > 0) {
            StringBuffer whereStringBuffer = new StringBuffer();
            stringBuffer.append("UPDATE ");
            stringBuffer.append(tableName);
            stringBuffer.append(" SET LAST_UPDTME=now(),LAST_UPDATE_USER_ID=?");

            objects.add(lastUpdateUserId);

            Map<String, Double> checkMap = new HashMap<>();
            for (String[] tmpCondition : sqlConditions) {//index:0-字段，1-运算类型，2-数值，3-允许负数[0不允许,1不允许],4，比较字段
                stringBuffer.append(",");
                stringBuffer.append(tmpCondition[0]);
                stringBuffer.append("=");
                stringBuffer.append(tmpCondition[0]);
                stringBuffer.append(tmpCondition[1]);
                stringBuffer.append("?");
                objects.add(tmpCondition[2]);

                if ("0".equals(tmpCondition[3]) && tmpCondition[0].equalsIgnoreCase(tmpCondition[4])) {
                    checkMap.put(tmpCondition[4], Math.abs(Double.parseDouble(tmpCondition[2])));
                }
            }

            Method method;
            for (String condition : whereCondition) {
                if (StringUtils.isNotEmpty(condition)) {
                    try {
                        method = updateObject.getClass().getMethod("get" + condition.toUpperCase());
                        objects.add(method.invoke(updateObject));
                    } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                        log.debug("dynamicUpdate---->" + updateObject.getClass().getName() + " Not Found Property '" + condition.toUpperCase() + "'");
                        try {
                            method = updateObject.getClass().getSuperclass().getMethod("get" + condition.toUpperCase());
                            objects.add(method.invoke(updateObject));
                        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException ee) {
                            log.debug("dynamicUpdate---->" + updateObject.getClass().getName() + "'s SuperClass " + updateObject.getClass().getSuperclass().getName() + " Not Found Property '" + condition.toUpperCase() + "'");
                            continue;
                        }
                    }

                    if (whereStringBuffer.length() != 0) {
                        whereStringBuffer.append(" AND ");
                    }
                    whereStringBuffer.append(condition.toUpperCase());
                    whereStringBuffer.append("=?");
                }
            }

            if (accountType == 2) {
                Iterator<Map.Entry<String, Double>> iterator = checkMap.entrySet().iterator();
                while (iterator.hasNext()) {
                    Map.Entry<String, Double> next = iterator.next();
                    if (whereStringBuffer.length() != 0) {
                        whereStringBuffer.append(" AND ");
                    }
                    whereStringBuffer.append(next.getKey());
                    whereStringBuffer.append(" >=?");
                    objects.add(next.getValue());
                }
            }
            whereStringBuffer.insert(0, " WHERE ");
            stringBuffer.append(whereStringBuffer).toString();
        }
    }

    public static void main(String[] args) {
        BaseEntity baseEntity = new BaseEntity();
        baseEntity.setDATA_SIGN(0l);
        baseEntity.setCANCELSIGN("N");
        baseEntity.setSERIES("111111111111111");
        baseEntity.setTENANT_NUM_ID(12123123l);

        StringBuffer stringBuffer = new StringBuffer();
        List<Object> objects = new ArrayList<>();

        List<String[]> sqlConditions = new ArrayList<>();
        String[] tmpCondition = new String[5];//index:0-字段，1-运算类型，2-数值，3-允许负数[0不允许,1允许],4，sign
        tmpCondition[0] = "FILTER1";
        tmpCondition[1] = "-";
        tmpCondition[2] = "100";
        tmpCondition[3] = "0";
        tmpCondition[4] = "-1";
        sqlConditions.add(tmpCondition);

        tmpCondition = new String[5];//index:0-字段，1-运算类型，2-数值，3-允许负数[0不允许,1允许],4，sign
        tmpCondition[0] = "FILTER2";
        tmpCondition[1] = "-";
        tmpCondition[2] = "200";
        tmpCondition[3] = "1";
        tmpCondition[4] = "-1";
        sqlConditions.add(tmpCondition);

        tmpCondition = new String[5];//index:0-字段，1-运算类型，2-数值，3-允许负数[0不允许,1允许],4，sign
        tmpCondition[0] = "FILTER3";
        tmpCondition[1] = "-";
        tmpCondition[2] = "200";
        tmpCondition[3] = "0";
        tmpCondition[4] = "-1";
        sqlConditions.add(tmpCondition);

        buildDynamicUpdateSQL("BaseEntity", sqlConditions, 999l, 2l, baseEntity, stringBuffer, objects, "TENANT_NUM_ID", "DATA_SIGN");
        System.out.println(stringBuffer);
        System.out.println(Arrays.toString(objects.toArray()));
    }
}
