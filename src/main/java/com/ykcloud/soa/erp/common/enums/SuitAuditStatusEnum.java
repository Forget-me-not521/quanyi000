package com.ykcloud.soa.erp.common.enums;

/**
 * @Author stark.jiang
 * @Date 2021/10/16/17:24
 * @Description:
 * @Version 1.0
 */
public enum SuitAuditStatusEnum {
    PENDING(1, "待审核"),
    APPROVED(2, "审核通过"),
    REJECT(9, "审核否决");
    private Integer statusId;
    private String statusName;

    SuitAuditStatusEnum(Integer statusId, String statusName) {
        this.statusId = statusId;
        this.statusName = statusName;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }
}
