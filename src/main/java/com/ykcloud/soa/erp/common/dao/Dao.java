package com.ykcloud.soa.erp.common.dao;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.ykcloud.soa.erp.common.entity.PageQueryResult;
import com.ykcloud.soa.erp.common.utils.EntityFieldUtil;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

public abstract class Dao<T> {
    private String insertSql;

    private String tableName;

    private String tableFields;

    private Class<T> clazz;

    private static final int defaultBatchSize = 2000;

    public Dao() {
        clazz = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        this.tableName = clazz.getSimpleName();
        this.tableFields = EntityFieldUtil.fieldSplit(clazz, ",");
        this.insertSql = SqlUtil.generateInsertSql(clazz);
    }

    public int insert(T entity) {
        return jdbcTemplate().update(insertSql, SqlUtil.convertEntityInsert(entity));
    }

    public int insertNullToZero(T entity) {
        return jdbcTemplate().update(insertSql, SqlUtil.convertEntityInsertNotNull(entity));
    }

    public int batchInsert(List<T> entityList) {
        return this.batchInsert(entityList, defaultBatchSize);
    }

    public int batchInsert(List<T> entityList, int batchSize) {
        List<List<T>> partition = Lists.partition(entityList, batchSize);
        int result = 0;
        for (List<T> tList : partition) {
            List<Object[]> args = tList.stream().map(SqlUtil::convertEntityInsert).collect(Collectors.toList());
            int[] ints = jdbcTemplate().batchUpdate(insertSql, args);
            result += ints.length;
        }
        return result;
    }

    public int update(T entity, Long tenantNumId, Long dataSign, String series) {
        return this.update(entity, tenantNumId, dataSign, null, series);
    }

    public int update(T entity, Long tenantNumId, Long dataSign, Long subUnitNumId, String series) {
        return this.update(entity, tenantNumId, dataSign, subUnitNumId, null, series);
    }

    public int updateEntity(T entity, Long tenantNumId, Long dataSign, String cortNumId, String subUnitNumId, String series) {
        //return this.updateEntity(entity, tenantNumId, dataSign, cortNumId, subUnitNumId, series);
        List<Object> args = new LinkedList<>();
        String setSql = SqlUtil.convertEntityUpdate(entity, args);
        StringBuffer whereSql = new StringBuffer();
        whereSql.append(" tenant_num_id = ?  ");
        args.add(tenantNumId);
        whereSql.append(" and data_sign = ? ");
        args.add(dataSign);
        if (StringUtils.isNotBlank(subUnitNumId)) {
            whereSql.append(" and sub_unit_num_id = ? ");
            args.add(subUnitNumId);
        }
        if (StringUtils.isNotBlank(cortNumId)) {
            whereSql.append(" and cort_num_id = ? ");
            args.add(cortNumId);
        }
        whereSql.append(" and series = ? ");
        args.add(series);
        return jdbcTemplate().update(String.format("update %s set %s where %s ", tableName, setSql, whereSql), args.toArray());
    }

    public int update(T entity, Long tenantNumId, Long dataSign, Long subUnitNumId, String cortNumId, String series) {
        List<Object> args = new LinkedList<>();
        String setSql = SqlUtil.convertEntityUpdate(entity, args);
        StringBuffer whereSql = new StringBuffer();
        whereSql.append(" tenant_num_id = ?  ");
        args.add(tenantNumId);
        whereSql.append(" and data_sign = ? ");
        args.add(dataSign);
        if (Objects.nonNull(subUnitNumId)) {
            whereSql.append(" and sub_unit_num_id = ? ");
            args.add(subUnitNumId);
        }
        if (Objects.nonNull(cortNumId)) {
            whereSql.append(" and cort_num_id = ? ");
            args.add(cortNumId);
        }
        whereSql.append(" and series = ? ");
        args.add(series);
        return jdbcTemplate().update(String.format("update %s set %s where %s ", tableName, setSql, whereSql), args.toArray());
    }

    public int batchUpdate(String sql, Function<T, Object[]> convert, List<T> entityList) {
        List<Object[]> args = entityList.stream().map(convert).collect(Collectors.toList());
        int[] result = jdbcTemplate().batchUpdate(sql, args);
        return result.length;
    }


    public T get(String sql, Object... args) {
        return jdbcTemplate().queryForObject(sql, args, new BeanPropertyRowMapper<>(clazz));
    }

    public T get(Long tenantNumId, String series) {
        String sql = "SELECT " + getTableFields() + " FROM " + getTableName() + " WHERE series = ? and tenant_num_id = ?";
        return get(sql, new Object[]{series, tenantNumId});
    }
    public List<T> query(String sql, Object... args) {
        return jdbcTemplate().query(sql, args, new BeanPropertyRowMapper<>(clazz));
    }

    public List<T> query(Long tenantNumId, String cortNumId,List<String> series) {
        StringBuffer sb = new StringBuffer("'-0-'");
        for (String s : series) {
            sb.append(",'").append(s).append("'");
        }
        String sql = "SELECT " + getTableFields() + " FROM " + getTableName()
                + " WHERE tenant_num_id = ? and cort_num_id = ? and series in("+sb.toString()+")";
        return query(sql,tenantNumId,cortNumId);
    }

    public <R> R get(String sql, Class<R> r, Object... args) {
        return jdbcTemplate().queryForObject(sql, args, r);
    }

    public <R> List<R> query(String sql, Class<R> r, Object... args) {
        return jdbcTemplate().queryForList(sql, args, r);
    }

    public int batchDelete(List<String> series, Long tenantNumId,String cortNumId) {
        final StringBuffer sb = new StringBuffer();
        series.forEach(s -> sb.append("'").append(s).append("'").append(","));
        StringBuffer sql = new StringBuffer("update ")
                .append(tableName)
                .append(" set cancelsign = 'Y' ")
                .append(" where series in(")
                .append(sb.substring(0, sb.length() - 1)).append(")")
                .append(" and tenant_num_id = ").append(tenantNumId)
                .append(" and cort_num_id = '").append(cortNumId).append("'");
        return jdbcTemplate().update(sql.toString());
    }


    public <T> PageQueryResult<T> pageQuery(String sql, Long pageNum, Long pageSize, Class<T> clazz, Object ...args){
        Long from = (pageNum == null || pageNum <= 0 ? 1L : pageNum) - 1;
        Long size = (pageSize == null || pageSize <= 0 || pageSize > 1000) ? 10L : pageSize;

        String countSql = " SELECT count(*) from (" + sql + ") T";
        Object[] param = args;
        // 总数据行
        Long recordCount = jdbcTemplate().queryForObject(countSql, param, Long.class);
        Long pageCount = recordCount % size == 0 ? recordCount / size : (recordCount / size) + 1;

        String dataSql = sql + " limit " + (from * size) + " , " + size;
        List data = jdbcTemplate().queryForList(dataSql, param);
        data = JSON.parseArray(JSON.toJSONString(data), clazz);

        PageQueryResult result = new PageQueryResult();
        result.setData(data);
        result.setPageNum(from+1);
        result.setPageSize(size);
        result.setRecordCount(recordCount);
        result.setPageCount(pageCount);
        return result;
    }

    public List<T> queryForList(Long tenantNumId, Long dataSign, String subUnitNumId, String unitNumId, String cortNumId) {

        String sql = "select * from " + getTableName() +
                " where tenant_num_id = ? " +
                " and data_sign = ? " +
                " and cancelsign = 'N' ";
        List<Object> objectList = new ArrayList();
        objectList.add(tenantNumId);
        objectList.add(dataSign);
        if(StringUtils.isNotEmpty(subUnitNumId)){
            sql = sql +  " and sub_unit_num_id = ? ";
            objectList.add(subUnitNumId);
        }
        if(StringUtils.isNotEmpty(unitNumId)){
            sql = sql +  " and unit_num_id = ? ";
            objectList.add(unitNumId);
        }
        if(StringUtils.isNotEmpty(cortNumId)){
            sql = sql +  " and cort_num_id = ? ";
            objectList.add(cortNumId);
        }
        Object[] objectArray = new Object[objectList.size()];
        objectList.toArray(objectArray);
        //System.out.println(Arrays.toString(objectArray));
        return jdbcTemplate().query(sql, objectArray, new BeanPropertyRowMapper<>(clazz));
    }

    public Long selectCount(Long tenantNumId, Long dataSign, Long statusNumId, String subUnitNumId, String unitNumId, String cortNumId) {
        String sql = "select count(*) from " + getTableName() +
                " where tenant_num_id = ? " +
                " and data_sign = ? " +
                " and cancelsign = 'N' ";
        List<Object> objectList = new ArrayList();
        objectList.add(tenantNumId);
        objectList.add(dataSign);
        if(StringUtils.isNotEmpty(subUnitNumId)){
            sql = sql +  " and sub_unit_num_id = ? ";
            objectList.add(subUnitNumId);
        }
        if(StringUtils.isNotEmpty(unitNumId)){
            sql = sql +  " and unit_num_id = ? ";
            objectList.add(unitNumId);
        }
        if(StringUtils.isNotEmpty(cortNumId)){
            sql = sql +  " and cort_num_id = ? ";
            objectList.add(cortNumId);
        }
        if((ObjectUtils.isNotEmpty(statusNumId))&&(statusNumId.longValue() != 0L)){
            sql = sql +  " and status_num_id = ? ";
            objectList.add(statusNumId);
        }
        Object[] objectArray = new Object[objectList.size()];
        objectList.toArray(objectArray);
        //System.out.println(Arrays.toString(objectArray));
        return jdbcTemplate().queryForObject(sql, objectArray, Long.class);
    }

    public abstract JdbcTemplate jdbcTemplate();

    public String getTableName() {
        return tableName;
    }

    public String getTableFields() {
        if (Objects.isNull(tableFields)) {
            return EntityFieldUtil.fieldSplit(clazz, ",");
        }
        return tableFields;
    }
}
