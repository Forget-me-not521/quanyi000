package com.ykcloud.soa.erp.common.enums;

/**
 * 供应商保证金状态
 *
 * @author Sealin
 * @date 2018-07-20
 */
public enum StatusEnumForSupBond {
    UN_CONFIRM(1L, "未确认"),
    CONFIRMED(2L, "已确认"),
    ;
    public final Long ID;
    public final String COMMENT;

    StatusEnumForSupBond(Long id, String comment) {
        this.ID = id;
        this.COMMENT = comment;
    }
}
