package com.ykcloud.soa.erp.common.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description: 扣项扣款方向
 * @author: henry.wang
 * @date: 2018/5/14 14:27
 **/
public enum KxDirectionEnum {
    CUT(0L,"扣款"),
    BACK(1L,"返款");

    public static Map<Long,Long> kxDirection=new HashMap<>();

    static{
        kxDirection.put(1L,1L);
        kxDirection.put(-1L,2L);
    }

    private Long value;
    private String desc;

    KxDirectionEnum(Long value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
