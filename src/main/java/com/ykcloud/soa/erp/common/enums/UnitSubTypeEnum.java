package com.ykcloud.soa.erp.common.enums;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author stark.jiang
 * @Date 2021/10/25/16:45
 * @Description:
 * @Version 1.0
 */
public enum UnitSubTypeEnum {
    CORT(1,"公司"),
    SHOP(2,"门店"),
    STORAGE(3,"仓库"),
    DIVISION(4,"事业部");

    private int subType;

    private String typeDesc;


    UnitSubTypeEnum(int subType, String typeDesc) {
        this.subType = subType;
        this.typeDesc = typeDesc;
    }

    public int getSubType() {
        return subType;
    }

    public void setSubType(int subType) {
        this.subType = subType;
    }

    public String getTypeDesc() {
        return typeDesc;
    }



    public void setTypeDesc(String typeDesc) {
        this.typeDesc = typeDesc;
    }

    public static List<Integer> getCodeList() {
        List<Integer> typeNumIdList = new ArrayList<>();
        for(UnitSubTypeEnum subTypeEnum: UnitSubTypeEnum.values()) {
            typeNumIdList.add(subTypeEnum.getSubType());
        }
        return typeNumIdList;
    }
}
