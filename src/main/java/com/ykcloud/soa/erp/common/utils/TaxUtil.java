package com.ykcloud.soa.erp.common.utils;


import com.gb.soa.omp.ccommon.util.MathUtil;

import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Stream;


/**
 * 计算税额工具类
 * @author tz.x
 * @date 2018年5月4日下午8:59:04
 */
public class TaxUtil {
	
	/**
	 * 计算税额   price：含税单价，  taxRate：税率，    qty：数量
	 * @author tz.x
	 * @date 2018年5月5日上午9:43:18
	 */
	public static Double getTaxAmount(Double priceIncludeTax, Double taxRate, Double qty) {
		if (Objects.equals(0D, taxRate)) {
			return 0D;
		} 
		Double totalAmount = MathUtil.mutiply(priceIncludeTax, qty, 10);
		Double taxAmount = MathUtil.mutiply((MathUtil.divide(totalAmount, MathUtil.add(100, taxRate, 10), 10)), taxRate, 2);
		return taxAmount;
		
	}

	/**
	 * 计算税额
	 *
	 * @param totalAmount 金额
	 * @param taxRate     税率
	 * @return 税金
	 *
	 * @author Sealin
	 */
	public static Double getTaxAmount(Double totalAmount, Double taxRate) {
		if (Stream.of(totalAmount, taxRate).anyMatch(Predicate.isEqual(0D))) {
			return 0D;
		}
		// 如果存的是17这样的税率,先转换成0.17
		double realTaxRate = taxRate > 1D ? MathUtil.divide(taxRate, 100D, 10) : taxRate;
		// 税率大于100%, 不管传多少金额全都是税
		if (realTaxRate >= 1D) {
			return totalAmount;
		}

		// 税金计算公式 : 金额 / (1 + 税率) * 税率
		return MathUtil.mutiply(MathUtil.divide(totalAmount, MathUtil.add(1, realTaxRate, 2), 10), realTaxRate, 2);
	}

	/**
	 * 计算税额
	 *
	 * @param totalAmount 金额
	 * @param taxRate     税率
	 * @return 税金
	 *
	 * @author Sealin
	 */
	public static Double getTaxAmount(Double totalAmount, Double taxRate, int scale) {
		if (Stream.of(totalAmount, taxRate).anyMatch(Predicate.isEqual(0D))) {
			return 0D;
		}
		// 如果存的是17这样的税率,先转换成0.17
		double realTaxRate = taxRate > 1D ? MathUtil.divide(taxRate, 100D, 10) : taxRate;
		// 税率大于100%, 不管传多少金额全都是税
		if (realTaxRate >= 1D) {
			return totalAmount;
		}

		// 税金计算公式 : 金额 / (1 + 税率) * 税率
		return MathUtil.mutiply(MathUtil.divide(totalAmount, MathUtil.add(1, realTaxRate, 10), 10), realTaxRate, scale);
	}


	/**
	 * 含税转不含税   priceIncludeTax：含税价格或金额，  taxRate：税率
	 * @author tz.x
	 * @date 2018年5月5日上午10:00:02
	 */
	public static Double getPriceExcludeTax(Double priceIncludeTax, Double taxRate, int scale) {
//		if (Objects.equals(0D, taxRate)) {
//			return priceIncludeTax;
//		}
		Double priceExcludeTax = MathUtil.mutiply((MathUtil.divide(priceIncludeTax, MathUtil.add(100, taxRate, 10), 10)), 100, scale);
		return priceExcludeTax;
	}
	
	/**
	 * 不含税转含税   priceExcludeTax：不含税价格，  taxRate：税率
	 * @author tz.x
	 * @date 2018年5月5日上午10:00:02
	 */
	public static Double getPriceIncludeTax(Double priceExcludeTax, Double taxRate, int scale) {
		if (Objects.equals(0D, taxRate)) {
			return priceExcludeTax;
		}
		Double priceIncludeTax = MathUtil.mutiply((MathUtil.divide(priceExcludeTax, 100, 10)), MathUtil.add(100, taxRate, 10), scale);
		return priceIncludeTax;
	}
	
	/**
	 * 计算税额   totalAmountIncludeTax：含税总价，  taxRate：税率
	 * @author tz.x
	 * @date 2018年5月5日下午3:42:13
	 */
	public static Double getTaxAmountByTotalAmount(Double totalAmountIncludeTax, Double taxRate) {
//		if (Objects.equals(0D, taxRate)) {
//			return 0D;
//		}
		Double taxAmount = MathUtil.mutiply((MathUtil.divide(totalAmountIncludeTax, MathUtil.add(100, taxRate, 10), 10)), taxRate, 2);
		return taxAmount;
	}
	/**
	 * 计算税额   totalAmountExcludeTax：不含税总价，  taxRate：税率
	 * @author tz.x
	 * @date 2018年5月5日下午3:42:13
	 */
	public static Double getTaxAmountByTotalAmountExcludeTax(Double totalAmountExcludeTax, Double taxRate) {
		if (Objects.equals(0D, taxRate)) {
			return 0D;
		}
		Double taxAmount = MathUtil.mutiply(totalAmountExcludeTax, MathUtil.divide(taxRate, 100, 10), 2);
		return taxAmount;
	}
	
	/**
	 * @author zhaokang 
	 * @Description 获取税率
	 * @date 2018年12月5日
	 */
	public static Double getTaxAmountByTotalAmount(Double totalAmountIncludeTax, Double taxRate,int scale) {
		
		Double taxAmount = MathUtil.mutiply((MathUtil.divide(totalAmountIncludeTax, 
				MathUtil.add(100, taxRate, 10), 10)), taxRate, scale);
		return taxAmount;
	}
	
	public static void main(String[] args) {
		Double priceExcludeTax = 0.19;
		System.out.println(getPriceExcludeTax(priceExcludeTax,13d,8));
	}

}
