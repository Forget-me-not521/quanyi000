package com.ykcloud.soa.erp.common.enums;

public enum SalesReturnFlagEnum {
    IGNORE(0L,"忽略"),
    PLUS(1L, "加上"),
    MINUS(2L, "减掉");

    private Long value;
    private String desc;

    SalesReturnFlagEnum(Long value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public static SalesReturnFlagEnum fromValue(Long value) {
        for (SalesReturnFlagEnum enumItem : SalesReturnFlagEnum.values()) {
            if (enumItem.getValue().equals(value)) {
                return enumItem;
            }
        }
        return null;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
