package com.ykcloud.soa.erp.common.enums;

public enum InventoryBalanceAdjustType {

    CONSIGNMENT(1L,"代销商品款");

    InventoryBalanceAdjustType(Long sign,String desc){
        this.sign = sign;
        this.desc = desc;
    }

    private Long sign;

    private String desc;

    public Long getSign() {
        return sign;
    }

    public String getDesc() {
        return desc;
    }
}
