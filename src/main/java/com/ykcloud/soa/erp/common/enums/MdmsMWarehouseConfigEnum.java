package com.ykcloud.soa.erp.common.enums;

public enum  MdmsMWarehouseConfigEnum {
    REPLENISH_FALG_0(0,"replenish_falg","波次补货开关 0关"),
    REPLENISH_FALG_1(1,"replenish_falg","波次补货开关 1开");
    private Integer key;
    private String name ;
    private String desc;

    public Integer getKey() {
        return key;
    }

    public void setKey(Integer key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    MdmsMWarehouseConfigEnum(Integer key, String name, String desc) {
        this.key = key;
        this.name = name;
        this.desc = desc;
    }
}
