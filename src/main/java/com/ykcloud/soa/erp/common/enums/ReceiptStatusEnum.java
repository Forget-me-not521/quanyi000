package com.ykcloud.soa.erp.common.enums;

/**
 * 验收入库状态
 *
 * @author Sealin
 * Created on 2018-06-22
 */
public enum ReceiptStatusEnum {
    FULL_RECEIPTED(5L, "入库完成待结算");

    public final Long ID;
    public final String COMMENT;

    ReceiptStatusEnum(Long id, String comment) {
        this.ID = id;
        this.COMMENT = comment;
    }
}
