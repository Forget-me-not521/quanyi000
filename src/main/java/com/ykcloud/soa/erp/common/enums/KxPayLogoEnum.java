package com.ykcloud.soa.erp.common.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description: 扣项交款标志
 * @author: henry.wang
 * @date: 2018/5/14 14:27
 **/
public enum KxPayLogoEnum {

    CASH(0L,"交现金"),
    GOODS(1L,"从货款扣"),
    SUPPLY_DEFAULT(2L,"按供应商默认方式");


    public static Map<Long,Long> kxPayLogo=new HashMap<>();
    static{
//        kxPayLogo.put(0L,?);
        kxPayLogo.put(1L,2L);
//        kxPayLogo.put(2L,?)
    }

    KxPayLogoEnum(Long value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    private Long value;

    private String desc;

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
