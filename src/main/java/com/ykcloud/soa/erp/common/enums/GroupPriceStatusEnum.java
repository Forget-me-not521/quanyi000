package com.ykcloud.soa.erp.common.enums;

/**
 * @Author stark.jiang
 * @Date 2021/10/12/19:42
 * @Description:
 * @Version 1.0
 */
public enum GroupPriceStatusEnum {
    NOT_EFFECTIVE(1, "未生效"),
    EFFECTIVE(2, "已生效"),
    INVALID(9, "失效");
    private Integer statusId;
    private String statusName;

    GroupPriceStatusEnum(Integer statusId, String statusName) {
        this.statusId = statusId;
        this.statusName = statusName;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public static String getStatusNameByStatudId(Integer statusId) {
        String statusName = null;
        for (GroupPriceStatusEnum statusEnum : GroupPriceStatusEnum.values()) {
            if (statusEnum.statusId==statusId) {
                statusName = statusEnum.statusName;
                break;
            }
        }
        return statusName;
    }
}
