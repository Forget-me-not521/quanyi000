package com.ykcloud.soa.erp.common.enums;

import org.omg.CORBA.OBJ_ADAPTER;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description: 扣项类型
 * @author: henry.wang
 * @date: 2018/5/14 14:27
 **/
public enum KxTypeEnum {
    SALES_INCOME(0L,"按销售收入"),
    SALES_COST(1L, "按销售成本"),
    RECEIPT_COST(2L, "按验收成本"),
    PURCHASE_COST(3L,"按净进货成本"),
    ACCPET_QTY(4L,"按验收件数");

    public static Map<Long,Long> kxType=new HashMap<>();

    static {
        kxType.put(0L,0L);
        kxType.put(1L,1L);
//        kxType.put(2L,?);
        kxType.put(16L,2L);
//        kxType.put(25,?)
    }

    private Long value;
    private String desc;

    KxTypeEnum(Long value, String desc) {
        this.value = value;
        this.desc = desc;
    }


    public static KxTypeEnum fromValue(Long value) {
        for (KxTypeEnum enumItem : KxTypeEnum.values()) {
            if (enumItem.getValue().equals(value)) {
                return enumItem;
            }
        }
        return null;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }


    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
