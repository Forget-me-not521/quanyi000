package com.ykcloud.soa.erp.common.enums;

public enum KxAutoDelayFlagEnum {
    NO(0L,"否"),
    YES(1L, "是");

    private Long value;
    private String desc;

    KxAutoDelayFlagEnum(Long value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public static KxAutoDelayFlagEnum fromValue(Long value) {
        for (KxAutoDelayFlagEnum enumItem : KxAutoDelayFlagEnum.values()) {
            if (enumItem.getValue().equals(value)) {
                return enumItem;
            }
        }
        return null;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
