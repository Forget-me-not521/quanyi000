package com.ykcloud.soa.erp.common.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description: 扣项范围
 * @author: henry.wang
 * @date: 2018/5/14 14:27
 **/
public enum KxRangeEnum {
    ALL(0L,"全部"),
    SUB_UNIT_NUM_ID(2L, "按门店"),
    PTY_NUM_1(3L, "按大类");

    public static Map<Long,Long> kxRangeMap=new HashMap<>();
    static{
        kxRangeMap.put(0L,0L);
        kxRangeMap.put(1L,3L);
//        kxRangeMap.put(2L,?);
    }

    private Long value;
    private String desc;

    KxRangeEnum(Long value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public static KxRangeEnum fromValue(Long value) {
        for (KxRangeEnum enumItem : KxRangeEnum.values()) {
            if (enumItem.getValue().equals(value)) {
                return enumItem;
            }
        }
        return null;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
