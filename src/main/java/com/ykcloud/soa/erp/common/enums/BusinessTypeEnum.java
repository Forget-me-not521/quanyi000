package com.ykcloud.soa.erp.common.enums;


public enum BusinessTypeEnum {
	
	REPLENISH(10001L, "门店补货申请单"),
	DM_PRICE_ADJUSTMENT(10026L, "DM促销管理"),
	COST_ADJUSTMENT(10027L, "进价管理"),
	PRICE_ADJUSTMENT(10028L, "售价管理"),
	COST_PRICE_ADJUSTMENT(10029L, "进售价管理"),
	COMBO_ADJUSTMENT(10030L, "组合商品管理"),
	VIP_PRICE_ADJUSTMENT(10031L, "会员价管理"),
	STOCKCHECK(10012L, "盘点"),
	LOSS(10015L, "报损"),
	ADJUST_HZ(10016L, "库存数量调整"),
	WORK_ZM(10021L, "加工转码"),
	WORK_YJ(10022L, "议价加工转码"),
	SALECOMPENSATE(10034L, "固定价差促销折扣管理"),
	MOVE(10019L,"移库单"),
	COUPON(10013L,"团购单"),
	PRICECUT(10014L,"削价单");
	
	private Long businessType;
	private String businessTypeName;
	
	BusinessTypeEnum(Long businessType, String businessTypeName) {
		this.businessType = businessType;
		this.businessTypeName = businessTypeName;
	}
	
	public Long getBusinessType() {
		return businessType;
	}
	
	public void setBusinessType(Long businessType) {
		this.businessType = businessType;
	}
	
	public String getBusinessTypeName() {
		return businessTypeName;
	}
	
	public void setBusinessTypeName(String businessTypeName) {
		this.businessTypeName = businessTypeName;
	}
	
	public static String getBusinessTypeNameByBusinessType(Long businessType) {
        String businessTypeName = null;
        for (BusinessTypeEnum businessTypeEnum : BusinessTypeEnum.values()) {
            if (businessTypeEnum.businessType.equals(businessType)) {
            	businessTypeName = businessTypeEnum.businessTypeName;
                break;
            }
        }
        return businessTypeName;
    }

}
