package com.ykcloud.soa.erp.common.enums;

/**
 * @Author stark.jiang
 * @Date 2021/10/16/17:24
 * @Description:
 * @Version 1.0
 */
public enum SuitStatusEnum {
    NOT_EFFECTIVE(1, "未生效"),
    EFFECTIVE(2, "已生效"),
    INVALID(9, "失效");
    private Integer statusId;
    private String statusName;

    SuitStatusEnum(Integer statusId, String statusName) {
        this.statusId = statusId;
        this.statusName = statusName;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }
}
