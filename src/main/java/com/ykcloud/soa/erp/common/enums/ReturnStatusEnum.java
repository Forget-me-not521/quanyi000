package com.ykcloud.soa.erp.common.enums;

public enum ReturnStatusEnum {
    DRAFT_STATUS(1L,"草稿"),
    APPROVAL_PENDING(2L, "待审批"),
    APPROVAL_SUCCESS(3L, "审批通过"),
    APPROVAL_FAILED(4L, "审批未通过"),
    CONFIRM_CALLOUT(5L, "确认调出"),
    REJECT_CALLOUT(6L, "拒绝调出");

    private Long value;
    private String desc;

    ReturnStatusEnum(Long value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public static ReturnStatusEnum fromValue(Long value) {
        for (ReturnStatusEnum enumItem : ReturnStatusEnum.values()) {
            if (enumItem.getValue().equals(value)) {
                return enumItem;
            }
        }
        return null;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
