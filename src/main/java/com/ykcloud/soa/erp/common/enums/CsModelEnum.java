package com.ykcloud.soa.erp.common.enums;

/**
 * @author tz.x
 * @date 2020/9/7 15:35
 */
public enum CsModelEnum {

    REBATE_7_0_1(7, 0, 1),
    REBATE_7_1_1(7, 0, 1);

    private Integer businessType; // 0-初始化 1-授信单 2-销售订单 3-出库单 4-结算单 5-发票单 6-收款单 7-返利单
    private Integer billType; // 业务单据自己决定要按哪个字段区分
    private Integer operateType; // 1:财务审核 往后递增

    public Integer getBusinessType() {
        return businessType;
    }

    public void setBusinessType(Integer businessType) {
        this.businessType = businessType;
    }

    public Integer getBillType() {
        return billType;
    }

    public void setBillType(Integer billType) {
        this.billType = billType;
    }

    public Integer getOperateType() {
        return operateType;
    }

    public void setOperateType(Integer operateType) {
        this.operateType = operateType;
    }

    CsModelEnum(Integer businessType, Integer billType, Integer operateType) {
        this.businessType = businessType;
        this.billType = billType;
        this.operateType = operateType;
    }
}
