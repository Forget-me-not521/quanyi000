package com.ykcloud.soa.erp.common.exception;

import com.gb.soa.omp.ccommon.api.exception.AbstractExceptionType;
import com.gb.soa.omp.ccommon.api.exception.ExceptionTypeCategory;

public class ErpExceptionType extends AbstractExceptionType {


    public static ErpExceptionType VCE13002 = new ErpExceptionType(-13002,
            ExceptionTypeCategory.VALIDATE_CLIENT_EXCEPTION, SubSystem.ERP_SYNC, "期初同步系统客户端验证异常！");

    public static ErpExceptionType VCE13003 = new ErpExceptionType(-13003,
            ExceptionTypeCategory.VALIDATE_CLIENT_EXCEPTION, SubSystem.ERP_SCM, "供应链系统客户端验证异常！");

    //2-sync 3-scm 4-so 5-wm 6-fi
    public static ErpExceptionType INSTANCE = VCE13003;

    public static ErpExceptionType VCE13004 = new ErpExceptionType(-13004,
            ExceptionTypeCategory.VALIDATE_CLIENT_EXCEPTION, SubSystem.ERP_SO, "订单单据系统客户端验证异常！");

    public static ErpExceptionType VCE13005 = new ErpExceptionType(-13005,
            ExceptionTypeCategory.VALIDATE_CLIENT_EXCEPTION, SubSystem.ERP_WM, "仓库管理系统客户端验证异常！");

    public static ErpExceptionType VCE13006 = new ErpExceptionType(-13006,
            ExceptionTypeCategory.VALIDATE_CLIENT_EXCEPTION, SubSystem.ERP_FI, "财务系统客户端验证异常！");

    public static ErpExceptionType VCE13007 = new ErpExceptionType(-13007,
            ExceptionTypeCategory.VALIDATE_CLIENT_EXCEPTION, SubSystem.ERP_ARS, "补货系统客户端验证异常！");

    public static ErpExceptionType VCE13008 = new ErpExceptionType(-13008,
            ExceptionTypeCategory.VALIDATE_CLIENT_EXCEPTION, SubSystem.ERP_OPS, "门店运营系统客户端验证异常！");

    public static ErpExceptionType VCE13009 = new ErpExceptionType(-13009,
            ExceptionTypeCategory.VALIDATE_CLIENT_EXCEPTION, SubSystem.ERP_SCM, "供应链系统客户端验证异常！");


    public static ErpExceptionType VBE23002 = new ErpExceptionType(-23002,
            ExceptionTypeCategory.VALIDATE_BUSINESS_EXCEPTION, SubSystem.ERP_SYNC, "期初同步系统业务验证异常！");

    public static ErpExceptionType VBE23003 = new ErpExceptionType(-23003,
            ExceptionTypeCategory.VALIDATE_BUSINESS_EXCEPTION, SubSystem.ERP_SCM, "供应链系统业务验证异常！");

    public static ErpExceptionType VBE23004 = new ErpExceptionType(-23004,
            ExceptionTypeCategory.VALIDATE_BUSINESS_EXCEPTION, SubSystem.ERP_SO, "订单单据系统业务验证异常！");

    public static ErpExceptionType VBE23005 = new ErpExceptionType(-23005,
            ExceptionTypeCategory.VALIDATE_BUSINESS_EXCEPTION, SubSystem.ERP_WM, "仓库管理系统业务验证异常！");

    public static ErpExceptionType VBE23006 = new ErpExceptionType(-23006,
            ExceptionTypeCategory.VALIDATE_BUSINESS_EXCEPTION, SubSystem.ERP_FI, "财务系统业务验证异常！");

    public static ErpExceptionType VBE23008 = new ErpExceptionType(-23008,
            ExceptionTypeCategory.VALIDATE_BUSINESS_EXCEPTION, SubSystem.ERP_OPS, "门店运营系统业务验证异常！");

    public static ErpExceptionType DOE33002 = new ErpExceptionType(-33002,
            ExceptionTypeCategory.DATABASE_OPERATE_EXCEPTION, SubSystem.ERP_SYNC, "期初同步系统数据库操作异常！");

    public static ErpExceptionType DOE33003 = new ErpExceptionType(-33003,
            ExceptionTypeCategory.DATABASE_OPERATE_EXCEPTION, SubSystem.ERP_SCM, "供应链系统数据库操作异常！");

    public static ErpExceptionType DOE33004 = new ErpExceptionType(-33004,
            ExceptionTypeCategory.DATABASE_OPERATE_EXCEPTION, SubSystem.ERP_SO, "订单单据系统数据库操作异常！");

    public static ErpExceptionType DOE33005 = new ErpExceptionType(-33005,
            ExceptionTypeCategory.DATABASE_OPERATE_EXCEPTION, SubSystem.ERP_WM, "仓库管理系统数据库操作异常！");

    public static ErpExceptionType DOE33006 = new ErpExceptionType(-33006,
            ExceptionTypeCategory.DATABASE_OPERATE_EXCEPTION, SubSystem.ERP_FI, "财务系统数据库操作异常！");

    public static ErpExceptionType DOE33007 = new ErpExceptionType(-33007,
            ExceptionTypeCategory.DATABASE_OPERATE_EXCEPTION, SubSystem.ERP_ARS, "补货系统数据库操作异常！");

    public static ErpExceptionType DOE33008 = new ErpExceptionType(-33008,
            ExceptionTypeCategory.DATABASE_OPERATE_EXCEPTION, SubSystem.ERP_OPS, "门店运营系统数据库操作异常！");

    public static ErpExceptionType BE43001 = new ErpExceptionType(-43001,
            ExceptionTypeCategory.BUSINESS_EXCEPTION, SubSystem.ERP_SCM, "商品补货业务异常！");

    public static ErpExceptionType BE43002 = new ErpExceptionType(-43002,
            ExceptionTypeCategory.BUSINESS_EXCEPTION, SubSystem.ERP_WM, "仓库收货业务异常！");

    public static ErpExceptionType BE43003 = new ErpExceptionType(-43003,
            ExceptionTypeCategory.BUSINESS_EXCEPTION, SubSystem.ERP_SCM, "配送锁库失败！");
    public static ErpExceptionType BE43004 = new ErpExceptionType(-43004,
            ExceptionTypeCategory.BUSINESS_EXCEPTION, SubSystem.ERP_WM, "未查询到仓库中商品的最新批次价格！");
    public static ErpExceptionType BE43005 = new ErpExceptionType(-43005,
            ExceptionTypeCategory.BUSINESS_EXCEPTION, SubSystem.ERP_WM, "未查询到仓库中商品的有库存最新批次价格！");

    public static ErpExceptionType BE43006 = new ErpExceptionType(-43006,
            ExceptionTypeCategory.BUSINESS_EXCEPTION, SubSystem.ERP_WM, "未查询到记录！");//origin:53000

    public static ErpExceptionType BE43007 = new ErpExceptionType(-43007,
            ExceptionTypeCategory.BUSINESS_EXCEPTION, SubSystem.ERP_WM, "未查询到商品库中的商品信息！");//53005

    public static ErpExceptionType BE43008 = new ErpExceptionType(-43008,
            ExceptionTypeCategory.BUSINESS_EXCEPTION, SubSystem.ERP_WM, "未查询到批次明细表信息！");//53006

    public static ErpExceptionType BE43009 = new ErpExceptionType(-43009,
            ExceptionTypeCategory.BUSINESS_EXCEPTION, SubSystem.ERP_WM, "批次明细表字段不合法！");//53007

    public static ErpExceptionType BE43010 = new ErpExceptionType(-43010,
            ExceptionTypeCategory.BUSINESS_EXCEPTION, SubSystem.ERP_WM, "批次明细未能完全分配！");//53008

    public static ErpExceptionType BE43011 = new ErpExceptionType(-43011,
            ExceptionTypeCategory.BUSINESS_EXCEPTION, SubSystem.ERP_WM, "批次明细分配批次失败！");//53009


    public static ErpExceptionType BE43012 = new ErpExceptionType(-43012,
            ExceptionTypeCategory.DATABASE_OPERATE_EXCEPTION, SubSystem.ERP_WM, "查询到重复记录！");//53010

    public static ErpExceptionType BE43013 = new ErpExceptionType(-43013,
            ExceptionTypeCategory.BUSINESS_EXCEPTION, SubSystem.ERP_WM, "导入盘存表失败！");

    //public static ErpExceptionType BE43013=new ErpExceptionType(-43013,
    //		ExceptionTypeCategory.BUSINESS_EXCEPTION,SubSystem.ERP_FI, "参数验证不通过！"); //63000

    public static ErpExceptionType BE43014 = new ErpExceptionType(-43014,
            ExceptionTypeCategory.BUSINESS_EXCEPTION, SubSystem.ERP_FI, "缺少批次日月进销存配置表数据！");//63001

    public static ErpExceptionType BE43015 = new ErpExceptionType(-43015,
            ExceptionTypeCategory.BUSINESS_EXCEPTION, SubSystem.ERP_FI, "批次库存进销存处理失败！");//63002

    public static ErpExceptionType BE43016 = new ErpExceptionType(-43016,
            ExceptionTypeCategory.BUSINESS_EXCEPTION, SubSystem.ERP_FI, "商品移动加权权重处理失败！");//63003

    public static ErpExceptionType BE43017 = new ErpExceptionType(-43017,
            ExceptionTypeCategory.BUSINESS_EXCEPTION, SubSystem.ERP_FI, "商品进销处理失败！");//63004

    public static ErpExceptionType BE43018 = new ErpExceptionType(-43018,
            ExceptionTypeCategory.BUSINESS_EXCEPTION, SubSystem.ERP_FI, "代销商品处理失败！");//63005

    public static ErpExceptionType BE43019 = new ErpExceptionType(-43019,
            ExceptionTypeCategory.BUSINESS_EXCEPTION, SubSystem.ERP_SO, "该商品缺货,店内调拨失败！");//63006

    public static ErpExceptionType BE43020 = new ErpExceptionType(-43020,
            ExceptionTypeCategory.BUSINESS_EXCEPTION, SubSystem.ERP_SO, "店间调拨处理失败！");//63007

    public static ErpExceptionType BE43021 = new ErpExceptionType(-43021,
            ExceptionTypeCategory.BUSINESS_EXCEPTION, SubSystem.ERP_SCM, "批量导入订货审批单失败！");//63008

    public static ErpExceptionType BE43022 = new ErpExceptionType(-43022,
            ExceptionTypeCategory.BUSINESS_EXCEPTION, SubSystem.ERP_SCM, "批量作废采购单失败！"); //63009

    public static ErpExceptionType BE43023 = new ErpExceptionType(-43023,
            ExceptionTypeCategory.BUSINESS_EXCEPTION, SubSystem.ERP_SCM, "批量导入配送审批单失败！");//63010

    public static ErpExceptionType BE43024 = new ErpExceptionType(-43024,
            ExceptionTypeCategory.BUSINESS_EXCEPTION, SubSystem.ERP_SCM, "批量导入店间调拨申请单失败！");//63011

    public static ErpExceptionType BE43025 = new ErpExceptionType(-43025,
            ExceptionTypeCategory.BUSINESS_EXCEPTION, SubSystem.ERP_SCM, "批量导入期初库存数据失败！");//63012

    public static ErpExceptionType BE43026 = new ErpExceptionType(-43026,
            ExceptionTypeCategory.BUSINESS_EXCEPTION, SubSystem.ERP_SO, "店内调拨、行政领料、委外拆解领料商品EXCEL导入失败！");

    public static ErpExceptionType BE43027 = new ErpExceptionType(-43027,
            ExceptionTypeCategory.BUSINESS_EXCEPTION, SubSystem.ERP_SO, "退货申请单EXCEL导入失败！");

    public static ErpExceptionType BE43028 = new ErpExceptionType(-43028,
            ExceptionTypeCategory.BUSINESS_EXCEPTION, SubSystem.ERP_SO, "日结处理中！");

    public static ErpExceptionType BE43029 = new ErpExceptionType(-43029,
            ExceptionTypeCategory.BUSINESS_EXCEPTION, SubSystem.ERP_SCM, "批量导入门店商品销售属性失败 ！");

    public static ErpExceptionType BE43031 = new ErpExceptionType(-43031,
            ExceptionTypeCategory.BUSINESS_EXCEPTION, SubSystem.ERP_SCM, "批量导入采购协议失败 ！");

    public static ErpExceptionType BE43030 = new ErpExceptionType(-43030,
            ExceptionTypeCategory.BUSINESS_EXCEPTION, SubSystem.ERP_SCM, "批量导入进价调整单失败 ！");

    public static ErpExceptionType BE43033 = new ErpExceptionType(-43033,
            ExceptionTypeCategory.VALIDATE_BUSINESS_EXCEPTION, SubSystem.ERP_SCM, "批量导入数据验证失败 ！");

    public static ErpExceptionType BE43034 = new ErpExceptionType(-43034,
            ExceptionTypeCategory.VALIDATE_BUSINESS_EXCEPTION, SubSystem.ERP_SCM, "批量导入订货申请单失败 ！");

    public static ErpExceptionType BE43035 = new ErpExceptionType(-43035,
            ExceptionTypeCategory.BUSINESS_EXCEPTION, SubSystem.ERP_SCM, "商品状态禁止补货！");

    public static ErpExceptionType BE43036 = new ErpExceptionType(-43036,
            ExceptionTypeCategory.BUSINESS_EXCEPTION, SubSystem.ERP_SCM, "订货审批单复制完成！");

    public static ErpExceptionType BE43037 = new ErpExceptionType(-43037,
            ExceptionTypeCategory.BUSINESS_EXCEPTION, SubSystem.ERP_SCM, "批量导入采购合同失败 ！");

    public static ErpExceptionType BE43038 = new ErpExceptionType(-43038,
            ExceptionTypeCategory.BUSINESS_EXCEPTION, SubSystem.ERP_SCM, "部分导入成功 ！");

    public static ErpExceptionType BE43042 = new ErpExceptionType(-43042,
            ExceptionTypeCategory.BUSINESS_EXCEPTION, SubSystem.ERP_OPS, "部分导入成功 ！");

    public static ErpExceptionType BE43039 = new ErpExceptionType(-43039,
            ExceptionTypeCategory.BUSINESS_EXCEPTION, SubSystem.ERP_SCM, "批量补货指定多门店，多商品导入失败");

    public static ErpExceptionType BE43040 = new ErpExceptionType(-43040,
            ExceptionTypeCategory.BUSINESS_EXCEPTION, SubSystem.ERP_SCM, "生成采购申请单失败！");
    public static ErpExceptionType BE43041 = new ErpExceptionType(-43041,
            ExceptionTypeCategory.BUSINESS_EXCEPTION, SubSystem.ERP_FI, "部分导入成功 ！");

    public static ErpExceptionType BE100001 = new ErpExceptionType(-100001,
            ExceptionTypeCategory.BUSINESS_EXCEPTION, SubSystem.ERP_SCM, "供应链业务异常！");

    public static ErpExceptionType BE60001 = new ErpExceptionType(-60001,
            ExceptionTypeCategory.BUSINESS_EXCEPTION, SubSystem.ERP_ARS, "补货系统业务异常！");


    public static ErpExceptionType AC80001 = new ErpExceptionType(-80001,
            ExceptionTypeCategory.BUSINESS_EXCEPTION, SubSystem.ERP_ACTIVITI, "数据异常！");

    public static ErpExceptionType AC80002 = new ErpExceptionType(-80002,
            ExceptionTypeCategory.BUSINESS_EXCEPTION, SubSystem.ERP_ACTIVITI, "业务异常！");

    public static ErpExceptionType AC80003 = new ErpExceptionType(-80003,
            ExceptionTypeCategory.BUSINESS_EXCEPTION, SubSystem.ERP_ACTIVITI, "流程图异常！");

    public static ErpExceptionType AC80004 = new ErpExceptionType(-80004,
            ExceptionTypeCategory.BUSINESS_EXCEPTION, SubSystem.ERP_ACTIVITI, "请求参数异常！");
    
    public static ErpExceptionType DOE80005 = new ErpExceptionType(-80005,
            ExceptionTypeCategory.DATABASE_OPERATE_EXCEPTION, SubSystem.ERP_FI, "工作流系统数据库操作异常！");
    
    public static ErpExceptionType AC90001 = new ErpExceptionType(-90001,
            ExceptionTypeCategory.BUSINESS_EXCEPTION, SubSystem.ERP_SUPINTERFACE, "数据异常！");

    public static ErpExceptionType AC90002 = new ErpExceptionType(-90002,
            ExceptionTypeCategory.BUSINESS_EXCEPTION, SubSystem.ERP_SUPINTERFACE, "业务异常！");

    public static ErpExceptionType AC90003 = new ErpExceptionType(-90003,
            ExceptionTypeCategory.BUSINESS_EXCEPTION, SubSystem.ERP_SUPINTERFACE, "请求参数异常！");

    // 构造方法
    private ErpExceptionType(long code, ExceptionTypeCategory category, String subSystem, String description) {
        super(code, category, subSystem, description);
        lookup.put(code, this);
    }

    private static class SubSystem {
        public static String ERP_SYNC = "erp-sync";//2
        public static String ERP_SCM = "erp-scm";//3
        public static String ERP_SO = "erp-so";//4
        public static String ERP_WM = "erp-wm";//5
        public static String ERP_FI = "erp-fi";//6
        public static String ERP_ARS = "erp-ars";
        public static String ERP_ACTIVITI = "erp-activiti";
        public static String ERP_SUPINTERFACE = "erp-supinterface";
        public static String ERP_OPS = "erp_ops";
    }

    public AbstractExceptionType getAbstractExceptionTypeByCode(long code) {
        AbstractExceptionType et = lookup.get(code);
        return et;
    }

}