package com.ykcloud.soa.erp.common.enums;

/**
 * @Author stark.jiang
 * @Date 2021/10/12/19:42
 * @Description:
 * @Version 1.0
 */
public enum DictEnum {
    DOSAGE(1, "剂型"),
    APPROVE_FIRST_TYPE(2, "批文一级类型"),
    APPROVE_SECOND_TYPE(3, "批文二级类型"),
    TAX_RATE_ID(4, "税务分类编码"),
    SALE_CODE(5, "经营资质代码"),
    DISEASE_CODE(6, "疾病种码");
    private int typeId;
    private String typeName;

    DictEnum(int typeId, String typeName) {
        this.typeId = typeId;
        this.typeName = typeName;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public static String getTypeNameByTypeId(int typeId) {
        String typeName = null;
        for (DictEnum dictEnum : DictEnum.values()) {
            if (dictEnum.typeId==typeId) {
                typeName = dictEnum.typeName;
                break;
            }
        }
        return typeName;
    }
}
