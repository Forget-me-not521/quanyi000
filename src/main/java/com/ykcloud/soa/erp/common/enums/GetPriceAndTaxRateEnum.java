package com.ykcloud.soa.erp.common.enums;

/**
 * /**
 *
 * @description:
 * @author: Dan
 * @time: 2020/6/14 15:00
 */
public enum GetPriceAndTaxRateEnum {

    FIRST("1", "类型1"),
    SECOND("2", "类型2"),
    THIRD("3","批次成本");

    private String typeNumId;

    private String desc;

    GetPriceAndTaxRateEnum(String typeNumId, String desc) {
        this.typeNumId = typeNumId;
        this.desc = desc;
    }

    public String getTypeNumId() {
        return typeNumId;
    }

    public void setTypeNumId(String typeNumId) {
        this.typeNumId = typeNumId;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
