package com.ykcloud.soa.erp.common.utils;

import java.text.DecimalFormat;
import java.util.Objects;
import java.util.regex.Pattern;

/**
 * @author alfred.liu
 * @date 2019/2/18 20:33
 * @description 生成金额码
 */
public class GenerateAmountCodeUtil {

    private static final String CODE = "23";
    // 保留两位小数
    private static final String TWO = "######0.00";
    // 保留三位小数
    private static final String THREE = "######0.000";
    // 保留零位小数
    private static final String ZERO = "######0";

    // 区分报错信息
    private static final String COUNT = "count";
    private static final String AMOUNT = "amount";

    /***
     计算其校验的过程为：
     前12位的奇数位的和c1：6+0+2+4+6+8=26
     前12位的偶数位之和c2：9+1+3+5+7+9=34
     将奇数和与偶数和的八倍相加26+34*8=128
     取结果的个位数：128  8
     用10减去这个个位数 10-8=2
     所以校验码为2
     (注：如果取结果的个数为0，那么校验码不是为10，而是0.
     * 获取最后一位校验位的方法
     * @param barcode
     * @return
     */
    public static String lastValidateCode(String barcode) {
        char[] aryStr = barcode.toCharArray();
        int a = 0;
        int b = 0;
        StringBuffer s1 = new StringBuffer();
        StringBuffer s2 = new StringBuffer();
        int index = 1;
        for (int i = aryStr.length ; i >= 0; i--) {
            if (index % 2 == 0) {
                a = a + Integer.parseInt(String.valueOf(aryStr[i]));
                s1.append(aryStr[i] + " + ");
            } else if (index >= 3) {
                b = b + Integer.parseInt(String.valueOf(aryStr[i]));
                s2.append(aryStr[i] + " + ");
            }
            index++;
        }
        int s = a * 3 + b;
        int res = 10 - s % 10;
        if (res == 10) {
            res = 0;
        }
        return String.valueOf(res);
    }

    // 获取金额码
    public static String AmountCode(String barcode, Double count, Double amount) throws Exception {
        StringBuilder sb = new StringBuilder();
        sb.append(barcode);
        String tempItemNumId = "";
        if(amount >=10000){
            throw new Exception("团购金额不能超过9999.99！");
        }

        if(count >=100){
            throw new Exception("团购数量不能超过99.999！");
        }
        count = count * 1000;
        // 获取团购数量规则
        String toCount = toString(count, ZERO);
        String tempCount = temp(toCount,5,COUNT);
        // 获取金额规则
        amount = amount * 100;
        String toAmount = toString(amount, ZERO);
        String tempAmount= temp(toAmount,6,AMOUNT);
        sb.append(tempCount+tempAmount);
        String code = lastValidateCode(sb.toString());
        sb.append(code);
        //System.out.println(count);
        return sb.toString();
    }

    private static String temp(String counts, int size,String ex) throws Exception {
        String tempCount = "";
        if (counts.length()<=size){
            boolean boo =true;
            do {
                if (tempCount.length()+counts.length() == size){
                    boo = false;
                    continue;
                }
                tempCount = 0+tempCount;

            }while (boo);
           return tempCount = tempCount+counts;
        }else {
            if (Objects.equals(ex,COUNT)){
                throw new Exception("数量不符合金额码的生成规则！");
            }else if (Objects.equals(ex,AMOUNT)){
                throw new Exception("金额不符合金额码的生成规则！");
            }
            return null;
        }
    }

    // double转string
    public static String toString(Double d, String s) {
        DecimalFormat decimalFormat = new DecimalFormat(s);
        return decimalFormat.format(d);
    }

    // 截取字符串
    public static String[] spiltString(String s, String pattern) {
        String[] strings = s.split(Pattern.quote(pattern));
        return strings;
    }

    public static void main(String[] args) {
        try {
                    System.out.println(AmountCode("23444444", 99.999D, 332.97D));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        String barcode = "23123456789";
        System.out.println(barcode.substring(0,8));
        System.out.println(lastValidateCode("2309464433000109560"));
    }
}
