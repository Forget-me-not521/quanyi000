package com.ykcloud.soa.erp.common.enums;

import java.util.Arrays;

/**
 * 支付周期
 *
 * @author Sealin
 */
public enum PayCycleEnum {
    /**
     * 日结
     */
    DAILY(1L, "日结"),
    /**
     * 周结
     */
    WEEKLY(2L, "周结"),
    /**
     * 双周 / 半月结
     */
    TWICE_OF_MONTHLY(3L, "双周/半月结"),
    /**
     * 月结
     */
    MONTHLY(4L, "月结"),
    /**
     * 双月结算
     */
    DOUBLE_MONTH(5L, "双月结算");

    public final Long ID;
    public final String COMMENT;

    PayCycleEnum(Long id, String comment) {
        this.ID = id; this.COMMENT = comment;
    }

    /**
     * 将编号转换为结算周期
     *
     * @author Sealin
     */
    public static PayCycleEnum from(Long value) {
        return Arrays.stream(PayCycleEnum.values())
                     .filter(e -> e.ID.equals(value))
                     .findFirst()
                     .orElse(null);
    }

    /**
     * 校验编号是否为有效的结算周期
     *
     * @author Sealin
     */
    public static Boolean checkValid(Long value) {
        return Arrays.stream(PayCycleEnum.values())
                     .anyMatch(e -> e.ID.equals(value));
    }
}
