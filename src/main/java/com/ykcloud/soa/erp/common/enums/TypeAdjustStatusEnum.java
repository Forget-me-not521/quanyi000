package com.ykcloud.soa.erp.common.enums;

public enum TypeAdjustStatusEnum {

    APPROVING(1L, "未审核"), ACCOUNT_OF(2L, "制单审核"), ACCOUNT_IN(-3L, "入账中"), APPROVING_NOT(3L, "财务审核");

    public Long ID;

    public String COMMENT;

    TypeAdjustStatusEnum(Long id, String comment) {
        this.ID = id;
        this.COMMENT = comment;
    }

}
