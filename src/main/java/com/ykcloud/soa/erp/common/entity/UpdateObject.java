package com.ykcloud.soa.erp.common.entity;

import com.gb.soa.omp.ccommon.util.RedisLock;
import lombok.Data;

/**
 * @author tz.x
 * @date 2020/9/24 19:57
 */
@Data
public class UpdateObject {

    private String sql;

    private Object[] params;

}
