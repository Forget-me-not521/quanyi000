package com.ykcloud.soa.erp.common.entity;

import com.gb.soa.omp.ccommon.api.request.AbstractUserSessionRequest;

import java.util.Date;

public final class EntityConverter {

    public static <T extends AbstractUserSessionRequest,V extends BaseEntity> V get(T t, V v){
        InnerConverter converter = new InnerConverter();
        converter.setT(t);
        converter.setV(v);
        return (V)converter.get();
    }

    private static class InnerConverter<T extends AbstractUserSessionRequest, V extends BaseEntity> {
        private T t;
        private V v;

        public void setT(T t) {
            this.t = t;
        }

        public void setV(V v) {
            this.v = v;
        }

        public V get() {
            v.setTENANT_NUM_ID(t.getTenantNumId());
            v.setCREATE_DTME(new Date());
            v.setCREATE_USER_ID(t.getUserNumId());
            v.setDATA_SIGN(t.getDataSign());
            v.setLAST_UPDATE_USER_ID(t.getUserNumId());
            v.setLAST_UPDTME(new Date());
            v.setCANCELSIGN("N");
            return (V)v;
        }
    }
}
