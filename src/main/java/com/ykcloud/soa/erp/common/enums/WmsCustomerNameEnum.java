package com.ykcloud.soa.erp.common.enums;

/**
 * @Package: com.ykcloud.soa.erp.wm.enums
 * @ClassName: WmsCustomerNameEnum
 * @Author: Dan
 * @Date: 2020/11/9 20:18
 */
public enum WmsCustomerNameEnum {

    PULUOGE("PULUOGE"),
    FUQUE("FUQUE");

    private String customerName;

    WmsCustomerNameEnum(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }
}
