package com.ykcloud.soa.erp.common.enums;

/**
 * 调整结果表单据状态
 *
 * @author Sealin
 * @date 2018-07-02
 */
public enum StatusEnumForAdjustResult {
    UNCONFIRD(1L, "未确认"),
    CONFIRMED(2L, "已确认");


    public final Long ID;
    public final String COMMENT;

    StatusEnumForAdjustResult(Long id, String comment) {
        ID = id;
        COMMENT = comment;
    }
}
